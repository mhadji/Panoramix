#ifndef SoRich_MCRichOpticalPhotonType_h
#define SoRich_MCRichOpticalPhotonType_h

// Inheritance :
#include <OnXSvc/KeyedType.h>

#include <Event/MCRichOpticalPhoton.h> //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class SoSeparator;
class MCRichOpticalPhotonRep;

class MCRichOpticalPhotonType 
:public OnXSvc::KeyedType<LHCb::MCRichOpticalPhoton> 
{
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
  virtual void beginVisualize();
  virtual void visualize(Lib::Identifier aIdentifier,void*);
  virtual void endVisualize();
public:
  MCRichOpticalPhotonType(IUserInterfaceSvc*,
                  ISoConversionSvc*,
                  IDataProviderSvc*);
private:
  SoSeparator* m_separator;
  MCRichOpticalPhotonRep* m_rep;
  bool m_empty;
};

#endif
