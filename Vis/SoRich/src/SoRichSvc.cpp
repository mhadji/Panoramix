// this :
#include "SoRichSvc.h"

// Gaudi :
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/IToolSvc.h>

// OnXSvc :
#include <OnXSvc/IUserInterfaceSvc.h>

// local
#include "MCRichHitType.h"
#include "MCRichOpticalPhotonType.h"
#include "MCRichSegmentType.h"

DECLARE_COMPONENT( SoRichSvc )

SoRichSvc::SoRichSvc(const std::string& name,ISvcLocator* pSvcLocator)
  :Service(name,pSvcLocator)
  ,m_uiSvc(0)
  ,m_soConSvc(0)
  ,m_evtDataSvc(0)
  ,m_toolSvc(0)
  ,m_msgStream(0) { }

SoRichSvc::~SoRichSvc() { delete m_msgStream; }

StatusCode SoRichSvc::initialize()
{
  StatusCode sc = Service::initialize();
  if( sc.isFailure() ) return sc ;

  debug() << "SoRichSvc::initialize " << endmsg;

  setProperties();

  sc = ( getService( "OnXSvc",          m_uiSvc      ) &&
         getService( "SoConversionSvc", m_soConSvc   ) &&
         getService( "EventDataSvc",    m_evtDataSvc ) &&
         getService( "ToolSvc",         m_toolSvc    ) );
  if ( sc.isFailure() ) return sc ;

  m_uiSvc->addType(new MCRichHitType          (m_uiSvc,m_soConSvc,m_evtDataSvc));
  m_uiSvc->addType(new MCRichOpticalPhotonType(m_uiSvc,m_soConSvc,m_evtDataSvc));
  m_uiSvc->addType(new MCRichSegmentType      (m_uiSvc,m_soConSvc,m_evtDataSvc));

  return sc;
}

StatusCode SoRichSvc::finalize()
{
  releaseSvc(m_uiSvc);
  releaseSvc(m_soConSvc);
  releaseSvc(m_evtDataSvc);
  releaseSvc(m_toolSvc);
  debug() << "Finalized successfully" << endmsg;
  return StatusCode::SUCCESS;
}
