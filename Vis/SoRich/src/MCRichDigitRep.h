
/** @file MCRichDigitRep.h
 *
 *  Header file for visual representation object for class : MCRichDigitRep
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#ifndef  SORICH_MCRICHDIGITREP_H
#define  SORICH_MCRICHDIGITREP_H 1

// STL
#include <functional>

#include <HEPVis/nodes/SoHighlightMaterial.h>
#include "MarkerSet.h"

// GaudiKernel
#include "GaudiKernel/StatusCode.h"

// Rich SmartIDTool
#include "RichInterfaces/IRichSmartIDTool.h"

class SoSeparator;
class SoStyleCache;

namespace LHCb
{
  class MCRichDigit;
}

// ============================================================================
/** @class MCRichDigitRep  MCRichDigitRep.h
 *
 *  Helper class(functor) which performs the real
 *  visualization of MCRichDigit Object
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */
// ============================================================================

class MCRichDigitRep
  : public std::unary_function<const LHCb::MCRichDigit*,SoSeparator*>
{

public: // help class

  class Qualities {

  public:

    Qualities( const SbColor & color,
               const SbColor & hcolor,
               const SoMarkerSet::MarkerType mType )
      : m_color  ( color  ),
        m_hcolor ( hcolor ),
        m_mType  ( mType  ) { }

    const SbColor & color()  const { return m_color;  }
    const SbColor & hcolor() const { return m_hcolor; }
    SoMarkerSet::MarkerType markerType() const { return m_mType; }

  private:

    SbColor m_color;
    SbColor m_hcolor;
    SoMarkerSet::MarkerType m_mType;

  };

public:

  /** standard constructor
   */
  MCRichDigitRep( const std::string & Type,
                  const Qualities & qualities,
                  const Rich::ISmartIDTool * smartTool,
                  SoStyleCache* styleCache );

  /** virtual destructor
   */
  virtual  ~MCRichDigitRep();

  /** the only one essential method
   *  @param digit pointer to the digit
   *  @return pointer to corresponsing Inventor Node
   */
  SoSeparator* operator() ( const LHCb::MCRichDigit * digit ) const ;


private: // methods

  const Rich::ISmartIDTool * smartIDTool() const  { return m_smartIDTool; }
  const std::string & markerStyle() const       { return m_type;        }
  const Qualities & qualities() const           { return m_qualities;   }

private: // data

  std::string m_type;

  const Qualities m_qualities;

  /// Pointer to RichSmartID tool
  const Rich::ISmartIDTool * m_smartIDTool;

  SoStyleCache* m_styleCache;
};

// ============================================================================
#endif ///  SORICH_MCRICHDIGITVIS_H
// ============================================================================
