// this :
#include "MCRichHitType.h"

#include "MCRichHitRep.h"

#include <OnXSvc/Helpers.h>

#include <Inventor/nodes/SoSeparator.h>

//////////////////////////////////////////////////////////////////////////////
MCRichHitType::MCRichHitType(
                             IUserInterfaceSvc* aUISvc
                             ,ISoConversionSvc* aSoCnvSvc
                             ,IDataProviderSvc* aDataProviderSvc
                             )
  :OnXSvc::Type<LHCb::MCRichHit>(
                                 LHCb::MCRichHits::classID(),
                                 "MCRichHit",
                                 LHCb::MCRichHitLocation::Default,
                                 aUISvc,aSoCnvSvc,aDataProviderSvc)
  ,m_separator(0)
  ,m_rep(0)
  ,m_empty(true)
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty("gPosX",Lib::Property::DOUBLE);
  addProperty("gPosY",Lib::Property::DOUBLE);
  addProperty("gPosZ",Lib::Property::DOUBLE);
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable MCRichHitType::value(
                                   Lib::Identifier aIdentifier
                                   ,const std::string& aName
                                   ,void*
                                   )
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::MCRichHit* obj = (LHCb::MCRichHit*)aIdentifier;

  const Gaudi::XYZPoint gPos = obj->entry();

  if(aName=="gPosX") {
    return Lib::Variable(printer(),gPos.x());
  } else if(aName=="gPosY") {
    return Lib::Variable(printer(),gPos.y());
  } else if(aName=="gPosZ") {
    return Lib::Variable(printer(),gPos.z());
  } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
void MCRichHitType::beginVisualize(
)
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  OnXSvc::Type<LHCb::MCRichHit>::beginVisualize();

  // Representation attributes :
  float r = 0.5, g = 0.5, b = 0.5;
  attribute("modeling.color",r,g,b);
  SbColor color(r,g,b);
  float hr = 1.0, hg = 1.0, hb = 1.0;
  attribute("modeling.highlightColor",hr,hg,hb);
  SbColor hcolor(hr,hg,hb);
  std::string shape;
  if(!attribute("modeling.MCRichHitMode",shape)) shape = "marker";

  m_rep = new MCRichHitRep(shape,
                           MCRichHitRep::Qualities
                           (color,hcolor,SoMarkerSet::CIRCLE_FILLED_5_5),
                           fSoRegion->styleCache());

  m_separator = m_rep->begin();

  m_empty = true;
}
//////////////////////////////////////////////////////////////////////////////
void MCRichHitType::visualize(
                              Lib::Identifier aIdentifier
                              ,void*
                              )
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::MCRichHit* obj = (LHCb::MCRichHit*)aIdentifier;
  m_rep->represent(obj,m_separator);
  m_empty = false;
}
//////////////////////////////////////////////////////////////////////////////
void MCRichHitType::endVisualize(
)
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(m_empty) {
    m_separator->unref();
  } else {
    fSoRegion->resetUndo();
    region_addToDynamicScene(*fSoRegion,m_separator);
  }
  delete m_rep;
  OnXSvc::Type<LHCb::MCRichHit>::endVisualize();
}



