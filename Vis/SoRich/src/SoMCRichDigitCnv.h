
/** @file SoMCRichDigitCnv.h
 *
 *  Header file for RICH "So" visualisation converter : SoMCRichDigitCnv
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#ifndef     SORICH_SOMCRICHDIGITCNV_H
#define     SORICH_SOMCRICHDIGITCNV_H 1

// SoRich
#include "SoRichBaseCnv.h"

// Rich SmartIDTool
#include "RichInterfaces/IRichSmartIDTool.h"

/**  @class SoMCRichDigitCnv  SoMCRichDigitCnv.h
 *
 *   Converter for visualization of MCRichDigit objects
 *
 *   @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *   @date    09/05/2004
 */

class SoMCRichDigitCnv : public SoRichBaseCnv
{
public:

  /// standard initialization method
  virtual StatusCode initialize ();

  /// standard finalization  method
  virtual StatusCode finalize   ();

  /// Returns the representation type
  virtual long repSvcType() const;

  // the only one essential method
  virtual StatusCode createRep( DataObject* /* Object */ ,
                                IOpaqueAddress*&  /* Address */ );

  /// Class ID for created object == class ID for this specific converter
  static const CLID&  classID();

  /// storage Type
  static unsigned char storageType () ;

  /// standard constructor
  SoMCRichDigitCnv( ISvcLocator* svcLoc );

  /// virtual destructor
  virtual ~SoMCRichDigitCnv();

private:

  /// default constructor is disabled
  SoMCRichDigitCnv           (                         ) ;

  /// copy constructor is disabled
  SoMCRichDigitCnv           ( const SoMCRichDigitCnv& ) ;

  /// assignment is disabled
  SoMCRichDigitCnv& operator=( const SoMCRichDigitCnv& ) ;

private:

  /// Pointer to RichSmartID tool
  const Rich::ISmartIDTool * m_smartIDTool;

};


// ============================================================================
#endif  //  SORICH_SOMCRICHDIGITCNV_H
// ============================================================================
