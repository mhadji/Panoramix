
/** @file SoMCRichDigitCnv.cpp
 *
 *  Implementation file for RICH "So" visual converter : SoMCRichDigitCnv
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

// Inventor :
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoCoordinate3.h>

// OnXSvc
#include  "OnXSvc/Win32.h"

// GaudiKernel
#include  "GaudiKernel/IRegistry.h"
#include  "GaudiKernel/IDataProviderSvc.h"
#include  "GaudiKernel/SmartDataPtr.h"
#include  "GaudiKernel/DataObject.h"
#include  "GaudiKernel/IToolSvc.h"

// OnXSvc
#include  "OnXSvc/IUserInterfaceSvc.h"
#include  "OnXSvc/ClassID.h"
#include  "OnXSvc/Helpers.h"

// Lib :
#include <Lib/smanip.h>
#include <Lib/Interfaces/ISession.h>

// HEPVis :
#include  "HEPVis/nodekits/SoRegion.h"

// RichEvent
#include  "Event/MCRichDigit.h"

// Local
#include  "SoMCRichDigitCnv.h"
#include  "MCRichDigitRep.h"

// namespaces
using namespace LHCb;

// ============================================================================
/// mandatory factory business
// ============================================================================
DECLARE_COMPONENT( SoMCRichDigitCnv )

// ============================================================================
/// standard constructor
// ============================================================================
SoMCRichDigitCnv::SoMCRichDigitCnv( ISvcLocator* svcLoc )
  : SoRichBaseCnv ( classID(), svcLoc ),
    m_smartIDTool ( 0 )
{
  setName ( "SoMCRichDigitCnv" );
}

// ============================================================================
/// standard desctructor
// ============================================================================
SoMCRichDigitCnv::~SoMCRichDigitCnv(){}

// ============================================================================
/// initialize
// ============================================================================
StatusCode SoMCRichDigitCnv::initialize()
{
  const StatusCode sc = SoRichBaseCnv::initialize();
  if ( sc.isFailure() )
    return Error("initialize: Could not initialize base class!");

  // Acquire tools
  this->acquireTool( "RichSmartIDTool", m_smartIDTool );

  return sc;
}

// ============================================================================
/// finalize
// ============================================================================
StatusCode SoMCRichDigitCnv::finalize()
{
  // release tools
  releaseTool( m_smartIDTool );

  return SoRichBaseCnv::finalize();
}

long SoMCRichDigitCnv::repSvcType() const
{
  return i_repSvcType();
}

// ============================================================================
/// Class ID for created object == class ID for this specific converter
// ============================================================================
const CLID& SoMCRichDigitCnv::classID()
{
  return MCRichDigits::classID();
}

// ============================================================================
/// storage Type
// ============================================================================
unsigned char SoMCRichDigitCnv::storageType()
{
  return So_TechnologyType;
}

// ============================================================================
// the only one essential method
// ============================================================================
StatusCode SoMCRichDigitCnv::createRep( DataObject*         object     ,
                                        IOpaqueAddress*& /* Address */ )
{

  // Preliminary checks
  if ( 0 == object  ) return Error("createRep: DataObject* points to NULL");
  if ( 0 == uiSvc() ) return Error("createRep: IUserInterfaceSvc* points to NULL" );

  SoRegion* soRegion = uiSvc()->currentSoRegion();
  if(soRegion==0)
  { return Error("createRep: SoRegion* points to NULL" );}

  ISession* session = uiSvc()->session();
  if(session==0)
  { return Error("createRep: ISession* points to NULL" );}

  const MCRichDigits* digits = dynamic_cast<const MCRichDigits*>( object );
  if ( 0 == digits  )
  { return Error( "createRep: Wrong input data type '" +
                  System::typeinfoName(typeid(object)) + "'" ); }
  if ( digits->empty() )
  { return Debug( "Container " + object->registry()->identifier() +
                  " is empty" ); }

  // Find first non-null digit
  typedef MCRichDigits::const_iterator RiMCDigitIt;
  RiMCDigitIt idig;
  for ( idig = digits->begin(); idig != digits->end(); ++idig ) { if(*idig) break; }
  if( digits->end() == idig )
  { return Warning("Container " + object->registry()->identifier() +
                   " contains NULL pointers !" , StatusCode::SUCCESS ) ;}

  // Colours
  double r, g, b, hr, hg, hb;
  std::string color1, color2;
  if ( !session->parameterValue( "modeling.color", color1 ) ) color1 = "red";
  Lib::smanip::torgb(color1,r,g,b);
  SbColor color(r,g,b);
  if ( !session->parameterValue( "modeling.highlightColor", color2 ) ) color2 = "red";
  Lib::smanip::torgb(color2,hr,hg,hb);
  SbColor hcolor(hr,hg,hb);

  // marker
  std::string shape;
  if ( !session->parameterValue("modeling.MCRichDigitMode",shape) ) shape = "marker";

  // create visualisation object
  Debug( "Visualizer for MCRichDigits : Style= " +shape +
         " : colors="+color1+" "+color2 );
  MCRichDigitRep vis( shape,
                      MCRichDigitRep::Qualities( color, hcolor,
                                                 SoMarkerSet::CIRCLE_FILLED_5_5 ),
                      m_smartIDTool,
                      soRegion->styleCache() );

  typedef std::vector<SoNode*> Nodes;
  Nodes nodes;

  // transform digits to separators
  std::transform( digits->begin(), digits->end(),
                  std::back_inserter( nodes ), vis ) ;

  for ( Nodes::iterator inode = nodes.begin();
        nodes.end() != inode ; ++inode ) {
    if ( 0 != *inode )  {
      //  Send scene graph to the viewing region
      // (in the "dynamic" sub-scene graph) :
      region_addToDynamicScene(*soRegion,*inode);
    } else { Error( "createRep: SoSeparator* points to NULL!, skip it!" ); }
  }

  return StatusCode::SUCCESS;
}
