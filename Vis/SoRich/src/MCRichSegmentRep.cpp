
/** @file MCRichSegmentRep.cpp
 *
 *  Implementation file for visual representation object for class : MCRichSegmentRep
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#include <OnXSvc/Win32.h>

#include <cmath>

// This :
#include "MCRichSegmentRep.h"

// CLHEP
#include "GaudiKernel/Point3DTypes.h"

// Inventor
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoIndexedLineSet.h>

#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoSceneGraph.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>

// RichEvent
#include <Event/MCRichSegment.h>

// namespaces
using namespace LHCb;

// ============================================================================
// standard constructor
// ============================================================================
MCRichSegmentRep::MCRichSegmentRep( const std::string& Type,
                                    const Qualities & qualities,
                                    SoStyleCache* styleCache)
  : m_type        ( Type      )
  , m_qualities   ( qualities ) 
  , m_styleCache  ( styleCache)
  , m_coordinate  ( 0 )
  , m_icoord      ( 0 )
{ }

// ============================================================================
// destructor
// ============================================================================
MCRichSegmentRep::~MCRichSegmentRep(){}

// ============================================================================
// ============================================================================
SoSeparator* MCRichSegmentRep::begin()
{
  SoSeparator* separator = new SoSeparator();
  separator->addChild(m_styleCache->getLightModelBaseColor());
  separator->addChild(m_styleCache->getLineStyle());

  m_coordinate = new SoCoordinate3;
  separator->addChild(m_coordinate);
  m_icoord = 0;

  return separator;
}


void MCRichSegmentRep::represent( 
 const MCRichSegment * segment 
,SoSeparator* aSeparator
)
{
  // Build name (for picking) :
  char sid[64];
  ::sprintf(sid,"MCRichSegment/0x%lx",(unsigned long)segment);

  SoSceneGraph * separator = new SoSceneGraph();
  separator->setString(sid);

  aSeparator->addChild(separator);

  separator->addChild(
    m_styleCache->getHighlightMaterial(qualities().color(),
                                       qualities().hcolor()));

  if ( markerStyle() == "line" ) {

    // Build a set of lines representing this segment
    const int segmentn = segment->trajectoryPoints().size() - 1;

    SbVec3f * points = new SbVec3f[2*segmentn];
    int32_t* coordIndex = new int32_t[3*segmentn];

    int32_t pointn = 0;
    int32_t coordi = 0;

    std::vector<Gaudi::XYZPoint>::const_iterator iPnts = 
      segment->trajectoryPoints().begin();

    // Draw lines between each of the trajectory points
    while ( iPnts != segment->trajectoryPoints().end() ) {
      const Gaudi::XYZPoint & start = *iPnts;
      if ( (++iPnts) == segment->trajectoryPoints().end() ) continue;
      const Gaudi::XYZPoint & end   = *iPnts;

      points[pointn].setValue(start.x(),start.y(),start.z());
      coordIndex[coordi] = m_icoord + pointn;
      ++pointn;
      coordi++;

      points[pointn].setValue(end.x(),end.y(),end.z());
      coordIndex[coordi] = m_icoord + pointn;
      ++pointn;
      coordi++;

      coordIndex[coordi] = SO_END_LINE_INDEX;
      coordi++;
    }

    if ( pointn > 0 ) 
    {
      m_coordinate->point.setValues(m_icoord,pointn,points);
      m_icoord += pointn;

      SoIndexedLineSet* lineSet = new SoIndexedLineSet;
      lineSet->coordIndex.setValues(0,coordi,coordIndex);
      separator->addChild(lineSet);
    }

    delete [] points;
    delete [] coordIndex;

  }
}

// ============================================================================
