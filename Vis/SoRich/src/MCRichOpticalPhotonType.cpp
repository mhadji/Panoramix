// this :
#include "MCRichOpticalPhotonType.h"

#include "MCRichOpticalPhotonRep.h"

#include <OnXSvc/Helpers.h>

#include <Inventor/nodes/SoSeparator.h>

//////////////////////////////////////////////////////////////////////////////
MCRichOpticalPhotonType::MCRichOpticalPhotonType(
                                                 IUserInterfaceSvc* aUISvc
                                                 ,ISoConversionSvc* aSoCnvSvc
                                                 ,IDataProviderSvc* aDataProviderSvc
                                                 )
  :OnXSvc::KeyedType<LHCb::MCRichOpticalPhoton>(
                                                LHCb::MCRichOpticalPhotons::classID(),
                                                "MCRichOpticalPhoton",
                                                LHCb::MCRichOpticalPhotonLocation::Default,
                                                aUISvc,aSoCnvSvc,aDataProviderSvc)
  ,m_separator(0)
  ,m_rep(0)
  ,m_empty(true)
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty("emissionX",Lib::Property::DOUBLE);
  addProperty("emissionY",Lib::Property::DOUBLE);
  addProperty("emissionZ",Lib::Property::DOUBLE);
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable MCRichOpticalPhotonType::value(
                                             Lib::Identifier aIdentifier
                                             ,const std::string& aName
                                             ,void*
                                             )
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::MCRichOpticalPhoton* obj = (LHCb::MCRichOpticalPhoton*)aIdentifier;

  //const Gaudi::XYZPoint & mirrSp    = photon->sphericalMirrorReflectPoint();
  //const Gaudi::XYZPoint & mirrFl    = photon->flatMirrorReflectPoint();
  //const Gaudi::XYZPoint & detPt     = photon->pdIncidencePoint();

  if(aName=="emissionX") {
    const Gaudi::XYZPoint& p = obj->emissionPoint();
    return Lib::Variable(printer(),p.x());
  } else if(aName=="emissionY") {
    const Gaudi::XYZPoint& p = obj->emissionPoint();
    return Lib::Variable(printer(),p.y());
  } else if(aName=="emissionZ") {
    const Gaudi::XYZPoint& p = obj->emissionPoint();
    return Lib::Variable(printer(),p.z());
  } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
void MCRichOpticalPhotonType::beginVisualize(
)
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  OnXSvc::KeyedType<LHCb::MCRichOpticalPhoton>::beginVisualize();

  // Representation attributes :
  float r = 0.5, g = 0.5, b = 0.5;
  attribute("modeling.color",r,g,b);
  SbColor color(r,g,b);
  float hr = 1.0, hg = 1.0, hb = 1.0;
  attribute("modeling.highlightColor",hr,hg,hb);
  SbColor hcolor(hr,hg,hb);
  std::string shape;
  if(!attribute("modeling.MCRichOpticalPhotonMode",shape)) shape = "line";

  m_rep = new MCRichOpticalPhotonRep(shape,
                                     MCRichOpticalPhotonRep::Qualities
                                     (color,hcolor,SoMarkerSet::CIRCLE_FILLED_5_5),
                                     fSoRegion->styleCache());

  m_separator = m_rep->begin();

  m_empty = true;
}
//////////////////////////////////////////////////////////////////////////////
void MCRichOpticalPhotonType::visualize(
                                        Lib::Identifier aIdentifier
                                        ,void*
                                        )
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::MCRichOpticalPhoton* obj = (LHCb::MCRichOpticalPhoton*)aIdentifier;
  m_rep->represent(obj,m_separator);
  m_empty = false;
}
//////////////////////////////////////////////////////////////////////////////
void MCRichOpticalPhotonType::endVisualize(
)
  //////////////////////////////////////////////////////////////////////////////
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(m_empty) {
    m_separator->unref();
  } else {
    fSoRegion->resetUndo();
    region_addToDynamicScene(*fSoRegion,m_separator);
  }
  delete m_rep;
  OnXSvc::KeyedType<LHCb::MCRichOpticalPhoton>::endVisualize();
}



