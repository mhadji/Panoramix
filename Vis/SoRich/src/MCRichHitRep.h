
/** @file MCRichHitRep.h
 *
 *  Header file for visual representation object for class : MCRichHitRep
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#ifndef  SORICH_MCRICHHITREP_H
#define  SORICH_MCRICHHITREP_H 1

#include <Inventor/SbColor.h>
#include "MarkerSet.h"

class SoSeparator;
class SoStyleCache;

// GaudiKernel
#include "GaudiKernel/StatusCode.h"

namespace LHCb
{
  class MCRichHit;
}

// ============================================================================
/** @class MCRichHitRep  MCRichHitRep.h
 *
 *  Helper class(functor) which performs the real
 *  visualization of MCRichHit Object
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */
// ============================================================================

class MCRichHitRep {

public: // help class

  class Qualities {

  public:

    Qualities( const SbColor & color,
               const SbColor & hcolor,
               const SoMarkerSet::MarkerType mType )
      : m_color  ( color  ),
        m_hcolor ( hcolor ),
        m_mType  ( mType  ) { }

    const SbColor & color()  const { return m_color;  }
    const SbColor & hcolor() const { return m_hcolor; }
    SoMarkerSet::MarkerType markerType() const { return m_mType; }

  private:

    SbColor m_color;
    SbColor m_hcolor;
    SoMarkerSet::MarkerType m_mType;

  };

public:

  /** standard constructor
   */
  MCRichHitRep( const std::string & Type,
                const Qualities & qualities,
                SoStyleCache* styleCache );

  /** virtual destructor
   */
  virtual  ~MCRichHitRep();

  /** begin a scene graph for a collection.
   *  @return the top separator.
   */
  SoSeparator* begin() const;

  /** represent one hit.
   *  @param hit pointer to the hit
   *  @param sep the top separator to put the sub-scene graph.
   */
  void represent(const LHCb::MCRichHit* hit,SoSeparator* sep) const;

private: // methods

  const std::string & markerStyle() const       { return m_type; }
  const Qualities & qualities() const { return m_qualities; }

private: // data

  std::string m_type;

  const Qualities m_qualities;

  SoStyleCache* m_styleCache;
};

// ============================================================================
#endif ///  SORICH_MCRICHHITVIS_H
// ============================================================================











