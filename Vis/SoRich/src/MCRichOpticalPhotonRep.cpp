
/** @file MCRichOpticalPhotonRep.cpp
 *
 *  Implementation file for visual representation object for class : MCRichOpticalPhotonRep
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#include <OnXSvc/Win32.h>

// This :
#include "MCRichOpticalPhotonRep.h"

#include <cmath>

// CLHEP
#include <GaudiKernel/Point3DTypes.h>

// Inventor
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoIndexedLineSet.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoDrawStyle.h>

#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoSceneGraph.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>

// RichEvent
#include <Event/MCRichOpticalPhoton.h>

// namespaces
using namespace LHCb;

// ============================================================================
// standard constructor
// ============================================================================
MCRichOpticalPhotonRep::MCRichOpticalPhotonRep( const std::string& Type,
                                                const Qualities & qualities,
                                                SoStyleCache* styleCache)
  : m_type        ( Type      )
  , m_qualities   ( qualities ) 
  , m_styleCache  ( styleCache)
  , m_coordinate  ( 0 )
  , m_icoord      ( 0 )
{ }

// ============================================================================
// destructor
// ============================================================================
MCRichOpticalPhotonRep::~MCRichOpticalPhotonRep(){}

// ============================================================================
// ============================================================================
SoSeparator* MCRichOpticalPhotonRep::begin()
{
  SoSeparator* separator = new SoSeparator();
  separator->addChild(m_styleCache->getLightModelBaseColor());
  separator->addChild(m_styleCache->getLineStyle());

  m_coordinate = new SoCoordinate3;
  separator->addChild(m_coordinate);
  m_icoord = 0;

  return separator;
}

void MCRichOpticalPhotonRep::represent( 
 const MCRichOpticalPhoton* photon 
,SoSeparator* aSeparator
)
{
  // Build picking string id :
  char sid[64];
  ::sprintf(sid,"MCRichOpticalPhoton/0x%lx",(unsigned long)photon);

  SoSceneGraph* separator = new SoSceneGraph();
  separator->setString(sid);

  aSeparator->addChild(separator);

  separator->addChild(
    m_styleCache->getHighlightMaterial(qualities().color(),
                                       qualities().hcolor()));

  if ( markerStyle() == "line" ) 
  {

    // references to geometrical photon data
    const Gaudi::XYZPoint & emissPt   = photon->emissionPoint();
    const Gaudi::XYZPoint & mirrSp    = photon->sphericalMirrorReflectPoint();
    const Gaudi::XYZPoint & mirrFl    = photon->flatMirrorReflectPoint();
    const Gaudi::XYZPoint & detPt     = photon->pdIncidencePoint();

    //FIXME : the below is too veerbose. 
    // Should be in option. Ot done through the data-accessor dump system.
    //std::cout << *photon << std::endl;

    // ray trace the photon
    int32_t nPoints = 6;
    int32_t coordIndex[9];
    SbVec3f points[6]; // create with max possible size
    if ( mirrorPointOK(mirrSp) && mirrorPointOK(mirrFl) ) 
    {
      points[0].setValue( emissPt.x(), emissPt.y(), emissPt.z() );
      points[1].setValue( mirrSp.x(),  mirrSp.y(),  mirrSp.z()  );
      points[2].setValue( mirrSp.x(),  mirrSp.y(),  mirrSp.z()  );
      points[3].setValue( mirrFl.x(),  mirrFl.y(),  mirrFl.z()  );
      points[4].setValue( mirrFl.x(),  mirrFl.y(),  mirrFl.z()  );
      points[5].setValue( detPt.x(),   detPt.y(),   detPt.z()   );
      nPoints = 6;
      coordIndex[0] = m_icoord + 0;
      coordIndex[1] = m_icoord + 1;
      coordIndex[2] = SO_END_LINE_INDEX;
      coordIndex[3] = m_icoord + 2;
      coordIndex[4] = m_icoord + 3;
      coordIndex[5] = SO_END_LINE_INDEX;
      coordIndex[6] = m_icoord + 4;
      coordIndex[7] = m_icoord + 5;
      coordIndex[8] = SO_END_LINE_INDEX;
    } 
    else if ( mirrorPointOK(mirrSp) ) 
    {
      points[0].setValue( emissPt.x(), emissPt.y(), emissPt.z() );
      points[1].setValue( mirrSp.x(),  mirrSp.y(),  mirrSp.z()  );
      points[2].setValue( mirrSp.x(),  mirrSp.y(),  mirrSp.z()  );
      points[3].setValue( detPt.x(),   detPt.y(),   detPt.z()   );
      nPoints = 4;
      coordIndex[0] = m_icoord + 0;
      coordIndex[1] = m_icoord + 1;
      coordIndex[2] = SO_END_LINE_INDEX;
      coordIndex[3] = m_icoord + 2;
      coordIndex[4] = m_icoord + 3;
      coordIndex[5] = SO_END_LINE_INDEX;
    } 
    else if ( mirrorPointOK(mirrFl) ) 
    {
      points[0].setValue( emissPt.x(), emissPt.y(), emissPt.z() );
      points[1].setValue( mirrFl.x(),  mirrFl.y(),  mirrFl.z()  );
      points[2].setValue( mirrFl.x(),  mirrFl.y(),  mirrFl.z()  );
      points[3].setValue( detPt.x(),   detPt.y(),   detPt.z()   );
      nPoints = 4;
      coordIndex[0] = m_icoord + 0;
      coordIndex[1] = m_icoord + 1;
      coordIndex[2] = SO_END_LINE_INDEX;
      coordIndex[3] = m_icoord + 2;
      coordIndex[4] = m_icoord + 3;
      coordIndex[5] = SO_END_LINE_INDEX;
    }
    else
    {
      nPoints = 0;
    }

    if ( nPoints > 0 ) 
    {
      m_coordinate->point.setValues(m_icoord,nPoints,points);
      m_icoord += nPoints;

      SoIndexedLineSet* lineSet = new SoIndexedLineSet;
      lineSet->coordIndex.setValues(0,3*(nPoints/2),coordIndex);
      separator->addChild(lineSet);
    }
  }
}

// ============================================================================
