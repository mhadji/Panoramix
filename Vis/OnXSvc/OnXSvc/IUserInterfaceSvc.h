#ifndef IUserInterfaceSvc_h
#define IUserInterfaceSvc_h

// Inheritance :
#include <GaudiKernel/IService.h>

static const InterfaceID IID_IUserInterfaceSvc(345, 1, 0); 

class DataObject;
class ContainedObject;
class IAppMgrUI;

class SoPage;
class SoRegion;

#include <vector>

namespace Slash {namespace Core {class IWriter;}}
namespace Slash {namespace Core {class ISession;}}
namespace Slash {namespace Data {class IAccessor;}}
namespace Slash {namespace Data {class IProcessor;}}
namespace Slash {namespace UI {class IWidget;}}

namespace AIDA {class IHistogram;}

// LHCB :
#include <GaudiKernel/Point3DTypes.h> //Because ROOT::Math::XYZPoint is a typedef !

class IUserInterfaceSvc : virtual public IService {
public:
  virtual ~IUserInterfaceSvc(){}

  static const InterfaceID& interfaceID() { return IID_IUserInterfaceSvc; }

  virtual StatusCode visualize(const std::string&) = 0;
  virtual StatusCode visualize(const DataObject&) = 0;
  virtual StatusCode visualize(const AIDA::IHistogram&) = 0;
  virtual StatusCode visualize(const Gaudi::XYZPoint&) = 0;
  enum RepType {
    LINES = 0,
    POINTS = 1,
    SEGMENTS = 2,
    POLYGON = 3
  };
  virtual StatusCode visualize(const std::vector<Gaudi::XYZPoint>&,RepType = LINES) = 0;
  virtual std::vector<std::string> dataChildren(const std::string&) = 0; 
  virtual void ls(const std::string& path,int depth = -1) = 0;
  virtual bool writeToString(const std::string&,std::string&) = 0;
  virtual void nextEvent() = 0;
  virtual IService* getService(const std::string&) = 0;
  virtual void clearDetectorStore() = 0;
  virtual StatusCode changeGeometry(const std::string&) = 0;
  virtual StatusCode changeColors(const std::string&) = 0;
  virtual void openEventFile(const std::string&) = 0;
  virtual SoPage* currentSoPage() = 0;
  virtual SoRegion* currentSoRegion() = 0;
  virtual const std::string& cuts() const = 0;
  virtual Slash::UI::IWidget* currentWidget() = 0;
  virtual void setSession(Slash::Core::ISession*) = 0;
  virtual Slash::Core::ISession* session() = 0;
  virtual Slash::Core::IWriter& printer() = 0;
  virtual void addType(Slash::Data::IAccessor*) = 0;
  virtual Slash::Data::IProcessor* typeManager() = 0;
  virtual Slash::Data::IAccessor* metaType() = 0;
  // For Python :
  virtual std::string torgb(const std::string&) = 0;
  virtual IAppMgrUI* appMgr() const = 0;
  virtual void* topointer(const std::string&) const = 0;
  virtual std::vector<std::string> getHighlightedSoShapeNames() = 0;
  virtual std::vector<ContainedObject*> getHighlightedContainedObject() = 0;
};			

#endif
