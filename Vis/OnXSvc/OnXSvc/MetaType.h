#ifndef OnXSvc_MetaType_h
#define OnXSvc_MetaType_h

class IIntrospectionSvc;

#include <Lib/BaseType.h>

class MetaType : public Lib::BaseType {
public:
  MetaType(IPrinter&,IIntrospectionSvc*);
public: //Lib::IType
  virtual const std::string& name() const;
  virtual bool setName(const std::string&);
  virtual void setIterator(Lib::IIterator*);
  virtual Lib::IIterator* iterator();
  virtual Lib::Property* getProperties(int&);
  virtual Lib::Variable value(Lib::Identifier,const std::string&);
private:
  std::string fType;
  IIntrospectionSvc* fIntrospectionSvc;
  Lib::IIterator* fIterator;
};


#endif
