#ifndef OnXSvc_ISvcLocatorManager_h
#define OnXSvc_ISvcLocatorManager_h

class ISvcLocator;
class IService;

class ISvcLocatorManager {
public:
  virtual ~ISvcLocatorManager(){}
  virtual ISvcLocator* serviceLocator() const = 0;
  virtual IService* service(const std::string&) const = 0;
};

#endif
