#ifndef OnXSvc_Helpers_h
#define OnXSvc_Helpers_h

#include <OnX/Helpers/Inventor.h>
#include <HEPVis/nodekits/SoRegion.h>

//////////////////////////////////////////////////////////////////////////////
inline SoRegion* page_currentRegion(
 SoPage& aPage
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return aPage.currentRegion();
}

#define DYNAMIC_SCENE "dynamicScene"
#define STATIC_SCENE "staticScene"

//////////////////////////////////////////////////////////////////////////////
inline void region_addToDynamicScene(
 SoRegion& aRegion
,SoNode* aNode
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  aRegion.doIt(SbAddNode(aNode,DYNAMIC_SCENE));
}
//////////////////////////////////////////////////////////////////////////////
inline void region_addToStaticScene(
 SoRegion& aRegion
,SoNode* aNode
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  aRegion.doIt(SbAddNode(aNode,STATIC_SCENE));
}

#endif
