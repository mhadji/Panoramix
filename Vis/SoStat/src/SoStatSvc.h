#ifndef SoStat_SoStatSvc_h
#define SoStat_SoStatSvc_h

// Gaudi :
#include <GaudiKernel/Service.h>

class SoStatSvc : public Service { 
public: //IService
  virtual StatusCode initialize();
  virtual StatusCode finalize();
  SoStatSvc(const std::string&,ISvcLocator*);
  virtual ~SoStatSvc();
};

#endif
