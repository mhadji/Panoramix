################################################################################
# Package: SoDet
################################################################################
gaudi_subdir(SoDet v7r18)

gaudi_depends_on_subdirs(Det/DetDesc
                         Vis/OnXSvc)

find_package(OpenScientist REQUIRED COMPONENTS Vis)

if(GAUDI_HIDE_WARNINGS)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-overloaded-virtual")
endif()

gaudi_add_module(SoDet
                 src/SoDetDLL.cpp
                 src/SoDetSvc.cpp
                 src/Types.cpp
                 src/SoDetConverter.cpp
                 src/SoDetElemCnv.cpp
                 src/SoLVolumeCnv.cpp
                 INCLUDE_DIRS Vis/OnXSvc OpenScientist
                 LINK_LIBRARIES DetDescLib OpenScientist)

string(REPLACE "-Wsuggest-override" " " CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS}) 

