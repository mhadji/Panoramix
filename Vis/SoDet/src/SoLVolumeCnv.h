#ifndef SoDet_SoLVolumeCnv_h
#define SoDet_SoLVolumeCnv_h

#include "SoDetConverter.h"

class SoLVolumeCnv : public SoDetConverter {
public:
  SoLVolumeCnv(ISvcLocator*);
  // The next method should be replaced by one with a single arguement
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
