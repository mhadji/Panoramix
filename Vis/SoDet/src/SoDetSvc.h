#ifndef SoDet_SoDetSvc_h
#define SoDet_SoDetSvc_h

// Gaudi :
#include <GaudiKernel/Service.h>

class IUserInterfaceSvc;
class IMagneticFieldSvc;

class SoDetSvc : public Service {
public: //IService
  virtual StatusCode initialize();
  virtual StatusCode finalize();
  SoDetSvc(const std::string&,ISvcLocator*);
  virtual ~SoDetSvc();
private:
  IUserInterfaceSvc* m_uiSvc;
  IMagneticFieldSvc* m_magneticFieldSvc;
};

#endif
