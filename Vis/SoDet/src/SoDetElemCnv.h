#ifndef SoDet_SoDetElemCnv_h
#define SoDet_SoDetElemCnv_h

#include "GaudiKernel/MsgStream.h"
#include <memory>

#include "SoDetConverter.h"

class SoDetElemCnv : public SoDetConverter {
public:
  SoDetElemCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
private:
  mutable std::unique_ptr<MsgStream> fLog;
private:
  /// On-demand access to MsgStream object
  inline MsgStream & msgStream() const
  {
    if (!fLog) { fLog.reset( new MsgStream(msgSvc(),"SoDetElemCnv") ); }
    return *fLog;
  }
};

#endif
