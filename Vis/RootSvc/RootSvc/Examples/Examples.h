#ifndef RootSvc_Examples_h
#define RootSvc_Examples_h

#ifndef WIN32
#undef Status //TGeoNodeCache::Statue clashes with a Status CPP macro in X11.
#endif


#include <TGeoMatrix.h>
#include <TGeoVolume.h>
#include <TGeoManager.h>
#include <TGeoMaterial.h>
#include <TGeoArb8.h>

// Used by [Qt,Iv]Callbacks.cxx

//////////////////////////////////////////////////////////////////////////////
inline TGeoVolume* RootSvc_example_rootgeom(
)
//////////////////////////////////////////////////////////////////////////////
/// From ROOT/tutorials/rootgeom.C
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if (gGeoManager) delete gGeoManager;
  //--- Definition of a simple geometry
  new TGeoManager("simple1", "Simple geometry");
  //--- define some materials
  TGeoMaterial *matVacuum = new TGeoMaterial("Vacuum", 0,0,0);
  TGeoMaterial *matAl = new TGeoMaterial("Al", 26.98,13,2.7);
  //   //--- define some media
  TGeoMedium *Vacuum = new TGeoMedium("Vacuum",1, matVacuum);
  TGeoMedium *Al = new TGeoMedium("Root Material",2, matAl);
  
  //--- define the transformations
  TGeoTranslation *tr1 = new TGeoTranslation(20., 0, 0.);
  TGeoTranslation *tr2 = new TGeoTranslation(10., 0., 0.);
  TGeoTranslation *tr3 = new TGeoTranslation(10., 20., 0.);
  TGeoTranslation *tr4 = new TGeoTranslation(5., 10., 0.);
  TGeoTranslation *tr5 = new TGeoTranslation(20., 0., 0.);
  TGeoTranslation *tr6 = new TGeoTranslation(-5., 0., 0.);
  TGeoTranslation *tr7 = new TGeoTranslation(7.5, 7.5, 0.);
  TGeoRotation *rot1 = new TGeoRotation("rot1", 90., 0., 90., 270., 0., 0.);
  TGeoCombiTrans *combi1 = new TGeoCombiTrans(7.5, -7.5, 0., rot1);
  TGeoTranslation *tr8 = new TGeoTranslation(7.5, -5., 0.);
  TGeoTranslation *tr9 = new TGeoTranslation(7.5, 20., 0.);
  TGeoTranslation *tr10 = new TGeoTranslation(85., 0., 0.);
  TGeoTranslation *tr11 = new TGeoTranslation(35., 0., 0.);
  TGeoTranslation *tr12 = new TGeoTranslation(-15., 0., 0.);
  TGeoTranslation *tr13 = new TGeoTranslation(-65., 0., 0.);
  
  TGeoTranslation  *tr14 = new TGeoTranslation(0,0,-100);
  TGeoCombiTrans *combi2 = new TGeoCombiTrans(0,0,100, 
		new TGeoRotation("rot2",90,180,90,90,180,0));
  TGeoCombiTrans *combi3 = new TGeoCombiTrans(100,0,0, 
		new TGeoRotation("rot3",90,270,0,0,90,180));
  TGeoCombiTrans *combi4 = new TGeoCombiTrans(-100,0,0, 
		new TGeoRotation("rot4",90,90,0,0,90,0));
  TGeoCombiTrans *combi5 = new TGeoCombiTrans(0,100,0, 
	      new TGeoRotation("rot5",0,0,90,180,90,270));
  TGeoCombiTrans *combi6 = new TGeoCombiTrans(0,-100,0, 
                            new TGeoRotation("rot6",180,0,90,180,90,90));
 
  //--- make the top container volume
  //Double_t worldx = 110.;
  //Double_t worldy = 50.;
  //Double_t worldz = 5.;
  TGeoVolume *top = gGeoManager->MakeBox("TOP", Vacuum, 270., 270., 120.);
  gGeoManager->SetTopVolume(top);
  TGeoVolume *replica = gGeoManager->MakeBox("REPLICA", Vacuum,120,120,120);
  replica->SetVisibility(kFALSE);
  TGeoVolume *rootbox = gGeoManager->MakeBox("ROOT", Vacuum, 110., 50., 5.);
  rootbox->SetVisibility(kFALSE);
  
  //--- make letter 'R'
  TGeoVolume *R = gGeoManager->MakeBox("R", Vacuum, 25., 25., 5.);
  R->SetVisibility(kFALSE);
  TGeoVolume *bar1 = gGeoManager->MakeBox("bar1", Al, 5., 25, 5.);
  bar1->SetLineColor(kRed);
  R->AddNode(bar1, 1, tr1);
  TGeoVolume *bar2 = gGeoManager->MakeBox("bar2", Al, 5., 5., 5.);
  bar2->SetLineColor(kRed);
  R->AddNode(bar2, 1, tr2);
  R->AddNode(bar2, 2, tr3);
  TGeoVolume *tub1 = gGeoManager->MakeTubs("tub1", Al, 5., 15., 5., 90., 270.);
  tub1->SetLineColor(kRed);
  R->AddNode(tub1, 1, tr4);
  TGeoVolume *bar3 = gGeoManager->MakeArb8("bar3", Al, 5.);
  bar3->SetLineColor(kRed);
  TGeoArb8 *arb = (TGeoArb8*)bar3->GetShape();
  arb->SetVertex(0, 15., -5.);
  arb->SetVertex(1, 5., -5.);
  arb->SetVertex(2, -10., -25.);
  arb->SetVertex(3, 0., -25.);
  arb->SetVertex(4, 15., -5.);
  arb->SetVertex(5, 5., -5.);
  arb->SetVertex(6, -10., -25.);
  arb->SetVertex(7, 0., -25.);
  R->AddNode(bar3, 1, gGeoIdentity);
  
  //--- make letter 'O'
  TGeoVolume *O = gGeoManager->MakeBox("O", Vacuum, 25., 25., 5.);
  O->SetVisibility(kFALSE);
  TGeoVolume *bar4 = gGeoManager->MakeBox("bar4", Al, 5., 7.5, 5.);
  bar4->SetLineColor(kYellow);
  O->AddNode(bar4, 1, tr5);
  O->AddNode(bar4, 2, tr6);
  TGeoVolume *tub2 = gGeoManager->MakeTubs("tub1", Al, 7.5, 17.5, 5., 0., 180.);
  tub2->SetLineColor(kYellow);
  O->AddNode(tub2, 1, tr7);
  O->AddNode(tub2, 2, combi1);
  
  //--- make letter 'T'
  TGeoVolume *T = gGeoManager->MakeBox("T", Vacuum, 25., 25., 5.);
  T->SetVisibility(kFALSE);
  TGeoVolume *bar5 = gGeoManager->MakeBox("bar5", Al, 5., 20., 5.);
  bar5->SetLineColor(kBlue);
  T->AddNode(bar5, 1, tr8);
  TGeoVolume *bar6 = gGeoManager->MakeBox("bar6", Al, 17.5, 5., 5.);
  bar6->SetLineColor(kBlue);
  T->AddNode(bar6, 1, tr9);
  
  rootbox->AddNode(R, 1, tr10);
  rootbox->AddNode(O, 1, tr11);
  rootbox->AddNode(O, 2, tr12);
  rootbox->AddNode(T, 1, tr13);
 
  replica->AddNode(rootbox, 1, tr14);
  replica->AddNode(rootbox, 2, combi2);
  replica->AddNode(rootbox, 3, combi3);
  replica->AddNode(rootbox, 4, combi4);
  replica->AddNode(rootbox, 5, combi5);
  replica->AddNode(rootbox, 6, combi6);

  top->AddNode(replica, 1, new TGeoTranslation(-150, -150, 0));
  top->AddNode(replica, 2, new TGeoTranslation(150, -150, 0));
  top->AddNode(replica, 3, new TGeoTranslation(150, 150, 0));
  top->AddNode(replica, 4, new TGeoTranslation(-150, 150, 0));
    
  //--- close the geometry
  gGeoManager->CloseGeometry();
    
  return top;
}   

#include <TStyle.h>
#include <TPaveText.h>
#include <TPad.h>
#include <TF2.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TApplication.h>


//////////////////////////////////////////////////////////////////////////////
inline void RootSvc_example_surfaces(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  gROOT->Reset();
  //c1 = new TCanvas("c1","Surfaces Drawing Options",200,10,700,900);
  gStyle->SetFrameFillColor(42);
  TPaveText* title = new TPaveText(.2,0.96,.8,.995);
  title->SetFillColor(33);
  title->AddText("Examples of Surface options");
  title->Draw();

  TPad* pad1 = new TPad("pad1","Gouraud shading",0.03,0.50,0.98,0.95,21);
  TPad* pad2 = new TPad("pad2","Color mesh",0.03,0.02,0.98,0.48,21);
  pad1->Draw();
  pad2->Draw();
  //
  // We generate a 2-D function
  TF2* f2 = new TF2("f2","x**2 + y**2 - x**3 -8*x*y**4",-1,1.2,-1.5,1.5);
  f2->SetContour(48);
  f2->SetFillColor(45);

  // Draw this function in pad1 with Gouraud shading option
  pad1->cd();
  pad1->SetPhi(-80);
  pad1->SetLogz();
  f2->Draw("surf4");

  // Draw this function in pad2 with color mesh option
  pad2->cd();
  pad2->SetTheta(25);
  pad2->SetPhi(-110);
  pad2->SetLogz();
  f2->Draw("surf1");
}

#include <TGraph.h>

//////////////////////////////////////////////////////////////////////////////
inline void RootSvc_example_graph(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  float x[3] = {1,2,3};
  float y[3] = {1.5, 3.0, 4.5};
  TGraph* graph  = new TGraph(3,x,y);
  graph->SetMarkerStyle(20);

  graph->Draw("AP");
}

#include <TRandom.h>
#include <TH1.h>

//////////////////////////////////////////////////////////////////////////////
inline TH1* RootSvc_example_histogram(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  TH1* histogram = new TH1D("hGauss","Random gauss",100,-5,5);
  for(int count=0;count<10000;count++) histogram->Fill(gRandom->Gaus(0,1));
  histogram->Draw();
  return histogram;
}

#endif
