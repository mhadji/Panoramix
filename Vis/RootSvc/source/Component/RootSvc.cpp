#include "RootSvc.h"

#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/System.h>

DECLARE_COMPONENT( RootSvc )

RootSvc::RootSvc(const std::string& aName,ISvcLocator* aSvcLoc)
:Service(aName,aSvcLoc){}

RootSvc::~RootSvc(){}

StatusCode RootSvc::queryInterface(const InterfaceID& aRIID,void** aPPVIF){
  if(aRIID==IID_IRootSvc) {
    *aPPVIF = static_cast<IRootSvc*>(this);
    addRef();
    return StatusCode::SUCCESS;
  } else {
    return Service::queryInterface(aRIID, aPPVIF);
  }
}

StatusCode RootSvc::initialize(){
  StatusCode status = Service::initialize();
  if( status.isFailure() ) return status;

  MsgStream log(msgSvc(), Service::name());
  log << MSG::DEBUG << "initialize" << endmsg;

  setProperties();
  return StatusCode::SUCCESS;
}

StatusCode RootSvc::finalize() {
  MsgStream log(msgSvc(), Service::name());
  log << MSG::DEBUG << "RootSvc finalized successfully" << endmsg;
  return Service::finalize();
}

#include <AIDA/IHistogram.h>
#include <TVirtualPad.h>
#include <TH1D.h>
#include <GaudiKernel/HistogramBase.h>

StatusCode RootSvc::visualize(const AIDA::IHistogram& aHistogram){
  MsgStream log(msgSvc(), Service::name());
  const Gaudi::HistogramBase* pih =
      dynamic_cast<const Gaudi::HistogramBase*>(&aHistogram);
  if( pih ) {
    TH1* th = dynamic_cast<TH1*>(pih->representation());
    if(th) {
      th->Draw();
      if(gPad) gPad->Update();
      return StatusCode::SUCCESS;
    }
  }
  log << MSG::ERROR << " histogram is not a GaudiPI one." << endmsg;
  return StatusCode::FAILURE;
}
