// this :
#include "MCHitType.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Variable.h"
#include "Lib/Out.h"

// Gaudi :
#include "Kernel/IParticlePropertySvc.h"   
#include "GaudiKernel/ISvcLocator.h"            
#include "GaudiKernel/IDataProviderSvc.h"       
#include "Kernel/ParticleProperty.h"
#include "GaudiKernel/SmartDataPtr.h"           

#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ISoConversionSvc.h"

// Event model :
#include "Event/MCHit.h"

//////////////////////////////////////////////////////////////////////////////
MCHitType::MCHitType(
 IUserInterfaceSvc* aUISvc
,ISoConversionSvc* aSoCnvSvc
,IDataProviderSvc* aDataProviderSvc
,LHCb::IParticlePropertySvc* aParticlePropertySvc
,const std::string& aDetector
)
:OnX::BaseType(aUISvc->printer())
,fType("") // See below.
,fDetector(aDetector)
,fIterator(0)
,fUISvc(aUISvc)
,fSoCnvSvc(aSoCnvSvc)
,fDataProviderSvc(aDataProviderSvc)
,fParticlePropertySvc(aParticlePropertySvc)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  fType = "MC" + fDetector + "Hit";
  addProperty("particle",Lib::Property::STRING);
  addProperty("energy",Lib::Property::DOUBLE);
  addProperty("x_entry",Lib::Property::DOUBLE);
  addProperty("y_entry",Lib::Property::DOUBLE);
  addProperty("z_entry",Lib::Property::DOUBLE);
  addProperty("x_exit",Lib::Property::DOUBLE);
  addProperty("y_exit",Lib::Property::DOUBLE);
  addProperty("z_exit",Lib::Property::DOUBLE);
  addProperty("address",Lib::Property::POINTER);
}
//////////////////////////////////////////////////////////////////////////////
std::string MCHitType::name() const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fType;
}
//////////////////////////////////////////////////////////////////////////////
void MCHitType::setIterator(
 Lib::IIterator* aIterator
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  fIterator = aIterator;
}
//////////////////////////////////////////////////////////////////////////////
Lib::IIterator* MCHitType::iterator(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(fIterator) return fIterator;
  if(!fDataProviderSvc) return 0;
  std::string location = "/Event/"+LHCb::MCParticleLocation::Default;
  SmartDataPtr<DataObject> smartDataObject(fDataProviderSvc,location);
  if(smartDataObject) {}
  DataObject* dataObject;
  std::string s = "/Event/MC/"+fDetector+"/Hits";
  StatusCode sc = fDataProviderSvc->retrieveObject(s, dataObject);
  if(!sc.isSuccess()) return 0;
  LHCb::MCHits* collection = dynamic_cast<LHCb::MCHits*>(dataObject);
  if(!collection) return 0;

  class Iterator : public Lib::IIterator {
  public: //Lib::IIterator
    virtual Lib::Identifier object() {
      if(fIterator==fVector.end()) return 0;
      return *fIterator;
    }
    virtual void next() { ++fIterator;}
    virtual void* tag() {return 0;}
  public:
    Iterator(LHCb::MCHits& aVector):fVector(aVector) {
      fIterator = fVector.begin();
    }
  private:
    LHCb::MCHits& fVector;
    LHCb::MCHits::iterator fIterator;
  };

  return new Iterator(*collection);

}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable MCHitType::value(
 Lib::Identifier aIdentifier
,const std::string& aName
,void*
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::MCHit* obj = (LHCb::MCHit*)aIdentifier;
  if(aName=="address") {
    return Lib::Variable(printer(),(void*)obj);
  } else if(aName=="energy") {
    return Lib::Variable(printer(),obj->energy());
  } else if(aName=="particle") {
    if(fParticlePropertySvc) {
      const LHCb::MCParticle* mcParticle = obj->mcParticle();
      if(mcParticle) {
        const LHCb::ParticleProperty* pp = fParticlePropertySvc->find
          (mcParticle->particleID());
        if(pp) return Lib::Variable(printer(),pp->particle());
      }
    }
    return Lib::Variable(printer(),std::string("nil"));
  } else if(aName=="x_entry") {
    return Lib::Variable(printer(),obj->entry().x());
  } else if(aName=="y_entry") {
    return Lib::Variable(printer(),obj->entry().y());
  } else if(aName=="z_entry") {
    return Lib::Variable(printer(),obj->entry().z());
  } else if(aName=="x_exit") {
    return Lib::Variable(printer(),obj->exit().x());
  } else if(aName=="y_exit") {
    return Lib::Variable(printer(),obj->exit().y());
  } else if(aName=="z_exit") {
    return Lib::Variable(printer(),obj->exit().z());
  } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
void MCHitType::visualize(
 Lib::Identifier aIdentifier
,void*
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!aIdentifier) return;
  if(!fSoCnvSvc) return;

  LHCb::MCHit* object = (LHCb::MCHit*)aIdentifier;

  // Build a LHCb::MCHits with one entry.
  // A LHCb::MCHits is the thing known by the SoConversionSvc.
  LHCb::MCHits* collection = new LHCb::MCHits;
  collection->add(object);

  // Convert it :
  IOpaqueAddress* addr = 0;
  StatusCode sc = fSoCnvSvc->createRep(collection, addr);
  if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,collection);

  collection->remove(object); // Else the next line delete the MCHit.
  delete collection;

}
