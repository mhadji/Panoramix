#ifndef SoEvent_SoVPClusterCnv_h
#define SoEvent_SoVPClusterCnv_h

#include "SoEventConverter.h"

class SoVPClusterCnv : public SoEventConverter {
public:
  SoVPClusterCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
