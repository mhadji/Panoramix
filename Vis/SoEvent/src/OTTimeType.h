#ifndef SoEvent_OTTimeType_h
#define SoEvent_OTTimeType_h

// Inheritance :
#include "Type.h"

#include "Event/OTTime.h" //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;

class OTTimeType : public SoEvent::Type<LHCb::OTTime> {
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public: //OnX::IType
  virtual void visualize(Lib::Identifier,void*);
public:
  OTTimeType(IUserInterfaceSvc*,ISoConversionSvc*,IDataProviderSvc*);
private:
  void visualizeMCParticle(LHCb::OTTime&);
};

#endif
