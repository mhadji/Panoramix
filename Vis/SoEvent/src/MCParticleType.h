#ifndef SoEvent_MCParticleType_h
#define SoEvent_MCParticleType_h

// Inheritance :
#include "Type.h"
#include "Kernel/IParticlePropertySvc.h"

#include "Event/MCParticle.h" //The data.

class IDataProviderSvc;
class IUserInterfaceSvc;
class IParticlePropertySvc;
class ISoConversionSvc;
namespace LHCb {
    class Track;
}

class MCParticleType : public SoEvent::Type<LHCb::MCParticle> {
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public: //OnX::IType
  virtual void visualize(Lib::Identifier,void*);
public:
  MCParticleType(IUserInterfaceSvc*,ISoConversionSvc*,IDataProviderSvc*,
         LHCb::IParticlePropertySvc*);
private:
  void visualizeTrack(LHCb::MCParticle&);
  void visualizeClusters(LHCb::MCParticle&);
  void visualizeHits(LHCb::MCParticle&);
private:
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
};

#endif
