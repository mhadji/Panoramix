#ifndef SoEvent_SoMCHitCnv_h
#define SoEvent_SoMCHitCnv_h

#include "SoEventConverter.h"

class SoMCHitCnv : public SoEventConverter
{
public:
  SoMCHitCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
