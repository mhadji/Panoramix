#ifndef SoEvent_VeloClusterType_h
#define SoEvent_VeloClusterType_h

// Inheritance :
#include "Type.h"

#include "Event/VeloCluster.h" //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class DeVelo;

class VeloClusterType : public SoEvent::Type<LHCb::VeloCluster> {
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public: //OnX::IType
  virtual void visualize(Lib::Identifier,void*);
public:
  VeloClusterType(IUserInterfaceSvc*,ISoConversionSvc*,IDataProviderSvc*,
          IDataProviderSvc*);
private:
  DeVelo * deVelo() const;
private:
  IDataProviderSvc* fDetectorDataSvc;
  void visualizeMCParticle(LHCb::VeloCluster&);
};

#endif
