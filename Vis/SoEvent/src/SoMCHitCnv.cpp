// To fix clashes between Gaudi and Windows :
#include "OnXSvc/Win32.h"

// this :
#include "SoMCHitCnv.h"

// Inventor :
#include "Inventor/nodes/SoSeparator.h"
#include "Inventor/nodes/SoLightModel.h"
#include "Inventor/nodes/SoDrawStyle.h"
#include "Inventor/nodes/SoCoordinate3.h"
#include "Inventor/nodes/SoTransform.h"
#include "Inventor/nodes/SoLineSet.h"

// SoUtils :
#include "SoUtils/SbProjector.h"

// HEPVis :
#include "HEPVis/nodes/SoSceneGraph.h"
#include "HEPVis/SbPolyhedron.h"
#include "HEPVis/nodes/SoPolyhedron.h"
#include "HEPVis/misc/SoStyleCache.h"
#include "HEPVis/nodes/SoHighlightMaterial.h"
#include "HEPVis/nodes/SoMarkerSet.h"
typedef HEPVis_SoMarkerSet SoMarkerSet;

// Lib :
#include "Lib/smanip.h"
#include "Lib/Interfaces/ISession.h"

// Gaudi :
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"

// LHCb :
#include "Event/MCHit.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ClassID.h"
#include "OnXSvc/Helpers.h"

DECLARE_COMPONENT( SoMCHitCnv )

//////////////////////////////////////////////////////////////////////////////
SoMCHitCnv::SoMCHitCnv(
                       ISvcLocator* aSvcLoc
                       )
  :SoEventConverter(aSvcLoc,SoMCHitCnv::classID())
                      //////////////////////////////////////////////////////////////////////////////
                      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoMCHitCnv::createRep(
                                 DataObject* aObject
                                 ,IOpaqueAddress*& aAddr
                                 )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log(msgSvc(), "SoMCHitCnv");
  //log << MSG::INFO << "MC createReps" << endmsg;

  if(!fUISvc) {
    log << MSG::INFO << " UI service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  ISession* session = fUISvc->session();
  if(!session) {
    log << MSG::INFO << " can't get ISession." << endmsg;
    return StatusCode::FAILURE;
  }

  SoRegion* region = 0;
  if(aAddr) {
    // Optimization.
    // If having a not null aAddr, we expect a SoRegion.
    // See SoEvent/Type.h/SoEvent::Type<>::beginVisualize.
    region = (SoRegion*)aAddr;
  } else {
    region = fUISvc->currentSoRegion();
  }
  if(!region) {
    log << MSG::INFO << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE;
  }

  if(!aObject) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE;
  }

  LHCb::MCHits* hits = dynamic_cast<LHCb::MCHits*>(aObject);
  if(!hits) {
    log << MSG::INFO << " bad object type." << endmsg;
    return StatusCode::FAILURE;
  }

  if(!hits->size()) {
    log << MSG::INFO << " collection is empty." << endmsg;
    return StatusCode::SUCCESS;
  }

  // Get modeling parameters :
  std::string value;
  double r = 0.5, g = 0.5, b = 0.5; // grey, visible on black or white back.
  if(session->parameterValue("modeling.color",value))
    Lib::smanip::torgb(value,r,g,b);
  double hr = 1.0, hg = 1.0, hb = 0.0;
  if(session->parameterValue("modeling.highlightColor",value))
    Lib::smanip::torgb(value,hr,hg,hb);
  double lineWidth = 0;
  if(session->parameterValue("modeling.lineWidth",value))
    if(!Lib::smanip::todouble(value,lineWidth)) lineWidth = 0;
  // Non linear projections :
  session->parameterValue("modeling.projection",value);
  log << MSG::INFO << " projector = " << value.c_str() << endmsg;
  SoUtils::SbProjector projector(value.c_str());

  SoStyleCache* styleCache = region->styleCache();
  SoLightModel* lightModel = styleCache->getLightModelBaseColor();
  SoDrawStyle* drawStyle =
    styleCache->getLineStyle(SbLinePattern_solid,float(lineWidth));
  SoMaterial* highlightMaterial =
    styleCache->getHighlightMaterial(float(r),float(g),float(b),
                                     float(hr),float(hg),float(hb),0,TRUE);

  std::string shape = "marker";
  if(!session->parameterValue("modeling.shape",shape)) shape = "marker";

  SoSceneGraph* separator = new SoSceneGraph;
  separator->setString("MCHits");

  // Material :
  separator->addChild(highlightMaterial);

  separator->addChild(lightModel);

  separator->addChild(drawStyle);

  if(shape=="line") {

    // For each hit, model with a line from hit.entry to hit.exit :

    int segmentn = hits->size();
    SbVec3f* points = new SbVec3f[2 * segmentn];
    int32_t pointn = 0;

    for(LHCb::MCHits::iterator it = hits->begin(); it != hits->end(); it++) {
      Gaudi::XYZPoint p1 = (*it)->entry();
      points[pointn].setValue((float)p1.x(),(float)p1.y(),(float)p1.z());
      pointn++;
      Gaudi::XYZPoint p2 = (*it)->exit();
      points[pointn].setValue((float)p2.x(),(float)p2.y(),(float)p2.z());
      pointn++;
    }

    SoCoordinate3* coordinate3 = new SoCoordinate3;
    projector.project(pointn,points);
    coordinate3->point.setValues(0,pointn,points);
    separator->addChild(coordinate3);

    int32_t* vertices = new int32_t [segmentn];
    for (int count=0;count<segmentn;count++) {
      vertices[count] = 2;
    }
    SoLineSet* lineSet = new SoLineSet;
    lineSet->numVertices.setValues(0,segmentn,vertices);
    separator->addChild(lineSet);

    delete [] vertices;
    delete [] points;

  } else if (shape=="marker") {

    // For each hit, model with a mark centered at "hit.entry"
    // this mark is supposed to have a constant size independent
    // of the scaling of the scene


    int segmentn = hits->size();
    SbVec3f* points = new SbVec3f[segmentn];
    int32_t pointn = 0;

    for(LHCb::MCHits::iterator it = hits->begin(); it != hits->end(); it++) {
      Gaudi::XYZPoint p1 = (*it)->entry();
      points[pointn].setValue((float)p1.x(),(float)p1.y(),(float)p1.z());
      pointn++;
    }

    SoCoordinate3* coordinate3 = new SoCoordinate3;
    projector.project(pointn,points);
    coordinate3->point.setValues(0,pointn,points);
    separator->addChild(coordinate3);
    delete [] points;

    SoMarkerSet* markerSet = new SoMarkerSet;
    markerSet->numPoints = pointn;
    markerSet->markerIndex = SoMarkerSet::DIAMOND_FILLED_7_7;
    separator->addChild(markerSet);

  } else if(shape=="box") {

    // Model each hit with a box centered at "hit.entry".

    bool sizeFixed = true;
    if(session->parameterValue("modeling.sizeFixed",value))
      if(!Lib::smanip::tobool(value,sizeFixed)) sizeFixed = true;

    double scale = 1;
    session->parameterValue("modeling.scale",value);
    if(!Lib::smanip::todouble(value,scale)) scale = 1;

    for(LHCb::MCHits::iterator it = hits->begin(); it != hits->end(); it++) {

      double energy = (*it)->energy();

      Gaudi::XYZPoint p1 = (*it)->entry();

      SoTransform* tsf = new SoTransform;
      tsf->translation.setValue
        (SbVec3f((float)p1.x(),(float)p1.y(),(float)p1.z()));
      separator->addChild(tsf);

      double halfSize = sizeFixed ? scale : scale * energy;

      SbPolyhedron* sbPol = new SbPolyhedronBox(halfSize,halfSize,halfSize);
      SoPolyhedron* polyhedron = new SoPolyhedron(*sbPol);
      delete sbPol;
      polyhedron->solid.setValue(FALSE);
      separator->addChild(polyhedron);

    }

  }

  //  Send scene graph to the viewing region
  // (in the "dynamic" sub-scene graph) :
  region_addToDynamicScene(*region,separator);

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoMCHitCnv::classID(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return LHCb::MCHits::classID();
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoMCHitCnv::storageType(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return So_TechnologyType;
}
