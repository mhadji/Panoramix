// this :
#include "TrackType.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Variable.h"
#include "Lib/smanip.h"
#include "Lib/Out.h"

// Gaudi :
#include "GaudiKernel/IParticlePropertySvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"

#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ISoConversionSvc.h"

// Event model
#include "Event/MCParticle.h"
#include "Event/MCRichSegment.h"
#include "Event/Measurement.h"
#include "Event/VeloCluster.h"
#include "Event/VPCluster.h"
#include "Event/STCluster.h"
#include "Event/VeloRMeasurement.h"
#include "Event/VeloPhiMeasurement.h"
#include "Event/STMeasurement.h"
#include "Event/OTMeasurement.h"
#include "Event/MuonCoord.h"
#include "Kernel/MuonTileID.h"
#include "Event/OTTime.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "Event/RecVertex.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "STDet/DeSTDetector.h"
#include "OTDet/DeOTDetector.h"
#include "DetDesc/LVolume.h"

// MC relations:
#include "Linker/LinkedTo.h"
#include "Linker/LinkedFrom.h"
#include "Linker/LinkerWithKey.h"

//////////////////////////////////////////////////////////////////////////////
TrackType::TrackType( IUserInterfaceSvc* aUISvc
                      ,ISoConversionSvc* aSoCnvSvc
                      ,IDataProviderSvc* aDataProviderSvc
                      ,IToolSvc*    aToolSvc
                      ,MsgStream&   aMsgStream )
  :SoEvent::Type<LHCb::Track>( LHCb::Tracks::classID(),
                               "Track",
                               "", //No location. Set in iterator method.
                               aUISvc,aSoCnvSvc,aDataProviderSvc),
   m_toolSvc(aToolSvc),
   m_msgStream(aMsgStream)
{
  addProperty("key",Lib::Property::INTEGER);
  addProperty("charge",Lib::Property::DOUBLE);
  addProperty("pt",Lib::Property::DOUBLE);
  addProperty("chi2dof",Lib::Property::DOUBLE);
  addProperty("tx",Lib::Property::DOUBLE);
  addProperty("ty",Lib::Property::DOUBLE);
  addProperty("velor",Lib::Property::BOOLEAN);
  addProperty("velo",Lib::Property::BOOLEAN);
  addProperty("upstream",Lib::Property::BOOLEAN);
  addProperty("unique",Lib::Property::BOOLEAN);
  addProperty("ttrack",Lib::Property::BOOLEAN);
  addProperty("downstream",Lib::Property::BOOLEAN);
  addProperty("long",Lib::Property::BOOLEAN);
  addProperty("backward",Lib::Property::BOOLEAN);
  addProperty("match",Lib::Property::BOOLEAN);
  addProperty("muon",Lib::Property::BOOLEAN);
  addProperty("nstate",Lib::Property::INTEGER);
  addProperty("type",Lib::Property::INTEGER);
  addProperty("veloTTITOT",Lib::Property::INTEGER);
  addProperty("pvass",Lib::Property::INTEGER);
  addProperty("address",Lib::Property::POINTER);
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable TrackType::value( Lib::Identifier aIdentifier
                                ,const std::string& aName
                                ,void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{

  IHitExpectation* m_itExpectation;
  IHitExpectation* m_otExpectation;
  IDataProviderSvc*  detsvc   = DetDesc::services()->detSvc();
  SmartDataPtr<const ILVolume> it(detsvc,DeSTDetLocation::location("IT")) ;
  SmartDataPtr<const ILVolume> ot(detsvc,DeOTDetectorLocation::Default) ;
// not used for the moment
//  bool ITexist = true;
//  if (0==it){ITexist = false;}
//  bool OTexist = true;
//  if (0==ot){OTexist = false;}
  
  if (it){
   StatusCode sc = m_toolSvc->retrieveTool("ITHitExpectation", m_itExpectation);
   if (sc.isFailure()){
    m_msgStream << MSG::ERROR << "Unable to retrieve ITHitExpectation" << endmsg;
   }
  }
  if (ot){
   StatusCode sc = m_toolSvc->retrieveTool("OTHitExpectation", m_otExpectation);
   if (sc.isFailure()){
    m_msgStream << MSG::ERROR << "Unable to retrieve OTHitExpectation" << endmsg;
   }
  }


  LHCb::Track* obj = (LHCb::Track*)aIdentifier;

  if(aName=="address") {
    return Lib::Variable(printer(),(void*)obj);
  } else if(aName=="charge") {
    return Lib::Variable(printer(),obj->charge());
  } else if(aName=="tx") {
    return Lib::Variable(printer(),obj->firstState().tx() );
  } else if(aName=="ty") {
    return Lib::Variable(printer(),obj->firstState().ty() );
  } else if(aName=="pt") {
    return Lib::Variable(printer(), obj->pt() );
  } else if(aName=="chi2dof") {
    return Lib::Variable(printer(), obj->chi2PerDoF() );
  } else if(aName=="unique") {
    return Lib::Variable(printer(),(bool)(obj->checkFlag(LHCb::Track::Clone)?false:true));
  } else if(aName=="velo")   {
    return Lib::Variable(printer(),(bool)(obj->checkType(LHCb::Track::Velo)?true:false));
  } else if(aName=="velor")   {
    return Lib::Variable(printer(),(bool)(obj->checkType(LHCb::Track::VeloR)?true:false));
  } else if(aName=="upstream") {
    return Lib::Variable(printer(),(bool)(obj->checkType(LHCb::Track::Upstream)?true:false));
  } else if(aName=="ttrack") {
    return Lib::Variable(printer(),(bool)(obj->checkType(LHCb::Track::Ttrack)?true:false));
  } else if(aName=="long") {
    return Lib::Variable(printer(),(bool)(obj->checkType(LHCb::Track::Long)?true:false));
  } else if(aName=="downstream") {
    return Lib::Variable(printer(),(bool)(obj->checkType(LHCb::Track::Downstream)?true:false));
  } else if(aName=="match") {
    return Lib::Variable(printer(),(bool)(obj->checkHistory(LHCb::Track::TrackMatching)?true:false));
  } else if(aName=="muon") {
    return Lib::Variable(printer(),(bool)(obj->checkType(LHCb::Track::Muon)?true:false));
  } else if(aName=="backward") {
    return Lib::Variable(printer(),(bool)(obj->checkFlag(LHCb::Track::Backward)?true:false));
  } else if(aName=="nstate") {
    return Lib::Variable(printer(),(int) (obj->nStates() ) );
  } else if(aName=="type") {
    return Lib::Variable(printer(),(int) (obj->type() ) );
  } else if(aName=="key") {
    return Lib::Variable(printer(),(int) (obj->key() ) );
  } else if(aName=="pvass") {
    DataObject* dataObject;
    bool matched = false;
    StatusCode sc = fDataProviderSvc->retrieveObject(LHCb::RecVertexLocation::Primary, dataObject);
    if (sc.isFailure()){
      m_msgStream << MSG::ERROR << "Unable to retrieve Primary Vertex" << endmsg;
      return Lib::Variable(printer(), (bool) (matched) );
    }
    LHCb::RecVertices* vertices = dynamic_cast<LHCb::RecVertices*>(dataObject);
    LHCb::RecVertices::iterator it;
    for(it = vertices->begin(); it != vertices->end(); it++) {
      SmartRefVector< LHCb::Track >  vtx_tracks = (*it)->tracks();
      //looping over tracks of this PV
      for (unsigned int itt = 0; itt < vtx_tracks.size(); itt++ ) {
        const LHCb::Track* ptr = vtx_tracks.at(itt);
        if (ptr->key() == obj->key() ) {
          matched = true;
          break;  }
        if (matched) {break;}
      }
    }
    return Lib::Variable(printer(), (bool) (matched) );
  } else if(aName=="veloTTITOT") {
    int flag = 0;
    int ithits = 0;
    int othits = 0;
    std::vector<LHCb::LHCbID > ids;
    if (obj->hasVelo()) {flag+=1000;}
    if (obj->hasTT()) {flag+=100;}
    if (obj->hasT()) {
      // this does not yet work, will work with future release of track event model
      //      ithits = obj->info(LHCb::Track::nExpectedIT,0.);
      //      othits = obj->info(LHCb::Track::nExpectedOT,0.);
      if (it) {
       m_itExpectation->collect(*obj, ids);
       ithits = ids.size();
      }
      if (ot){  
       m_otExpectation->collect(*obj, ids);
       othits = ids.size();
      }  
      if (ithits>0 ) {flag+=10;}
      if (othits>0 ) {flag+=1;}
    }
    return Lib::Variable(printer(),flag);
  } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
void TrackType::visualize( Lib::Identifier aIdentifier
                           ,void* aData )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if ( !aIdentifier || !fUISvc || !fUISvc->session() ) return;

  //FIXME : below location logic not needed. Done in the iterator()
  std::string value;

  // Get location :
  bool t = fUISvc->session()->parameterValue("Track.location",value);
  if( !t || (value=="")) { value = LHCb::TrackLocation::Default; }
  setLocation(value);

  // cast to track
  LHCb::Track * object = (LHCb::Track*)aIdentifier;
  if ( !object ) return;

  fUISvc->session()->parameterValue("modeling.what",value);
  if(value=="clusters")
  {
    visualizeMeasurements(*object);
  }
  else if(value=="MCParticle")
  {
    visualizeMCParticle(*object);
  }
  else if(value=="ChargeConjugate")
  {
    visualizeChargeConjugate(*object);
  }
  else if(value=="ReFit")
  {
    // get fit tool
    ITrackFitter* TrackMasterFitter;
    StatusCode sc = m_toolSvc->retrieveTool("TrackMasterFitter", TrackMasterFitter);
    if (sc.isFailure()){
      m_msgStream << MSG::ERROR << "Unable to retrieve TrackMasterFitter" << endmsg;
    }
    TrackMasterFitter->operator()(*object);
    this->SoEvent::Type<LHCb::Track>::visualize(aIdentifier,aData);
  }
  else
  {
    this->SoEvent::Type<LHCb::Track>::visualize(aIdentifier,aData);
  }

}
//////////////////////////////////////////////////////////////////////////////
void TrackType::visualizeMeasurements( LHCb::Track& aTrack ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  DataObject* dataObject;
  const std::vector<LHCb::LHCbID>& meas = aTrack.lhcbIDs();
  // Get location :
  const ObjectContainerBase* parentname = aTrack.parent();
  const std::string & container = parentname->registry()->identifier();   //finds name and directory of track
  int a = container.find("Rec");
  std::string slot = "";
  if (a != -1) {
    slot = container.substr(0,a);
  }
  StatusCode sc;
  LHCb::VeloClusters vClusters;
  LHCb::VPClusters   vpClusters;
  LHCb::STClusters   stClusters;
  LHCb::OTTimes      otTimes;
  LHCb::MuonCoords   muonCoords;
  std::string debug;
  fUISvc->session()->parameterValue("DEBUG",debug);

  for ( unsigned int index = 0; index < meas.size(); index++) {
    // FIXME : is it possible to be generic ?
    if( meas[index].isVelo())
    {
      LHCb::VeloChannelID vid = meas[index].veloID();
      sc = fDataProviderSvc->retrieveObject(slot+"Raw/Velo/Clusters", dataObject);
      if ( sc.isSuccess() )
      {
        LHCb::VeloCluster* obj =
          dynamic_cast<LHCb::VeloCluster*>(dynamic_cast<ObjectContainerBase*>(dataObject)->containedObject(vid.channelID()));
        if ( obj ) vClusters.add(obj);
      }
    }
    if( meas[index].isVP())
    {
      LHCb::VPChannelID vid = meas[index].vpID();
      sc = fDataProviderSvc->retrieveObject(slot+"Raw/VP/Clusters", dataObject);
      if ( sc.isSuccess() )
      {
       if (debug=="True"){  Lib::Out out(printer());
         out <<  "INFO : Track to VPCLuster: "   <<  dataObject  << Lib::endl;
       }
        LHCb::VPCluster* obj =
          dynamic_cast<LHCb::VPCluster*>(dynamic_cast<ObjectContainerBase*>(dataObject)->containedObject(vid.channelID()));
        if ( obj ) vpClusters.add(obj);
      }
    }
    if( meas[index].isTT()  || meas[index].isIT() || meas[index].isUT())
    {
      LHCb::STChannelID vid = meas[index].stID();
      if( vid.isTT() )
      {
        sc = fDataProviderSvc->retrieveObject(slot+"Raw/TT/Clusters", dataObject);
      }
      else if( vid.isIT() )
      {
        sc = fDataProviderSvc->retrieveObject(slot+"Raw/IT/Clusters", dataObject);
      }
      else 
      {
        sc = fDataProviderSvc->retrieveObject(slot+"Raw/UT/Clusters", dataObject);
      }
      if ( sc.isSuccess() )
      {
        LHCb::STCluster* obj
          = dynamic_cast<LHCb::STCluster*>(dynamic_cast<ObjectContainerBase*>(dataObject)->containedObject(vid.channelID()));
        if ( obj ) stClusters.add(obj);
      }
    }
    if( meas[index].isOT() )
    {
      LHCb::OTChannelID vid = meas[index].otID();
      sc = fDataProviderSvc->retrieveObject(slot+"Raw/OT/Times", dataObject);
      if ( sc.isSuccess() )
      {
        LHCb::OTTime* obj
          = dynamic_cast<LHCb::OTTime*>(dynamic_cast<ObjectContainerBase*>(dataObject)->containedObject(vid.channelID()));
        if ( obj ) otTimes.add(obj);
      }
    }
    if( meas[index].isMuon() ) {
      LHCb::MuonTileID vid = meas[index].muonID();
      sc = fDataProviderSvc->retrieveObject(slot+"Raw/Muon/Coords", dataObject);
      if ( sc.isSuccess() )
      {
        LHCb::MuonCoord* obj
          = dynamic_cast<LHCb::MuonCoord*>(dynamic_cast<ObjectContainerBase*>(dataObject)->containedObject(vid));
        if ( obj ) muonCoords.add(obj);
      }
    }
  }

  drawContainer(vClusters);
  drawContainer(vpClusters);
  drawContainer(stClusters);
  drawContainer(otTimes);
  drawContainer(muonCoords);
}
//////////////////////////////////////////////////////////////////////////////
void TrackType::visualizeMCParticle( LHCb::Track& aTrack ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Get location :
  const ObjectContainerBase* parentname = aTrack.parent();
  const std::string & namenew
    = parentname->registry()->identifier();   //finds name and directory of track being matched

  LinkedTo<LHCb::MCParticle> track3dLink(fDataProviderSvc, 0, namenew);

  if ( !track3dLink.notFound() )
  {
    LHCb::MCParticles objs;
    LHCb::MCParticle* obj = track3dLink.first(aTrack.key());
    while ( 0 != obj )
    {
      if (obj) objs.add(obj);
      obj = track3dLink.next();
    }
    drawContainer(objs);
  }
}
//////////////////////////////////////////////////////////////////////////////
void TrackType::visualizeChargeConjugate( LHCb::Track& aTrack ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  DataObject* dobj(NULL);
  // get extrapolator
  ITrackExtrapolator* extrapolator;
  StatusCode xc = m_toolSvc->retrieveTool("TrackMasterExtrapolator", extrapolator);
  // check for container:
  const StatusCode sc = fDataProviderSvc->retrieveObject("/Event/Rec/Track/CC", dobj);
  if ( !sc.isSuccess() ){
  // make a new one:
    LHCb::Tracks* myTrackCont   = new LHCb::Tracks();
    StatusCode sc = fDataProviderSvc->registerObject("/Event/Rec/Track","CC",myTrackCont);
               sc = fDataProviderSvc->retrieveObject("/Event/Rec/Track/CC", dobj);
  }
// make a new track
  LHCb::Track* newTrack = new LHCb::Track();
  newTrack->setType(aTrack.type());
  LHCb::State fs    = aTrack.firstState();
  LHCb::State state = LHCb::State(fs);
  state.setQOverP(-fs.qOverP());
  newTrack->addToStates(state);
  const std::vector< LHCb::State * >& states = aTrack.states();
  int number = states.size();
  for(int count=1;count<number;count++) {
      const LHCb::State& ostate = *(states[count]);
      double z = ostate.z();
      LHCb::State s = LHCb::State(state);
      extrapolator->propagate(s, z);
      newTrack->addToStates(s);
  }
  LHCb::Tracks* objsp = (dynamic_cast<LHCb::Tracks*>(dobj)); 
  objsp->add(newTrack);
  LHCb::Tracks objs;
  objs.add(newTrack);
  drawContainer(objs);
}
//////////////////////////////////////////////////////////////////////////////
Lib::IIterator* TrackType::iterator( )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!setLocationFromSession(false)) {
    setLocation(LHCb::TrackLocation::Default);
  }
  return SoEvent::Type<LHCb::Track>::iterator();
}
