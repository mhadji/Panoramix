#ifndef SoEvent_VPClusterType_h
#define SoEvent_VPClusterType_h

// Inheritance :
#include "Type.h"

#include "Event/VPCluster.h" //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class DeVP;

class VPClusterType : public SoEvent::Type<LHCb::VPCluster> {
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public: //OnX::IType
  virtual void visualize(Lib::Identifier,void*);
public:
  VPClusterType(IUserInterfaceSvc*,ISoConversionSvc*,IDataProviderSvc*,
          IDataProviderSvc*);
private:
  DeVP * deVP() const;
private:
  IDataProviderSvc* fDetectorDataSvc;
  void visualizeMCParticle(LHCb::VPCluster&);
};

#endif
