#include "SoOnStoredTrack.h"

#include "Event/OTClusterOnStoredTrack.h"
//static CnvFactory< SoOTOnStoredTrackCnv<OTClusterOnStoredTrack,OTTime> > s_OT_factory;
//extern const ICnvFactory& SoOTClusterOnStoredTrackCnvFactory = s_OT_factory;
DECLARE_COMPONENT( SoOTClusterOnStoredTrackCnv );

#include "Event/ITClusterOnStoredTrack.h"
//static CnvFactory< SoOnStoredTrackCnv<ITClusterOnStoredTrack,ITCluster> > s_IT_factory;
//extern const ICnvFactory& SoITClusterOnStoredTrackCnvFactory = s_IT_factory;
DECLARE_COMPONENT( SoITClusterOnStoredTrackCnv );

#include "Event/VeloClusterOnStoredTrack.h"
//static CnvFactory< SoOnStoredTrackCnv<VeloClusterOnStoredTrack,VeloCluster> > s_Velo_factory;
//extern const ICnvFactory& SoVeloClusterOnStoredTrackCnvFactory = s_Velo_factory;
DECLARE_COMPONENT( SoVeloClusterOnStoredTrackCnv );
