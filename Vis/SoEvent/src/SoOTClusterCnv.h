#ifndef SoEvent_SoOTClusterCnv_h
#define SoEvent_SoOTClusterCnv_h

#include "SoEventConverter.h"

class SoOTClusterCnv : public SoEventConverter {
public:
  SoOTClusterCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
