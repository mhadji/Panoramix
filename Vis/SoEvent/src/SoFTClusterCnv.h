#ifndef SoEvent_SoFTClusterCnv_h
#define SoEvent_SoFTClusterCnv_h

#include "SoEventConverter.h"

class SoFTClusterCnv : public SoEventConverter {
public:
  SoFTClusterCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
