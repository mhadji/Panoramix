#ifndef SoEvent_SoVertexCnv_h
#define SoEvent_SoVertexCnv_h

#include "SoEventConverter.h"
#include "Event/Vertex.h"

class SoVertexCnv : public SoEventConverter {
public:
  SoVertexCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
