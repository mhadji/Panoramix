#ifndef SoEvent_SoParticleCnv_h
#define SoEvent_SoParticleCnv_h

#include "SoEventConverter.h"

class SoParticleCnv : public SoEventConverter {
public:
  SoParticleCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
