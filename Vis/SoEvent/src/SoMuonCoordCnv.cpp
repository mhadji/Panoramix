// To fix clashes between Gaudi and Windows :
#include "OnXSvc/Win32.h"

// this :
#include "SoMuonCoordCnv.h"

// Inventor :
#include "Inventor/nodes/SoSeparator.h"
#include "Inventor/nodes/SoLightModel.h"
#include "Inventor/nodes/SoDrawStyle.h"
#include "Inventor/nodes/SoCoordinate3.h"
#include "Inventor/nodes/SoTransform.h"
#include "Inventor/nodes/SoIndexedLineSet.h"

// HEPVis :
#include "HEPVis/nodes/SoSceneGraph.h"
#include "HEPVis/nodes/SoHighlightMaterial.h"
#include "HEPVis/misc/SoStyleCache.h"
#include "HEPVis/nodes/SoMarkerSet.h"
typedef HEPVis_SoMarkerSet SoMarkerSet;

// Lib :
#include "Lib/smanip.h"
#include "Lib/Interfaces/ISession.h"

// Gaudi :
#include "GaudiKernel/MsgStream.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/SmartDataPtr.h"

// LHCb :
#include "Event/MuonCoord.h"
#include "MuonDet/DeMuonDetector.h"

// OnXSvc :
#include "OnXSvc/Filter.h"
#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ClassID.h"
#include "OnXSvc/Helpers.h"

DECLARE_COMPONENT( SoMuonCoordCnv )

//////////////////////////////////////////////////////////////////////////////
SoMuonCoordCnv::SoMuonCoordCnv(
 ISvcLocator* aSvcLoc
)
:SoEventConverter(aSvcLoc,SoMuonCoordCnv::classID())
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoMuonCoordCnv::createRep(
 DataObject* aObject
,IOpaqueAddress*& aAddr
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log(msgSvc(), "SoMuonCoordCnv");
  //log << MSG::INFO << "MuonCoord createReps" << endmsg;

  if(!fUISvc) {
    log << MSG::INFO << " UI service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  if(!fParticlePropertySvc) {
    log << MSG::INFO << " ParticleProperty service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  ISession* session = fUISvc->session();
  if(!session) {
    log << MSG::INFO << " can't get ISession." << endmsg;
    return StatusCode::FAILURE;
  }

  SoRegion* region = 0;
  if(aAddr) {
    // Optimization.
    // If having a not null aAddr, we expect a SoRegion.
    // See SoEvent/Type.h/SoEvent::Type<>::beginVisualize.
    region = (SoRegion*)aAddr;
  } else {
    region = fUISvc->currentSoRegion();
  }
  if(!region) {
    log << MSG::INFO << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE;
  }

  if(!aObject) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE;
  }

  LHCb::MuonCoords* coords = dynamic_cast<LHCb::MuonCoords*>(aObject);
  if(!coords) {
    log << MSG::INFO << " bad object type." << endmsg;
    return StatusCode::FAILURE;
  }

  bool deleteVector = false;
  // Filter :
  Filter<LHCb::MuonCoord> filter(*fUISvc,log);
  const std::string& cuts = fUISvc->cuts();
  if(cuts!="") {
    log << MSG::INFO << " cuts \"" << cuts << "\"" << endmsg;
    coords = filter.collect(*coords,"MuonCoord",cuts);
    if(!coords) return StatusCode::SUCCESS;
    //filter.dump(*coords,"MuonCoord");
    deleteVector = true;;
  }

  if(!coords->size()) {
    log << MSG::INFO << " collection is empty." << endmsg;
    return StatusCode::SUCCESS;
  }

   // get the Muon geometry
  SmartDataPtr<DeMuonDetector> muonDetector(fDetectorDataSvc,"/dd/Structure/LHCb/DownstreamRegion/Muon");
  if ( !muonDetector ) {
    log << MSG::ERROR << "Unable to retrieve muon detector element "
	<< endmsg;
    return StatusCode::FAILURE;
  }

  // Representation attributes :
  // Get color (default is grey (valid on black or white background) ):
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 0.0;
  std::string value;
  if(session->parameterValue("modeling.color",value))
    Lib::smanip::torgb(value,r,g,b);
  if(session->parameterValue("modeling.highlightColor",value))
    Lib::smanip::torgb(value,hr,hg,hb);
  double lineWidth = 0;
  if(session->parameterValue("modeling.lineWidth",value))
    if(!Lib::smanip::todouble(value,lineWidth)) lineWidth = 0;

  SoStyleCache* styleCache = region->styleCache();
  SoLightModel* lightModel = styleCache->getLightModelBaseColor();
  SoDrawStyle* drawStyle =
    styleCache->getLineStyle(SbLinePattern_solid,float(lineWidth));
  SoMaterial* highlightMaterial =
    styleCache->getHighlightMaterial(float(r),float(g),float(b),
                                     float(hr),float(hg),float(hb),0,TRUE);


  SoSeparator* separator = new SoSeparator;

  SoCoordinate3* coordinate3 = new SoCoordinate3;
  separator->addChild(coordinate3);
  int icoord = 0;
  int32_t coordIndex[9];
  SbBool empty = TRUE;

  // Construct one scene graph per MuonCoord :

  LHCb::MuonCoords::iterator it;
  for(it = coords->begin(); it != coords->end(); it++) {
    LHCb::MuonCoord* coord  = (*it);
    if(!coord) continue;

    // Build name for picking :
    char sid[64];
    ::sprintf(sid,"MuonCoord/0x%lx",(unsigned long)coord);

    LHCb::MuonTileID tile = coord->key();

    SoSceneGraph* sep = new SoSceneGraph;
    sep->setString(sid);

    separator->addChild(sep);

    sep->addChild(highlightMaterial);

    sep->addChild(lightModel);
    sep->addChild(drawStyle);

    double x,y,z;
    double deltax,deltay,deltaz;
    muonDetector->Tile2XYZ(tile,x,deltax,y,deltay,z,deltaz);
    int pointn = 8;
    SbVec3f points[8];
    points[0].setValue((float)(x-deltax),(float)y,(float)z);
    points[1].setValue((float)(x+deltax),(float)y,(float)z);
    points[2].setValue((float)x,(float)y,(float)z);
    points[3].setValue((float)x,(float)(y-deltay),(float)z);
    points[4].setValue((float)x,(float)(y+deltay),(float)z);
    points[5].setValue((float)x,(float)y,(float)z);
    points[6].setValue((float)x,(float)y,(float)(z-deltaz));
    points[7].setValue((float)x,(float)y,(float)(z+deltaz));
    coordIndex[0] = icoord + 0;
    coordIndex[1] = icoord + 1;
    coordIndex[2] = icoord + 2;
    coordIndex[3] = icoord + 3;
    coordIndex[4] = icoord + 4;
    coordIndex[5] = icoord + 5;
    coordIndex[6] = icoord + 6;
    coordIndex[7] = icoord + 7;
    coordIndex[8] = SO_END_LINE_INDEX;

    coordinate3->point.setValues(icoord,pointn,points);
    icoord += pointn;

    SoIndexedLineSet* lineSet = new SoIndexedLineSet;
    lineSet->coordIndex.setValues(0,pointn+1,coordIndex);
    sep->addChild(lineSet);
// indicate if it is made of two digits
    if (coord->digitTile().size()>1) {
     SoMarkerSet* markerSet = new SoMarkerSet;
     markerSet->numPoints = 1;
     markerSet->markerIndex = SoMarkerSet::CIRCLE_FILLED_5_5;
     sep->addChild(markerSet);
    }

    empty = FALSE;
  }

  if(deleteVector) {
    // We have first to empty the vector :
    while(coords->size()) {
      coords->remove(*(coords->begin()));
    }
    // Then we can delete it :
    delete coords;
  }


  if(empty==TRUE) {
    separator->unref();
  } else {
    //  Send scene graph to the viewing region
    // (in the "dynamic" sub-scene graph) :
    region_addToDynamicScene(*region,separator);
  }

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoMuonCoordCnv::classID(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return LHCb::MuonCoords::classID();
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoMuonCoordCnv::storageType(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return So_TechnologyType;
}
