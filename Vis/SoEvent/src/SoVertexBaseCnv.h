#ifndef SoEvent_SoVertexBaseCnv_h
#define SoEvent_SoVertexBaseCnv_h

#include "SoEventConverter.h"
#include "Event/VertexBase.h"

class SoVertexBaseCnv : public SoEventConverter {
public:
  SoVertexBaseCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
