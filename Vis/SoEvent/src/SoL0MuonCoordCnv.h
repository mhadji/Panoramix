#ifndef SoEvent_SoL0MuonCoordCnv_h
#define SoEvent_SoL0MuonCoordCnv_h

#include "SoEventConverter.h"

class SoL0MuonCoordCnv : public SoEventConverter {
public:
  SoL0MuonCoordCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
