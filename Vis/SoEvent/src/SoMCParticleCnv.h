#ifndef SoEvent_SoMCParticleCnv_h
#define SoEvent_SoMCParticleCnv_h

#include "SoEventConverter.h"

class SoMCParticleCnv : public SoEventConverter {
public:
  SoMCParticleCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
