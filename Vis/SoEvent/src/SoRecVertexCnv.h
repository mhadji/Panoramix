#ifndef SoEvent_SoRecVertexCnv_h
#define SoEvent_SoRecVertexCnv_h

#include "SoEventConverter.h"
#include "Event/RecVertex.h"

class SoRecVertexCnv : public SoEventConverter {
public:
  SoRecVertexCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
