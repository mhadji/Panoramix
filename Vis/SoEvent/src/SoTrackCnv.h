#ifndef SoEvent_SoTrackCnv_h
#define SoEvent_SoTrackCnv_h

#include "SoEventConverter.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Event/TrackTypes.h"

class SoTrackCnv : public SoEventConverter {
public:
  SoTrackCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
