#ifndef Panoramix_Helpers_h
#define Panoramix_Helpers_h

#include <Lib/Interfaces/ISession.h>
#include <Lib/Manager.h>

#include <OnX/Helpers/OnX.h>

#include <GaudiKernel/IParticlePropertySvc.h>
#include <GaudiKernel/IToolSvc.h>
#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiKernel/IDataManagerSvc.h>
#include <GaudiKernel/IEvtSelector.h>
#include <GaudiKernel/IHistogramSvc.h>
#include <GaudiKernel/IAlgManager.h>
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/IService.h>
#include <GaudiKernel/IAppMgrUI.h>

#include <GaudiKernel/KeyedContainer.h>

#include <GaudiKernel/DataObject.h>
#include <GaudiKernel/SmartDataPtr.h>

#include <OnXSvc/ISvcLocatorManager.h>
#include <OnXSvc/IUserInterfaceSvc.h>
#include <OnXSvc/ISoConversionSvc.h>

//////////////////////////////////////////////////////////////////////////////
inline ISvcLocator* find_svcLocator(
 ISession& aSession
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Find SvcLocator :
  ISvcLocatorManager* svcLocatorManager = 
    Lib_findManager(aSession,"SvcLocatorManager",ISvcLocatorManager);
  if(!svcLocatorManager) return 0;
  return svcLocatorManager->serviceLocator();
}
//////////////////////////////////////////////////////////////////////////////
inline IService* find_service(
 ISession& aSession
,const std::string& aName
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Find SvcLocator :
  ISvcLocatorManager* svcLocatorManager = 
    Lib_findManager(aSession,"SvcLocatorManager",ISvcLocatorManager);
  if(!svcLocatorManager) return 0;
  ISvcLocator* svcLocator = svcLocatorManager->serviceLocator();
  if(!svcLocator) return 0;
  const std::list<IService*>& services = svcLocator->getServices();
  std::list<IService*>::const_iterator it;
  for(it=services.begin();it!=services.end();it++) {
    if((*it)->name()==aName) {
      return (*it);
    }
  }
  return 0;
}
//////////////////////////////////////////////////////////////////////////////
inline IUserInterfaceSvc* find_uiSvc(
 ISession& aSession
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Find SvcLocator :
  ISvcLocatorManager* svcLocatorManager = 
    Lib_findManager(aSession,"SvcLocatorManager",ISvcLocatorManager);
  if(!svcLocatorManager) return 0;
  IService* service = svcLocatorManager->service("OnXSvc");
  if(!service) return 0;
  void* p = 0;
  StatusCode status = service->queryInterface(IID_IUserInterfaceSvc,&p);
  if(status!=StatusCode::SUCCESS) return 0;
  return (IUserInterfaceSvc*)p;
}

//////////////////////////////////////////////////////////////////////////////
inline ISoConversionSvc* find_soCnvSvc(
 ISession& aSession
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Find SvcLocator :
  ISvcLocatorManager* svcLocatorManager = 
    Lib_findManager(aSession,"SvcLocatorManager",ISvcLocatorManager);
  if(!svcLocatorManager) return 0;
  IService* service = svcLocatorManager->service("SoConversionSvc");
  if(!service) return 0;
  void* p = 0;
  StatusCode status = service->queryInterface(IID_ISoConversionSvc,&p);
  if(status!=StatusCode::SUCCESS) return 0;
  return (ISoConversionSvc*)p;
}

//////////////////////////////////////////////////////////////////////////////
template <class T>
inline void KO_visualize(ISoConversionSvc& aSoCnvSvc,T& aObject) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  typedef KeyedContainer<T> Collection;
  Collection* collection = new Collection;
  collection->add(&aObject);
  // Convert it :
  IOpaqueAddress* addr = 0;
  StatusCode status = aSoCnvSvc.createRep(collection, addr);
  if (status.isSuccess()) status = aSoCnvSvc.fillRepRefs(addr,collection);
  collection->remove(&aObject); // Else the next line delete the object.
  delete collection;
}

//////////////////////////////////////////////////////////////////////////////
inline IDataProviderSvc* find_eventDataSvc(
 ISession& aSession
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Find SvcLocator :
  ISvcLocatorManager* svcLocatorManager = 
    Lib_findManager(aSession,"SvcLocatorManager",ISvcLocatorManager);
  if(!svcLocatorManager) return 0;
  IService* service = svcLocatorManager->service("EventDataSvc");
  if(!service) return 0;
  void* p = 0;
  StatusCode status = service->queryInterface(IID_IDataProviderSvc,&p);
  if(status!=StatusCode::SUCCESS) return 0;
  return (IDataProviderSvc*)p;
}
//////////////////////////////////////////////////////////////////////////////
inline IToolSvc* find_toolSvc(
 ISession& aSession
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Find SvcLocator :
  ISvcLocatorManager* svcLocatorManager = 
    Lib_findManager(aSession,"SvcLocatorManager",ISvcLocatorManager);
  if(!svcLocatorManager) return 0;
  IService* service = svcLocatorManager->service("ToolSvc");
  if(!service) return 0;
  void* p = 0;
  StatusCode status = service->queryInterface(IID_IToolSvc,&p);
  if(status!=StatusCode::SUCCESS) return 0;
  return (IToolSvc*)p;
}
//////////////////////////////////////////////////////////////////////////////
inline IParticlePropertySvc* find_particlePropertySvc(
 ISession& aSession
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Find SvcLocator :
  ISvcLocatorManager* svcLocatorManager = 
    Lib_findManager(aSession,"SvcLocatorManager",ISvcLocatorManager);
  if(!svcLocatorManager) return 0;
  IService* service = svcLocatorManager->service("ParticlePropertySvc");
  if(!service) return 0;
  void* p = 0;
  StatusCode status = service->queryInterface(IID_IParticlePropertySvc,&p);
  if(status!=StatusCode::SUCCESS) return 0;
  return (IParticlePropertySvc*)p;
}
//////////////////////////////////////////////////////////////////////////////
inline IHistogramSvc* find_histogramSvc(
 ISession& aSession
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Find SvcLocator :
  ISvcLocatorManager* svcLocatorManager = 
    Lib_findManager(aSession,"SvcLocatorManager",ISvcLocatorManager);
  if(!svcLocatorManager) return 0;
  IService* service = svcLocatorManager->service("HistogramDataSvc");
  if(!service) return 0;
  void* p = 0;
  StatusCode status = service->queryInterface(IID_IHistogramSvc,&p);
  if(status!=StatusCode::SUCCESS) return 0;
  return (IHistogramSvc*)p;
}
//////////////////////////////////////////////////////////////////////////////
inline IAppMgrUI* find_appMgrUI(
 ISession& aSession
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Find SvcLocator :
  ISvcLocatorManager* svcLocatorManager = 
    Lib_findManager(aSession,"SvcLocatorManager",ISvcLocatorManager);
  if(!svcLocatorManager) return 0;
  IService* service = svcLocatorManager->service("ApplicationMgr");
  if(!service) return 0;
  void* p = 0;
  StatusCode status = service->queryInterface(IID_IAppMgrUI,&p);
  if(status!=StatusCode::SUCCESS) return 0;
  return (IAppMgrUI*)p;
}
//////////////////////////////////////////////////////////////////////////////
inline IAlgManager* find_algorithmManager(
 ISession& aSession
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Find SvcLocator :
  ISvcLocatorManager* svcLocatorManager = 
    Lib_findManager(aSession,"SvcLocatorManager",ISvcLocatorManager);
  if(!svcLocatorManager) return 0;
  IService* service = svcLocatorManager->service("ApplicationMgr");
  if(!service) return 0;
  void* p = 0;
  StatusCode status = service->queryInterface(IID_IAlgManager,&p);
  if(status!=StatusCode::SUCCESS) return 0;
  return (IAlgManager*)p;
}
//////////////////////////////////////////////////////////////////////////////
inline IEvtSelector* find_eventSelector(
 ISession& aSession
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Find SvcLocator :
  ISvcLocatorManager* svcLocatorManager = 
    Lib_findManager(aSession,"SvcLocatorManager",ISvcLocatorManager);
  if(!svcLocatorManager) return 0;
  IService* service = svcLocatorManager->service("EventSelector");
  if(!service) return 0;
  void* p = 0;
  StatusCode status = service->queryInterface(IID_IEvtSelector,&p);
  if(status!=StatusCode::SUCCESS) return 0;
  return (IEvtSelector*)p;
}

//////////////////////////////////////////////////////////////////////////////
inline DataObject* get_dataObject(
 IDataProviderSvc& aDataProviderSvc
,const std::string& aLocation
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  SmartDataPtr<DataObject> smartDataObject(&aDataProviderSvc,aLocation);
  if(smartDataObject) {}
  DataObject* dataObject = 0;
  StatusCode sc = aDataProviderSvc.retrieveObject(aLocation,dataObject);
  if(!sc.isSuccess()) return 0;
  return dataObject;
}

#endif
