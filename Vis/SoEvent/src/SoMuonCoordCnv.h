#ifndef SoEvent_SoMuonCoordCnv_h
#define SoEvent_SoMuonCoordCnv_h

#include "SoEventConverter.h"

class SoMuonCoordCnv : public SoEventConverter {
public:
  SoMuonCoordCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
