#ifndef SoEvent_SoSTClusterCnv_h
#define SoEvent_SoSTClusterCnv_h

#include "SoEventConverter.h"

class SoSTClusterCnv : public SoEventConverter {
public:
  SoSTClusterCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
