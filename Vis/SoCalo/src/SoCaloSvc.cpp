// ============================================================================
// Include files
// from Gaudi
#include "GaudiKernel/MsgStream.h"
// OnXSvc :
#include "OnXSvc/IUserInterfaceSvc.h"
// local
#include "SoCaloSvc.h"
#include "CaloDigitType.h"
#include "CaloClusterType.h"
//L0
#include "L0CaloDigitType.h"

/** @file SoCaloSvc.cpp
 *
 *  Implementation file for class SoCaloSvc
 *
 *  @date 09/10/2001
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *
 */

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SoCaloSvc  )

//=============================================================================
// Standard creator, initializes variables
//=============================================================================
SoCaloSvc::SoCaloSvc( const std::string& name,
                      ISvcLocator* pSvcLocator)
  : Service ( name , pSvcLocator )
  , m_uiSvc (0)
{
}

//=============================================================================
// Destructor
//=============================================================================
SoCaloSvc::~SoCaloSvc() {}

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode SoCaloSvc::initialize()
{
  MsgStream log( msgSvc(), name() );
  log << MSG::DEBUG << "==> Initialise" << endmsg;

  StatusCode sc = Service::initialize();
  if( sc.isFailure() )
  {
    log << MSG::ERROR
        << "Could not initialize the base class Service!" << endmsg;
    return sc ;
  }

  if(!serviceLocator())
  {
    log << MSG::ERROR << "service locator not found " << endmsg;
    return StatusCode::FAILURE;
  }
  ISvcLocator* svcLoc = serviceLocator();

  if(m_uiSvc) {
    m_uiSvc->release();
    m_uiSvc = 0;
  }
  sc = service("OnXSvc",m_uiSvc,true);
  if( sc.isFailure() || !m_uiSvc )
  {
    log << MSG::ERROR << "OnXSvc not found " << endmsg;
    return StatusCode::FAILURE ;
  }
  m_uiSvc->addRef();

  IPrinter& printer = m_uiSvc->printer();

  /// digits
  m_uiSvc->addType( new CaloDigitType("EcalDigits",svcLoc,printer));
  m_uiSvc->addType( new CaloDigitType("HcalDigits",svcLoc,printer));
  m_uiSvc->addType( new CaloDigitType("PrsDigits" ,svcLoc,printer));
  m_uiSvc->addType( new CaloDigitType("SpdDigits" ,svcLoc,printer));
  m_uiSvc->addType( new L0CaloDigitType("L0CaloEcalDigits",svcLoc,printer));
  m_uiSvc->addType( new L0CaloDigitType("L0CaloHcalDigits",svcLoc,printer));
  m_uiSvc->addType( new L0CaloDigitType("L0CaloPrsDigits" ,svcLoc,printer));
  m_uiSvc->addType( new L0CaloDigitType("L0CaloSpdDigits" ,svcLoc,printer));
  /// clusters
  m_uiSvc->addType( new CaloClusterType("EcalClusters",svcLoc,printer));
  m_uiSvc->addType( new CaloClusterType("HcalClusters",svcLoc,printer));

  log << MSG::DEBUG
      << "The Service is initialized successfully"
      << endmsg ;

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode SoCaloSvc::finalize()
{
  if(m_uiSvc) {
    m_uiSvc->release();
    m_uiSvc = 0;
  }

  MsgStream log(msgSvc(), name());
  log << MSG::DEBUG << "==> Finalize" << endmsg;

  return Service::finalize() ;
}

//=============================================================================
