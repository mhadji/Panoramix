// $Id: CaloClusterRep.cpp,v 1.13 2009-06-04 21:21:37 odescham Exp $ 
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.12  2009/06/04 17:13:47  odescham
// fix wrong indices
//
// Revision 1.11  2009/05/26 08:32:17  gybarran
// *** empty log message ***
//
// Revision 1.10  2008/07/28 08:13:33  truf
// just to please cmt/cvs, no changes
//
// Revision 1.9  2007/03/19 15:19:21  ranjard
// v8r1 - fix for LHCb v22r2
//
// Revision 1.8  2006/03/30 06:51:04  gybarran
// *** empty log message ***
//
// Revision 1.7  2006/03/30 06:29:55  gybarran
// *** empty log message ***
//
// Revision 1.6  2006/03/09 16:30:26  odescham
// v6r2 - migration to LHCb v20r0
//
// Revision 1.5  2004/09/09 08:50:23  ibelyaev
//  move generic code into new Vis/SoUtils package
//
// ============================================================================
#define    SOCALO_CALOCLUSTERREP_CPP 1 
// ============================================================================
// ============================================================================
// CLHEP
// ============================================================================
#include "CLHEP/Geometry/Point3D.h"
//From GaudiKernel
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
// ============================================================================
// Inventor
// ============================================================================
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoRotation.h>
#include <Inventor/nodes/SoSphere.h>
// ============================================================================
#include <HEPVis/nodes/SoSceneGraph.h>
#include <HEPVis/nodes/SoTextTTF.h>
#include <HEPVis/nodes/SoEllipsoid.h>
#include <HEPVis/nodes/SoArrow.h>
#include <HEPVis/nodes/SoEllipticalPrism.h>
// ============================================================================
// OnXSvc
// ============================================================================
#include <OnXSvc/Win32.h>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/SmartRef.h"
// ============================================================================
// DetDesc 
// ============================================================================
#include "DetDesc/IGeometryInfo.h"
// ============================================================================
// CaloEvent 
// ============================================================================
#include "Event/CaloCluster.h" 
#include "Event/CaloDataFunctor.h" 
// ============================================================================
// CaloDet
// ============================================================================
#include "CaloDet/DeCalorimeter.h" 
// ============================================================================
// SoUtils 
// ============================================================================
#include "SoUtils/SoUtils.h"
// ============================================================================
//local 
// ============================================================================
#include "CaloClusterRep.h" 
// ============================================================================

// ============================================================================
/** @file CaloClusterRep.cpp
 *
 *  implementation of CaloCluster representation
 *
 *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
 *  @date   20/10/2001 
 */
// ============================================================================

// ============================================================================
/** standard constructor
 *  @param DeCalo         pointer to DeCalorimeter object 
 *                        (source of geometry  information)
 *  @param Type           Own "Object Type" for picking 
 *  @param TypeD          Digit "Object Type" for picking 
 *  @param EScale         drawing scale
 *  @param EtVis          flag for drawing the transverse energy 
 *  @param LogVis         flag for logarithmic drawing
 *  @param TxtVis         flag for drawing the text 
 *  @param TxtSize        text size 
 *  @param TxtPos         text position
 *  @param VisDigits      flag for drawing of digits 
 *  @param VisShared      flag for special processing of shared digits 
 *  @param VisStructure   flag for visualization of cluster structure 
 *  @param VisCovariance  flag for visualization of covarinace matrix 
 *  @param VisSpread      flag for visualization of cluster spread
 *  @param VisPrism       flag for visualization of spread prism 
 */ 
// ============================================================================
CaloClusterRep::CaloClusterRep( const DeCalorimeter*   DeCalo        ,
                                const std::string&     Type          ,
                                const std::string&     TypeD         ,
                                const double           EScale        ,  
                                const bool             EtVis         ,  
                                const bool             LogVis        ,  
                                const bool             TxtVis        ,  
                                const double           TxtSize       ,  
                                const double           TxtPos        ,
                                const bool             VisDigits     , 
                                const bool             VisShared     , 
                                const bool             VisStructure  ,
                                const bool             VisCovariance , 
                                const bool             VisSpread     , 
                                const bool             VisPrism      )
  : m_type( Type )
  , m_visDigits     ( VisDigits     )
  , m_visShared     ( VisShared     ) 
  , m_visStructure  ( VisStructure  )
  , m_visCovariance ( VisCovariance )
  , m_visSpread     ( VisSpread     )
  , m_visPrism      ( VisPrism      )
  , m_digRep        ( DeCalo        , 
                      TypeD         , 
                      EScale        , 
                      EtVis         ,
                      LogVis        ,
                      TxtVis        ,
                      TxtSize       ,
                      TxtPos        )
{
  if( visShared () ) { setVisDigits ( true ) ; }
  if( visPrism  () ) { setVisSpread ( true ) ; }
}

// ============================================================================
/// destructor 
// ============================================================================
CaloClusterRep::~CaloClusterRep(){}

// ============================================================================
/// the only one essential method 
// ============================================================================
SoSeparator* CaloClusterRep::operator() ( const LHCb::CaloCluster* cluster ) const
{
  /// if detector is NULL or cluster is NULL drawing is not possible! 
  if( 0 == deCalo() || 0 == cluster ) { return 0 ; }
  /// create separator 
  SoSeparator* separator = new SoSeparator();
  ///
  // Build picking string id :
  std::string sid;
  if( !type().empty() ) 
    {
      char* s = new char[type().size()+32];
      ::sprintf(s,"%s/0x%lx",m_type.c_str(), (unsigned long) cluster );
      sid = std::string(s);
      delete [] s;
    }
  /// visualize the digits?
  unsigned nUsedDigs = 0 ;
  for( LHCb::CaloCluster::Entries::const_iterator it = 
         cluster->entries().begin() ; cluster->entries().end() != it ; ++it )
    {
      const LHCb::CaloDigit* digit        = it->digit();
      if( 0 == digit ) { continue ; }
      const LHCb::CaloDigitStatus::Status& status = it->status();
      /// create the representation of the digit 
      const Gaudi::XYZPoint& cellCenter = 
        deCalo()->cellCenter( digit->cellID() );
      const double      cellsize   = 
        deCalo()->cellSize  ( digit->cellID() );
      ///
      ++nUsedDigs;
      ///
      if( visDigits() ) 
        {     
          SoSeparator* sep = digRep()( digit,sid);
          if( 0 == sep ) { continue ; }
          separator->addChild( sep );

          if( visShared() && 
              0 != ( status & LHCb::CaloDigitStatus::SharedCell ) ) 
            {
              const double height = 
                digRep().size() * it->fraction() ;
              SoArrow* arrow = new SoArrow();
              Gaudi::XYZVector
                vect = Gaudi::XYZPoint( cluster->position().x() , 
                                        cluster->position().y() , 
                                        cluster->position().z() ) - cellCenter ;
              
              vect.SetZ( 0 );    // SetZ in DisplacementVector3D - setZ in CLHEP !!!!!
              vect *= 0.5 * cellsize / vect.r() ; // R() = sqrt(mag2)
              ///
              const Gaudi::XYZPoint tip( cellCenter + vect ) ;
              ///
              arrow->tail.
                setValue( (float)cellCenter.x () , 
                          (float)cellCenter.y () ,
                          (float)(cluster->position().z() + height) );
              arrow->tip.
                setValue( (float)tip.x        () , 
                          (float)tip.y        () , 
                          (float)(cluster->position().z() + height) );
              ///
              SoSceneGraph* sep = new SoSceneGraph;
              sep->setString(sid.c_str());
              sep->addChild(arrow);
              //
              separator -> addChild ( sep    ) ;
            }
        }
    }
  ///
  if( visStructure() )
    {
      Gaudi::XYZPoint seedPoint( cluster->position().x() ,
                                 cluster->position().y() ,
                                 cluster->position().z() ) ;
      for( LHCb::CaloCluster::Digits::const_iterator it = 
             cluster->entries().begin() ; 
           cluster->entries().end() != it ; ++it )
        {
	  const LHCb::CaloDigit* digit        = it->digit();
	  const LHCb::CaloDigitStatus::Status& status = it->status();
          if( 0 == digit )       { continue ; }
          /// 
          const  Gaudi::XYZPoint& cellCenter = 
            deCalo()->cellCenter( digit->cellID() );
          const double      cellsize   = 
            deCalo()->cellSize  ( digit->cellID() );
          /// redefine the seed point 
          if( status & LHCb::CaloDigitStatus::SeedCell ) 
            { seedPoint = cellCenter ; }
          ///
          Gaudi::XYZVector vect = seedPoint - cellCenter ;
          if( 0 != vect.mag2() ) 
            {    
              vect *= 0.25 * cellsize / vect.r() ;
              ///
              const Gaudi::XYZPoint tip ( cellCenter + vect );
              ///
              SoArrow* arrow = new SoArrow();
              ///
              arrow->tail.
                setValue( (float)cellCenter.x () , 
                          (float)cellCenter.y () ,
                          (float)cluster->position().z   () );
              arrow->tip.
                setValue( (float)tip.x        () , 
                          (float)tip.y        () , 
                          (float)cluster->position().z   () );
              ///
              SoSceneGraph* sep = new SoSceneGraph;
              sep->setString(sid.c_str());
              sep->addChild(arrow);
              //
              separator -> addChild ( sep    ) ;
            }
        }
    }
  /// visualize the ellipses
  /// (1) the "cov" ellipsoid !!!
  if( visCovariance() ) 
    { 
      Gaudi::SymMatrix3x3          mtrx   ;
      /// copy the matrix
      ///        x   x                                     x   x
      mtrx( 0 , 0 ) = cluster->position().covariance()( LHCb::CaloPosition::X ,   LHCb::CaloPosition::X ) ;
      ///        y   x                                     y   x 
      mtrx( 1 , 0 ) = cluster->position().covariance()( LHCb::CaloPosition::Y ,   LHCb::CaloPosition::X ) ;
      ///        y   y                                     y   y 
      mtrx( 1 , 1 ) = cluster->position().covariance()( LHCb::CaloPosition::Y ,   LHCb::CaloPosition::Y ) ;
      ///        e   x                                     x   e 
      mtrx( 2 , 0 ) = cluster->position().covariance()( LHCb::CaloPosition::X ,   LHCb::CaloPosition::E ) ;
      ///        e   y                                     y   e 
      mtrx( 2 , 1 ) = cluster->position().covariance()( LHCb::CaloPosition::Y ,   LHCb::CaloPosition::E ) ;
      ///        e   e                                     e   e 
      mtrx( 2 , 2 ) = cluster->position().covariance()(  LHCb::CaloPosition::E ,  LHCb::CaloPosition::E ) ;
      ///
      double height  = cluster->e();
      /// et ? 
      if( etVis() ) 
        {
          const double sintheta = 
            sin( Gaudi::XYZPoint( cluster->position().x() , 
                                  cluster->position().y() , 
                                  cluster->position().z() ).theta() );
          height = 
            cluster->e() * sintheta ;
          ///
          mtrx( 2 , 0 ) *= sintheta ;
          mtrx( 2 , 1 ) *= sintheta ;
          mtrx( 2 , 2 ) *= sintheta ;
          mtrx( 2 , 2 ) *= sintheta ;
        }
      /// log ? 
      if( logVis() ) 
        {
          mtrx( 2 , 0 ) /= -1.0 * height ;
          mtrx( 2 , 1 ) /= -1.0 * height ;
          mtrx( 2 , 2 ) /= -1.0 * height ;
          mtrx( 2 , 2 ) /= -1.0 * height ;
        }
      /// rescale 
      mtrx( 2 , 0 ) *= eScale()  ;
      mtrx( 2 , 1 ) *= eScale()  ;
      mtrx( 2 , 2 ) *= eScale()  ;
      mtrx( 2 , 2 ) *= eScale()  ;
      ///
      SoEllipsoid* ell = 0 ;
      StatusCode sc = 
        SoUtils::ellipsoid 
        ( Gaudi::XYZPoint( cluster->position().x() , 
                           cluster->position().y() , 
                           cluster->position().z() ) ,  mtrx , ell  );
      ///
      if( sc.isSuccess() && 0 != ell ) 
      { 
        SoSceneGraph* sep = new SoSceneGraph;
        sep->setString(sid.c_str());
        sep->addChild(ell);
        separator->addChild(sep);
      }
      ///
    }
  /// (2) the "spread" elliptical prism  
  if( visSpread() )
    { 
      ///    
      SoEllipticalPrism* ell = 0 ;
      ///
      double height = 1. ;
      if( visPrism() ) 
        {
          height = cluster->e() ;
          if( etVis() ) 
            {
              const double sintheta = 
                sin( Gaudi::XYZPoint( cluster->position().x() , 
                                 cluster->position().y() , 
                                 cluster->position().z() ).theta() );
              height = 
                cluster->e() * sintheta ;
            }
          if( logVis() )
            { height = 0 < height ? log10( height ) : 1.0 ; }
          ///
          height *= eScale() ;
          if( !visDigits() ) { height /= 2. ; }
          ///
        }
      ///
      StatusCode sc = 
        SoUtils::ellipticalprism 
        ( Gaudi::XYZPoint( cluster->position().x() , 
                           cluster->position().y() , 
                           cluster->position().z() + height / 2 ) , 
          cluster->position().spread()  , height / 2 , ell  );
      ///
      if( sc.isSuccess() && 0 != ell ) 
        { 
          SoSceneGraph* sep = new SoSceneGraph;
          sep->setString(sid.c_str());
          sep->addChild(ell);
          separator->addChild(sep);
        }
      ///
    }
  ///
  return separator;
  ///
}



