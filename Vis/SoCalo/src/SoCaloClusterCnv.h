// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.7  2008/07/28 08:13:33  truf
// just to please cmt/cvs, no changes
//
// Revision 1.6  2007/02/02 15:48:29  ranjard
// v8r0 - fix to use new PluginManager
//
// Revision 1.5  2006/12/07 10:01:07  gybarran
// G.Barrand : handle modeling solid, wire_frame
//
// Revision 1.4  2006/12/07 09:01:32  gybarran
// G.Barrand : handle coloring and other vis attributes. Have correct data-accessor name for picking
//
// Revision 1.3  2004/02/05 09:04:43  gybarran
// modifs for GAUDI_v14r1, LHCB_v15r1
//
// Revision 1.2  2002/09/11 07:00:58  barrand
// G.Barrand : update according new event model and OnX/v11
//
// Revision 1.1  2001/10/21 16:26:48  ibelyaev
// Visualization for CaloClusters is added
//
// ============================================================================
#ifndef SOCALO_SOCALOCLUSTERCNV_H
#define SOCALO_SOCALOCLUSTERCNV_H 1
// ============================================================================

// STD and STL
#include <vector>
#include <string>
// Calo
#include "SoCaloBaseCnv.h"

// forward decalarations
class DeCalorimeter;

/**  @class SoCaloClusterCnv  SoCaloClusterCnv.h
 *
 *   Converter for visualization of
 *   container of CaloCluster objects
 *
 *   @author  Vanya Belyaev  Ivan.Belyaev@cern.ch
 *   @date    20/10/2001
 */

class SoCaloClusterCnv : public SoCaloBaseCnv
{
public:
  /// standard initialization method
  virtual StatusCode initialize ();
  /// standard finalization  method
  virtual StatusCode finalize   ();

  virtual long repSvcType() const;

  /// the only one essential method
  virtual StatusCode createRep( DataObject* /* Object */ ,
                                IOpaqueAddress*&  /* Address */ );
  /// Class ID for created object == class ID for this specific converter
  static const CLID&         classID     ();
  /// storage Type
  static unsigned char storageType () ;

  /// standard constructor
  SoCaloClusterCnv( ISvcLocator* svcLoc );
  /// virtual destructor
  virtual ~SoCaloClusterCnv();
  /// helpful methos to save typing
  StatusCode  locateCalo( const unsigned int calo );
  /// accessor to calorimeter
  const DeCalorimeter* calorimeter( const unsigned int calo );
  ///
private:
  ///
  /// default constructor is disabled
  SoCaloClusterCnv           (                       ) ;
  /// copy constructor is disabled
  SoCaloClusterCnv           ( const SoCaloClusterCnv& ) ;
  /// assignment is disabled
  SoCaloClusterCnv& operator=( const SoCaloClusterCnv& ) ;
  ///
  std::string attribute ( const std::string& att ) const;
  bool modelingSolid ( ) const;
private:
  ///
  /// located calorimeter objects
  std::vector<const DeCalorimeter*>   m_calos;
  ///
};


// ============================================================================
#endif  ///< SOCALO_SOCALOCLUSTERCNV_H
// ============================================================================
