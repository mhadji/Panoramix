#ifndef SOCALO_SOCALOSVC_H
#define SOCALO_SOCALOSVC_H 1

// Inheritance :
#include <GaudiKernel/Service.h>

// Include files
// from STD & STL
#include <string>

class IUserInterfaceSvc;

/** @class SoCaloSvc SoCaloSvc.h
 *
 *   Auxillary class (probably temporary) for declaring
 *  "Calorimeter" visualization Types to the system
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   09/10/2001
 */

class SoCaloSvc : public Service
{
public:

  /** Standard initialization method
   *  just declare all available calormeter visualisation types
   *  @return status code
   */
  virtual StatusCode initialize();

  /** standard finaliation method
   *  @return statsu code
   */
  virtual StatusCode finalize  ();

  virtual ~SoCaloSvc( ); ///< Destructor

  /** Standard constructor
   *  @param name name of the service
   *  @param SvcLoc pointer to Service Locator
   */
  SoCaloSvc( const std::string& name, ISvcLocator* SvcLoc );

private:

  SoCaloSvc();
  SoCaloSvc( const SoCaloSvc& );
  SoCaloSvc& operator=( const SoCaloSvc& );

private:

  IUserInterfaceSvc* m_uiSvc;
};

// ============================================================================
#endif // SOCALOSVC_H
// ============================================================================
