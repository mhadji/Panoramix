// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.2  2002/09/11 07:00:58  barrand
// G.Barrand : update according new event model and OnX/v11
//
// Revision 1.1.1.1  2001/10/17 18:26:07  ibelyaev
// New Package: Visualization of Calorimeter objects 
// 
// ============================================================================
#ifndef    SOCALO_SOCALOBASECNV_H 
#define    SOCALO_SOCALOBASECNV_H  1 
// STL 
#include <string> 
// GaudiKernel 
#include "GaudiKernel/Converter.h"
// forward declaration 
class ISoConversionSvc;
class IUserInterfaceSvc;
class IChronoStatSvc; 
class GaudiException; 
class IDataProviderSvc;

/**  @class SoCaloBaseCnv  SoCaloBaseCnv.h CaloVis/SoCaloBaseCnv.h 
 *
 *   Common base class for "So" converters for visualisation of 
 *   calorimeter objects. 
 *   
 *   @author  Vanya Belyaev 
 *   @date    18/04/2001 
 */

class SoCaloBaseCnv : public Converter 
{
  ///
public:
  ///
  /// standard initialization method 
  virtual StatusCode initialize();
  /// standard finalization  method 
  virtual StatusCode finalize  ();
  /// the only one essential method 
  virtual StatusCode createRep( DataObject* /* Object */ , 
                                IOpaqueAddress*& /* Address */ );
  /// name of this converter 
  virtual const std::string&  name  () const { return m_name      ; }    
  ///
protected: 
  ///
  /// accessor to "So" conversion service 
  inline ISoConversionSvc* soSvc    () const { return m_soSvc     ; } 
  /// accessor to "UI" conversion service 
  inline IUserInterfaceSvc* uiSvc    () const { return m_uiSvc     ; } 
  /// accessor to Chrono & Stat service 
  inline IChronoStatSvc*   chronoSvc() const { return m_chronoSvc ; } 
  /// accessor to Detector Data Svc 
  inline IDataProviderSvc* detSvc   () const { return m_detSvc    ; } 
  ///
  /// print and count error message 
  StatusCode Error     ( const std::string    & Message , 
			 const StatusCode     & sc  = StatusCode::FAILURE );
  /// print and count warning  message 
  StatusCode Warning   ( const std::string    & msg                       ,  
                         const StatusCode     & sc  = StatusCode::FAILURE );
  /// print the message 
  StatusCode Print     ( const std::string    & msg                       ,  
                         const MSG::Level     & lvl = MSG::INFO           ,
                         const StatusCode     & sc  = StatusCode::FAILURE );
  /// exception print and count  
  StatusCode Exception ( const std::string    & msg                        ,   
                         const GaudiException & exc                        , 
                         const MSG::Level     & lvl = MSG::FATAL           ,
                         const StatusCode     & sc  = StatusCode::FAILURE );
  /// exception print and count  
  StatusCode Exception ( const std::string    & msg                        ,  
                         const std::exception & exc                        , 
                         const MSG::Level     & lvl = MSG::FATAL           ,
                         const StatusCode     & sc  = StatusCode::FAILURE );
  /// exception print and count  
  StatusCode Exception ( const std::string    & msg                        ,  
                         const MSG::Level     & lvl = MSG::FATAL           ,
                         const StatusCode     & sc  = StatusCode::FAILURE );
  ///
  /// set new converter name 
  virtual void  setName      ( const std::string& newName ) 
  { m_name       = newName ; } 
  /// set name of detector data service 
  virtual void  setDetSvcName( const std::string& newName ) 
  { m_detSvcName = newName ; }
  ///  
  /// standard constructor 
  SoCaloBaseCnv ( const CLID&  /* clid */ , ISvcLocator*  /* svcLoc */ );
  /// virtual destructor 
  virtual ~SoCaloBaseCnv();
  ///  
private:
  ///
  /// default constructor is desabled  
  SoCaloBaseCnv           (                      );
  /// copy constructor is desabled 
  SoCaloBaseCnv           ( const SoCaloBaseCnv& );
  /// assignment is disabled 
  SoCaloBaseCnv& operator=( const SoCaloBaseCnv& ); 
  ///
private:
  ///  
  /// So conversion service  
  ISoConversionSvc* m_soSvc      ;
  /// UI service 
  IUserInterfaceSvc* m_uiSvc;
  /// Chrono & Stat service 
  IChronoStatSvc*   m_chronoSvc  ; 
  /// name of this converter 
  std::string       m_name       ; 
  /// detector data service 
  IDataProviderSvc* m_detSvc     ;
  /// name of detector data svc 
  std::string       m_detSvcName ; 
  /// 
};

// ============================================================================
#endif  // SOCALO_SOCALOBASECNV_H 
// ============================================================================



















