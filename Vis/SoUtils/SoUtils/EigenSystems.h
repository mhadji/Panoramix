// $Id: EigenSystems.h,v 1.4 2008-07-28 08:11:22 truf Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.3  2007/03/19 15:23:33  ranjard
// v3r2 - fix for LHCb v22r2
//
// Revision 1.2  2006/03/09 16:48:15  odescham
// v2r1 - migrated to LHCb v20r0 - to be completed
//
// Revision 1.1.1.1  2004/09/08 15:52:31  ibelyaev
// New package: code moved from Vis/SoCalo
// 
// ============================================================================
#ifndef SOUTILS_EIGENSYSTEMS_H 
#define SOUTILS_EIGENSYSTEMS_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL 
// ============================================================================
#include <vector>
//From GaudiKernel
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/StatusCode.h"
// ============================================================================

namespace SoUtils
{
  /** calculate eigenvalues and eigenvectors of symmetric matrix 
   *
   *  see error codes into corresponsing SoCaloUtilsGSL.cpp or 
   *   and SoCaloUtilsNagC.cpp files 
   *
   *  @param matrix reference to symmetric matrix 
   *  @param eigenvalues  (return) vector of eigenvalues 
   *  @param einenvectors (return) vector of eigenvectors
   *  @param status code 
   */
  StatusCode eigensystem 
  ( const Gaudi::SymMatrix3x3&     matrix       ,
    Gaudi::Vector3&              eigenvalues  ,
    std::vector<Gaudi::Vector3>& eigenvectors ) ;
  
  /** calculate eigenvalues of symmetric matrix 
   *
   *  see error codes into corresponsing SoCaloUtilsGSL.cpp or 
   *   and SoCaloUtilsNagC.cpp files 
   *
   *  @param matrix reference to symmetric matrix 
   *  @param eigenvalues  (return) vector of eigenvalues 
   *  @param status code 
   */
  StatusCode eigenvalues 
  ( const Gaudi::SymMatrix3x3&     matrix       ,
    Gaudi::Vector3&              eigenvalues  );
  
}

// ============================================================================
// The END 
// ============================================================================
#endif // SOUTILS_EIGENSYSTEMS_H
// ============================================================================
