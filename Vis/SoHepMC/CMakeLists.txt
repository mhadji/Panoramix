################################################################################
# Package: SoHepMC
################################################################################
gaudi_subdir(SoHepMC v6r7)

gaudi_depends_on_subdirs(Event/GenEvent
                         Kernel/PartProp
                         Vis/SoUtils
                         Vis/OnXSvc)

find_package(OpenScientist REQUIRED COMPONENTS Vis)

if(GAUDI_HIDE_WARNINGS)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-overloaded-virtual")
endif()

gaudi_add_module(SoHepMC
                 src/SoHepMCDLL.cpp
                 #src/SoHepMCSvc.cpp
                 #src/SoHepMCEventCnv.cpp
                 #src/Types.cpp
                 INCLUDE_DIRS Vis/OnXSvc OpenScientist
                 LINK_LIBRARIES GenEvent GaudiKernel PartPropLib OpenScientist)

string(REPLACE "-Wsuggest-override" " " CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})

