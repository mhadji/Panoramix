#ifndef SoHepMC_SoHepMCSvc_h
#define SoHepMC_SoHepMCSvc_h

// Gaudi :
#include <GaudiKernel/Service.h>
#include "Kernel/IParticlePropertySvc.h"

class IUserInterfaceSvc;
class IDataProviderSvc;
class IParticlePropertySvc;
class IToolSvc;

class SoHepMCSvc : public Service {
public: //IService
  virtual StatusCode initialize();
  virtual StatusCode finalize();

  SoHepMCSvc(const std::string&,ISvcLocator*);
  virtual ~SoHepMCSvc();
protected:
  IUserInterfaceSvc* m_uiSvc;
  IDataProviderSvc* m_eventDataSvc;
  LHCb::IParticlePropertySvc* m_particlePropertySvc;
};

#endif
