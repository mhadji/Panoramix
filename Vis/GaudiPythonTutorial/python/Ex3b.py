from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2010"
lhcbApp.Simulation = True
lhcbApp.DDDBtag   =  "head-20101206"
lhcbApp.CondDBtag =  "sim-20101210-vc-md100" 
 
appConf = ApplicationMgr(OutputLevel = INFO, AppName     = 'Ex3b') 

DataOnDemandSvc().AlgMap['/Event/MC/Particles'] =  'UnpackMCParticle' 
DataOnDemandSvc().AlgMap['/Event/MC/Vertices']  =  'UnpackMCVertex' 
 
for c in appConf.getProperties(): 
 print '%20s'%(c), appConf.getProperties()[c] 
 
import GaudiPython

appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/MC2010_00008281_00000001_1.dst']) 
evt = appMgr.evtsvc()

appMgr.run(1)
evt.dump()
print evt['MC/Particles'].size()
print evt['MC/Vertices'].size()
evt.dump()



