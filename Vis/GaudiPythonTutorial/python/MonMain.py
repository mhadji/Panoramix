from ROOT import TH1F, TBrowser, TCanvas
from LHCbConfig import *
import pyMon
lhcbApp.DataType = "DC06"

appConf = ApplicationMgr(OutputLevel = INFO,AppName = 'MonMain')

#### three additional lines for main program
# add python alg to Gaudi
appConf.TopAlg +=['MyAlg']

#here it does not work
foo = pyMon.MyAlg()

import GaudiPython
appMgr = GaudiPython.AppMgr()

# here it works
#foo = pyMon.MyAlg()
#appMgr.addAlgorithm(pyMon.MyAlg())
####


sel    = appMgr.evtsel()
sel.open(['PFN:$AFSROOT/cern.ch/lhcb/group/tracking/vol1/00001378_00000002_5.dst'])

print 'start with event loop'
appMgr.run(10)
appMgr.stop()
appMgr.finalize()


