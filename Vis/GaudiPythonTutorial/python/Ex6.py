import os 
from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"

# use random misaligned velo 
importOptions('VeloAlignment_0.py')
  
appConf = ApplicationMgr(AppName = 'Ex6', OutputLevel = INFO )

import GaudiPython

appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
det    = appMgr.detSvc() 
lhcb   = det['/dd/Structure/LHCb']

sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst']) 
evt    = appMgr.evtsvc()
tsvc   = appMgr.toolsvc()

velopotool = tsvc.create('VeloClusterPosition',interface='IVeloClusterPosition')

appMgr.run(1)  
veloClusters = evt['Raw/Velo/Clusters']
# The key for VeloClusters is a VeloChannelID object, 
# therefore direct access by index, evt['Raw/Velo/Clusters'][0], does not work.
aCluster = veloClusters.containedObjects()[0]
clusInfo = velopotool.position(aCluster)
print clusInfo.fractionalError

