from PanoramixSys.Configuration import *
from Configurables import HltConf

hltconf = HltConf()
hltconf.hltType = 'Hlt1'
hltconf.Verbose = True
ApplicationMgr().ExtSvc += [ "LoKiSvc" ,"HltANNSvc"]
l0seq = GaudiSequencer("seqL0")
ApplicationMgr().TopAlg = [ l0seq ]
L0Conf().setProp( "L0Sequencer", l0seq )
L0Conf().setProp( "ReplaceL0BanksWithEmulated", True ) 
ApplicationMgr().TopAlg += [ GaudiSequencer('Hlt') ]
EventSelector().PrintFreq  = 1000

files = []
file  = 'PFN:/castor/cern.ch/grid/lhcb/production/DC06/L0-v1-lumi2/00001959/DST/0000/00001959_0000XXXX_1.dst' 
for n in range(1,99) : 
  ff = file.replace('XXXX','%(X)04d'%{'X':n})
  x  = os.system('nsls '+ff.replace('PFN:',''))
  if x == 0 :  files.append(ff)

PanoramixSys().setProp('User_file',files)


