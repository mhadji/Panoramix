from ROOT import TFile,TCanvas,TH1F,TH2F
debug = False
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2009"

InputCopyStream().Output   = "DATAFILE='PFN:beamgas.dst' TYP='POOL_ROOTTREE' OPT='REC' " 

appConf = ApplicationMgr( OutputLevel = INFO, AppName = 'protonDecay',OutStream = [InputCopyStream()] )

EventSelector().PrintFreq = 1000

import GaudiPython
from processing import Pool

def processFile(file) :
  ff = str(file.__hash__() )
  h_beam    = TH1F('h_beam_'+ff,'beam energie - 5TeV ',100,-1.,1.)
  h_decay   = TH1F('h_decay_'+ff,'decay z of proton from elastic collissions ',1000,-5000.,15000.)
  h_energy  = TH2F('h_energy_'+ff,'sum of energies of decay products - 7TeV vs baryon nr ',100,-10000.,20000.,10,-0.5,9.5)
  h_xy      = TH2F('h_xy_'+ff,'x/y ',100,-1.,1.,100,-1.,1.)
  
  appMgr = GaudiPython.AppMgr()
  appMgr.algorithm('InputCopyStream').Enable = False  
  sel = appMgr.evtsel()
  sel.open([file])
  evt = appMgr.evtsvc()
  while 0 < 1:
   appMgr.run(1)
# check if there are still valid events
   if not evt['MC/Particles'] : break   
   if evt['Gen/Collisions'].size() != 1 : continue  
   col = evt['Gen/Collisions'][0]
   proc = col.processType()
   if proc == 91 : 
     
     for p in evt['MC/Particles'] :
       if p.particleID().pid() != 2212 : continue
       vx = p.originVertex()
       if vx.type() != vx.ppCollision:  continue        
       h_beam.Fill(p.momentum().E()-5000000.)
       for e in p.endVertices() : 
        z = e.position().z()
        sc = h_decay.Fill(z)     
        if z>-4000. and z<10000. : 
          pos = e.position()
          xx = pos.x()
          yy = pos.y()
          h_xy.Fill(xx,yy)
          print 'proton decay ?',evt['DAQ/ODIN'].eventNumber(), xx, yy, z, p.momentum().E()
          # appMgr.algorithm('InputCopyStream').execute()
          dlist = []
          for x in evt['MC/Particles'] : 
            if x.mother()  : 
             if x.mother().key() == p.key() : 
              dlist.append(x)
          totE=0
          b = 0
          for d in dlist :
            totE += d.momentum().E()
            if d.particleID().isBaryon():
               b+= float(d.particleID().threeCharge())/3.
          if z > 0. :      
            sc = h_energy.Fill(totE-5000000.,b)     

  return h_decay,h_energy,h_beam,h_xy
     
if __name__ == '__main__' :
 files = []
 file  = 'PFN:castor:/castor/cern.ch/grid/lhcb/MC/MC09/DST/00004827/0000/00004827_00000XXX_1.dst'
 for n in range(1,20) : 
  ff = file.replace('XXX','%(X)03d'%{'X':n})
  x  = os.system('nsls '+ff.replace('PFN:castor:',''))
  if x == 0 :  files.append(ff)
                                                                                                          
  #------Use up to 8 processes                                                                               
 n = 4
 pool = Pool(n)
 h_decay  = None
 h_energy = None
 h_beam = None
 h_xy = None

 if not debug : 
    result = pool.map_async(processFile, files)    
    for h in result.get(timeout=90000) : 
     if h_decay : h_decay.Add(h[0])
     else       : h_decay = h[0]
     if h_energy : h_energy.Add(h[1])
     else       : h_energy = h[1]
     if h_beam : h_beam.Add(h[2])
     else       : h_beam = h[2]
     if h_xy : h_xy.Add(h[3])
     else       : h_xy = h[3]
 else : 
    h_decay,h_energy,h_beam,h_xy =  processFile(files[0])
 h_decay.Draw()
 h_energy.Draw('box')

