from ROOT import TFile,TH1F,TH2F,TCanvas,TBrowser,gStyle,TText,TMath,TF1,gROOT,MakeNullPointer,gSystem
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2010"
lhcbApp.Simulation = True     
lhcbApp.DDDBtag   =  "head-20101206"
lhcbApp.CondDBtag =  "sim-20101210-vc-md100" 

appConf = ApplicationMgr( OutputLevel = INFO, AppName = 'KsCheck' )

from Configurables import NoPIDsParticleMaker
PreLoadPions = NoPIDsParticleMaker('PreLoadPions')
PreLoadPions.Particle = 'pions'
appConf.TopAlg += [PreLoadPions]

EventSelector().PrintFreq = 100


import GaudiPython
from array import array
import math
 
appMgr = GaudiPython.AppMgr()

sel  = appMgr.evtsel()
evt  = appMgr.evtsvc()
his  = appMgr.histsvc()
det  = appMgr.detsvc()
partsvc = appMgr.ppSvc()
msg  = appMgr.service('MessageSvc', 'IMessageSvc')
dps  = appMgr.service('EventDataSvc', 'IDataProviderSvc')

sel.open(['$PANORAMIXDATA/MC2010_00008281_00000001_1.dst'])

from LinkerInstances.eventassoc import *
gbl = GaudiPython.gbl
ParticleID     = gbl.LHCb.ParticleID
MCTrackInfo    = gbl.MCTrackInfo
XYZVector      = gbl.ROOT.Math.XYZVector
Particle       = gbl.LHCb.Particle
LorentzVector  = gbl.Gaudi.LorentzVector
ParticleID     = gbl.LHCb.ParticleID
MCParticle     = gbl.LHCb.MCParticle
Track          = gbl.LHCb.Track
Vertex         = gbl.LHCb.Vertex
part           = MakeNullPointer(MCParticle)
MCDecayFinder  = appMgr.toolsvc().create('MCDecayFinder', interface='IMCDecayFinder')
OfflineVertexFitter = appMgr.toolsvc().create('OfflineVertexFitter', interface='IVertexFit')
#pvfit = appMgr.toolsvc().create('PVFitterTool', interface='IPVFitterTool')

h_Ks           = TH2F('h_Ks',' lowest momentum vs. z of Ks vertex',100,-200.,2500.,100,0.,50000.)
h_Ks_rad       = TH2F('h_Ks_rad',' radius vs. z of Ks vertex',100,-200.,2500.,200,0.,1000.)
h_Ks_rec       = TH2F('h_Ks_rec',   ' lowest momentum vs. z of Ks vertex, reconstructed tracks',100,-200.,2500.,100,0.,50000.)
h_Ks_mc_rec    = TH2F('h_Ks_mc_rec',' lowest momentum vs. z of Ks vertex, reconstructible and reconstructed tracks',100,-200.,2500.,100,0.,50000.)
h_Ks_rec_vv    = TH2F('h_Ks_rec_vv',' lowest momentum vs. z of Ks vertex, vv tracks',100,-200.,2500.,100,0.,50000.)
h_Ks_rec_ll    = TH2F('h_Ks_rec_ll',' lowest momentum vs. z of Ks vertex, ll tracks',100,-200.,2500.,100,0.,50000.)
h_Ks_rec_dd    = TH2F('h_Ks_rec_dd',' lowest momentum vs. z of Ks vertex, dd tracks',100,-200.,2500.,100,0.,50000.)
h_Ks_mc        = TH2F('h_Ks_mc',   ' lowest momentum vs. z of Ks vertex, reconstructible',100,-200.,2500.,100,0.,50000.)
h_Ks_mc_vv     = TH2F('h_Ks_mc_vv',' lowest momentum vs. z of Ks vertex, vv tracks, reconstructible',100,-200.,2500.,100,0.,50000.)
h_Ks_mc_ll     = TH2F('h_Ks_mc_ll',' lowest momentum vs. z of Ks vertex, ll tracks, reconstructible',100,-200.,2500.,100,0.,50000.)
h_Ks_mc_dd     = TH2F('h_Ks_mc_dd',' lowest momentum vs. z of Ks vertex, dd tracks, reconstructible',100,-200.,2500.,100,0.,50000.)
h_type         = TH2F('h_type','track type daughter 1 vs. daughter 2',8,-0.5,7.5,8,-0.5,7.5)

h_Ks_pullz_dd  = TH1F('h_Ks_pullz_dd',' z rec - z of Ks vertex, dd tracks',100,-200.,200.)
h_Ks_pullz_ll  = TH1F('h_Ks_pullz_ll',' z rec - z of Ks vertex, ll tracks',100,-20.,20.)
h_Ks_pullz_vv  = TH1F('h_Ks_pullz_vv',' z rec - z of Ks vertex, with velo tracks',100,-20.,20.)

h_Ks_chi2_dd  = TH1F('h_Ks_chi2_dd',' chi2 per dof, dd tracks',100,0.,10.)
h_Ks_chi2_ll  = TH1F('h_Ks_chi_ll',' chi2 per dof, vertex, ll tracks',100,0.,10.)
h_Ks_chi2_vv  = TH1F('h_Ks_chi2_vv',' chi2 per dof, with velo tracks',100,0.,10.)

h_Ks_ratio     = TH2F('h_Ks_ratio','ratio of pz1/pz2',100,0.,10.,100,0.,10.)
h_Ks_mcratio   = TH1F('h_Ks_mcratio','diff ratio of pz1/pz2, MC-rec',100,-0.1,0.1)
h_Ks_perpangle = TH1F('h_Ks_perpangle','angle decay plane / Ks flight direction Jpsi MC',100,-0.1,0.1)
h_Ks_perpanglerec = TH1F('h_Ks_perpanglerec','angle decay plane / Ks flight direction rec',100,-0.1,0.1)

h_Bmass    = TH1F('h_Bmass','B mass',100,4.5,6.5)
h_Bmass_ll = TH1F('h_Bmass_ll','B mass',100,4.5,6.5)

mass_ks = partsvc.find(ParticleID(310)).mass()
mass_pi = partsvc.find(ParticleID(211)).mass()
mass_mu = partsvc.find(ParticleID(13)).mass() 

def makeKs(ratio,sx1,sy1,sx2,sy2,vx) :
    ratiosq = ratio*ratio
    mass_kssq = mass_ks*mass_ks
    part1   = 1.+sx1*sx2+sy1*sy2
    part2   = sx2*sx2*(1. + sy1*sy1) + (sy1 - sy2)*(sy1 - sy2) -  2.*sx1*sx2*(1. + sy1*sy2) + sx1*sx1*(1. + sy2*sy2)
    part3   = ratio*sy1+sy2
    part4   = part3*part3 + (1. + ratio)*(1. + ratio) + ratiosq*sx1*sx1+2.*ratio*sx1*sx2+sx2*sx2
    part5   = mass_kssq*part1*ratio-part4*mass_pi*mass_pi 
    part6   = part2*ratiosq*(mass_kssq*mass_kssq-4.*mass_kssq*mass_pi*mass_pi)
    part7   = math.sqrt(part6 + part5*part5)
    part8   = mass_kssq*part1*ratio-part4*mass_pi*mass_pi
    pz2     = math.sqrt( (part8+part7)/(part2*ratiosq) ) / math.sqrt(2.)
    pz1     = pz2*ratio
    Ks = Particle(ParticleID(310))
    Ks.setMeasuredMass(mass_ks)
    mom = LorentzVector(pz1*sx1+pz2*sx2,pz1*sy1+pz2*sy2,pz1+pz2,0.)
    p = mom.P()
    mom.SetE(math.sqrt(mass_kssq+p*p))
    Ks.setMomentum(mom)
    Ks.setEndVertex(vx)
    return Ks

def findPart(t1,t2):
 p1 = None
 p2 = None
 for p in pa:
   if p.proto().track().key()==t1: p1 = p
   if p.proto().track().key()==t2: p2 = p
 return p1,p2
    
    
    
decaystr = '[B0,B+,B_s0]cc'
stop = False
k = 500
while k > 0 :
  k -=1 
  appMgr.run(1)
  cont = evt['Rec/Track/Best']
  pa   = evt['Phys/PreLoadPions/Particles']  
  try :
    n = cont.size()
  except: 
# probably reached end of file, so make a break
    break  

# get MC particles container  
  contmc = evt['MC/Particles']
  trackinfo = MCTrackInfo(dps, msg)
  decaylist = []
  MCDecayFinder.setDecay(decaystr)
  if MCDecayFinder.hasDecay(contmc) :
   part = MakeNullPointer(MCParticle)
   while MCDecayFinder.findDecay(contmc,part)>0 :
    decaylist.append(contmc[part.key()])
   for decay in decaylist :
    daughters = GaudiPython.gbl.std.vector('const LHCb::MCParticle*')()    
    MCDecayFinder.descendants(decay,daughters)
    lfrommc = linkedFrom(Track,MCParticle,'Rec/Track/Best')       
##### find the Ks in the B decay chain
    KsList = []
    for mcp in daughters : 
     if mcp.particleID().pid() == 310 :  KsList.append(mcp)     
    for ks in KsList : 
     kskey = ks.key()
     ksdaughters = []
     for daughter in daughters :
      mother = daughter.mother()
      if not(mother)            : continue
      if mother.key() != kskey  : continue
      if daughter.particleID().abspid() != 22 : ksdaughters.append(daughter)

     if len(ksdaughters) != 2 : continue
     pmin=min(ksdaughters[0].p(),ksdaughters[1].p()) 
     z   = ks.endVertices()[0].position().z()
     rho = ks.endVertices()[0].position().rho()
     p   = ks.p()
     success = h_Ks.Fill(z,pmin)
     success = h_Ks_rad.Fill(z,rho)
          
     trec_ll1 = trackinfo.hasVeloAndT(ksdaughters[0])
     trec_ll2 = trackinfo.hasVeloAndT(ksdaughters[1])
     trec_vv1 = trackinfo.hasVelo(ksdaughters[0])
     trec_vv2 = trackinfo.hasVelo(ksdaughters[1])
     trec_dd1 = trackinfo.hasT(ksdaughters[0]) and trackinfo.hasTT(ksdaughters[0])
     trec_dd2 = trackinfo.hasT(ksdaughters[1]) and trackinfo.hasTT(ksdaughters[1])             

# both pions reconstructible in Velo
     if trec_vv1 and trec_vv2 : success = h_Ks_mc_vv.Fill(z,pmin)
# both pions reconstructible as long
     if trec_ll1 and trec_ll2 : success = h_Ks_mc_ll.Fill(z,pmin)
# both pions reconstructible as dd
     if trec_dd1 and trec_dd2 : success = h_Ks_mc_dd.Fill(z,pmin)
# both pions somehow reconstructible
     if (trec_ll1 or trec_dd1) and (trec_ll2 or trec_dd2) : success = h_Ks_mc.Fill(z,pmin)         
       
     tr1 = lfrommc.first(ksdaughters[0])
     tr2 = lfrommc.first(ksdaughters[1])
     if not(tr1) : 
       tr1type = 0
     else :
       tr1type =  tr1.type()
     if not(tr2) : 
       tr2type = 0
     else :
       tr2type =  tr2.type()
     h_type.Fill(tr1type,tr2type) 
     
     if not(tr1) or not(tr2) : continue
     if tr1type == tr1.Ttrack or tr2type == tr2.Ttrack : continue 
     success = h_Ks_rec.Fill(z,pmin)    
# make Ks vertex
     KsVx = Vertex()
     Ks = Particle()
     
     pa1,pa2 = findPart(tr1.key(),tr2.key())
     if not pa1 or not pa2 : continue
     OfflineVertexFitter.fit(pa1,pa2,KsVx,Ks)     
     rvx  = KsVx.position()
     zrec = rvx.z()
# find MC origin vertex
     ovx =  ks.originVertex().position()
     sks = XYZVector(rvx.x() - ovx.x(),rvx.y() - ovx.y(),rvx.z() - ovx.z()) 
     sxks = sks.x()/sks.z() 
     syks = sks.y()/sks.z()   
     ratiopx = - (sxks - tr2.firstState().tx()) / (sxks - tr1.firstState().tx())
     ratiopy = - (syks - tr2.firstState().ty()) / (syks - tr1.firstState().ty())
     aratio  = (ratiopx+ratiopy)/2.
     mcratio = ksdaughters[0].momentum().z() / ksdaughters[1].momentum().z() 
     perpvec = tr1.firstState().slopes().Cross(tr2.firstState().slopes())
     perpangle = perpvec.Dot(sks)

# find mumu vertex
     perpanglerec  = -10.
     jpsidaughters = []
     JpsiVx = Vertex()
     Bkey = ks.mother().key()
     for daughter in daughters :
      mother = daughter.mother()
      if not(mother)           : continue
      grama = mother.mother()
      if not(grama)           : continue
      if grama.key() != Bkey  : continue
      if daughter.particleID().abspid() == 13   : jpsidaughters.append(daughter)
     if len(jpsidaughters) == 2 : 
      mu1 = lfrommc.first(jpsidaughters[0])
      mu2 = lfrommc.first(jpsidaughters[1])
      if mu1 and mu2 :  
          if mu1.type() == mu1.Long and mu2.type() == mu2.Long :
           pa1,pa2 = findPart(mu1.key(),mu2.key())
           if not pa1 or not pa2 : continue
# fitted with pion !
           Jpsi = Particle(ParticleID(443))
           OfflineVertexFitter.fit(pa1,pa2,JpsiVx,Jpsi) 
           ovx          = JpsiVx.position()
           sksrec       = XYZVector(rvx.x() - ovx.x(),rvx.y() - ovx.y(),rvx.z() - ovx.z()) 
           perpanglerec = perpvec.Dot(sksrec)       
          
     if (trec_ll1 or trec_dd1) and (trec_ll2 or trec_dd2) : 
        success = h_Ks_mc_rec.Fill(z,pmin) 

# Velo VeloR  Long   Upstream Downstream Ttrack
#  1     2      3      4         5          6
     if  tr1.type() != tr1.Ttrack and tr2.type() != tr2.Ttrack and tr1.type() != tr1.Downstream and tr2.type() != tr2.Downstream : 
      success = h_Ks_rec_vv.Fill(z,pmin)    
     if  tr1.type() == tr1.Long and tr2.type() == tr2.Long : 
      success = h_Ks_rec_ll.Fill(z,pmin)    
      success = h_Ks_pullz_ll.Fill(zrec-z) 
      success = h_Ks_chi2_ll.Fill(tr1.chi2PerDoF())
      success = h_Ks_chi2_ll.Fill(tr2.chi2PerDoF())

     if  tr1.type() == tr1.Downstream and tr2.type() == tr2.Downstream : 
      success = h_Ks_rec_dd.Fill(z,pmin)    
      success = h_Ks_pullz_dd.Fill(zrec-z) 
      success = h_Ks_chi2_dd.Fill(tr1.chi2PerDoF())
      success = h_Ks_chi2_dd.Fill(tr2.chi2PerDoF())
#      print evt['DAQ/ODIN'].eventNumber(),tr1.key(),tr2.key()
     
     if (tr1.type() != tr1.Ttrack and tr2.type() != tr2.Ttrack) and (tr1.type() == tr1.Velo or tr2.type() == tr2.Velo or tr1.type() == tr1.Upstream or tr2.type() == tr2.Upstream ) :
          success = h_Ks_pullz_vv.Fill(zrec-z) 
          success = h_Ks_ratio.Fill(ratiopx,ratiopy) 
          success = h_Ks_chi2_vv.Fill(tr1.chi2PerDoF())
          success = h_Ks_chi2_vv.Fill(tr2.chi2PerDoF())
          success = h_Ks_mcratio.Fill(mcratio-aratio) 
          success = h_Ks_perpangle.Fill(perpangle)  
          if  perpanglerec > -5 :
            success = h_Ks_perpanglerec.Fill(perpanglerec)          
            Ks = makeKs(aratio,tr1.firstState().tx(),tr1.firstState().ty(),tr2.firstState().tx(),tr2.firstState().ty(),KsVx)
            print 'rec Ks:',Ks.momentum().P(),KsVx.position().x(),KsVx.position().y(),KsVx.position().z()   
            ksmc = ksdaughters[0].mother()
            ksmcVx = ksdaughters[0].originVertex()
            print 'MC  Ks:',ksmc.momentum().P(),ksmcVx.position().x(),ksmcVx.position().y(),ksmcVx.position().z()   
            mu1mom  = pa[mu1.key()].momentum()
            mu1momE = math.sqrt(mu1mom.P()*mu1mom.P()+mass_mu*mass_mu)
            mu2mom  = pa[mu2.key()].momentum()
            mu2momE = math.sqrt(mu2mom.P()*mu2mom.P()+mass_mu*mass_mu)
            Jpsi = Particle(ParticleID(443))
            mom = LorentzVector(mu1mom.x()+mu2mom.x(),\
                                mu1mom.y()+mu2mom.y(),\
                                mu1mom.z()+mu2mom.z(),\
                                mu1momE + mu1momE )
            Jpsi.setMomentum(mom)
            Jpsi.setEndVertex(JpsiVx)
            print 'rec Jpsi:',Jpsi.momentum().P(),JpsiVx.position().x(),JpsiVx.position().y(),JpsiVx.position().z()
            jpsimc = jpsidaughters[0].mother()   
            jpsimcVx = jpsidaughters[0].originVertex()
            print 'MC  Jpsi:',jpsimc.momentum().P(),jpsimcVx.position().x(),jpsimcVx.position().y(),jpsimcVx.position().z()   
            Bmes = Particle(ParticleID(511))
            Bmes.setEndVertex(JpsiVx)
            mom = LorentzVector(Jpsi.momentum().x()+Ks.momentum().x(),\
                                Jpsi.momentum().y()+Ks.momentum().y(),\
                                Jpsi.momentum().z()+Ks.momentum().z(),\
                                Jpsi.momentum().E()+Ks.momentum().E() )
            Bmes.setMomentum(mom)
            h_Bmass.Fill(Bmes.momentum().mass()/1000.) 
            print 'rec B:',Bmes.momentum().mass(),Bmes.momentum().P(),Bmes.endVertex().position().z()   
            Bmesmc =  contmc[Bkey]
            print 'MC  B:',Bmesmc.momentum().mass(),Bmesmc.momentum().P(),Bmesmc.originVertex().position().z()   
# break to look at event from panoramix
#            stop = True
#            k = -100000000   
          
          
          print  tr1.key(),tr2.key()

     if trec_ll1 and trec_ll2 and perpanglerec > -5 :

            Ks = Particle(ParticleID(310))
            Ks.setMeasuredMass(mass_ks)
            mom = LorentzVector(pa1.momentum().x()+pa2.momentum().x(),\
                                pa1.momentum().y()+pa2.momentum().y(),\
                                pa1.momentum().z()+pa2.momentum().z(),\
                                pa1.momentum().E()+pa2.momentum().E() )
            Ks.setMomentum(mom)
            Ks.setEndVertex(KsVx)
            print 'rec Ks ll:',Ks.momentum().P(),KsVx.position().x(),KsVx.position().y(),KsVx.position().z()   
            ksmc = ksdaughters[0].mother()
            ksmcVx = ksdaughters[0].originVertex()
            print 'MC  Ks ll:',ksmc.momentum().P(),ksmcVx.position().x(),ksmcVx.position().y(),ksmcVx.position().z()   
            mu1mom  = pa1.momentum()
            mu1momE = math.sqrt(mu1mom.P()*mu1mom.P()+mass_mu*mass_mu)
            mu2mom  = pa2.momentum()
            mu2momE = math.sqrt(mu2mom.P()*mu2mom.P()+mass_mu*mass_mu)
            Jpsi = Particle(ParticleID(443))
            mom = LorentzVector(mu1mom.x()+mu2mom.x(),\
                                mu1mom.y()+mu2mom.y(),\
                                mu1mom.z()+mu2mom.z(),\
                                mu1momE + mu1momE )
            Jpsi.setMomentum(mom)
            Jpsi.setEndVertex(JpsiVx)
            print 'rec Jpsi ll:',Jpsi.momentum().P(),JpsiVx.position().x(),JpsiVx.position().y(),JpsiVx.position().z()
            jpsimc = jpsidaughters[0].mother()   
            jpsimcVx = jpsidaughters[0].originVertex()
            print 'MC  Jpsi ll:',jpsimc.momentum().P(),jpsimcVx.position().x(),jpsimcVx.position().y(),jpsimcVx.position().z()   
            Bmes = Particle(ParticleID(511))
            Bmes.setEndVertex(JpsiVx)
            mom = LorentzVector(Jpsi.momentum().x()+Ks.momentum().x(),\
                                Jpsi.momentum().y()+Ks.momentum().y(),\
                                Jpsi.momentum().z()+Ks.momentum().z(),\
                                Jpsi.momentum().E()+Ks.momentum().E() )
            Bmes.setMomentum(mom)
            h_Bmass_ll.Fill(Bmes.momentum().mass()/1000.) 
            print 'rec B ll:',Bmes.momentum().mass(),Bmes.momentum().P(),Bmes.endVertex().position().z()   
            Bmesmc =  contmc[Bkey]
            print 'MC  B ll:',Bmesmc.momentum().mass(),Bmesmc.momentum().P(),Bmesmc.originVertex().position().z()   


if stop :
  from panoramixmodule import *     
  Style().setLineWidth(2.0) 
  Style().setColor('blue')
  Object_visualize(evt['Rec/Track/Best'][4])
  toui()
     
f=TFile('KsCheck.root','recreate')
for h in gROOT.GetList() : 
 h.Write()
f.Close()
gStyle.SetOptStat(1100011)
      
      
ttype = ['not reconstr.','Velo', 'VeloR',  'Long',   'Upstream', 'Downstream', 'Ttrack']
for l in ttype : 
 h_type.GetXaxis().SetBinLabel(ttype.index(l)+1,l)      
 h_type.GetYaxis().SetBinLabel(ttype.index(l)+1,l)      

h_Ks_pullz_dd.Fit('gaus')                                      
h_Ks_pullz_ll.Fit('gaus')                                      
h_Ks_pullz_vv.Fit('gaus')                                      
h_Ks_perpanglerec.Fit('gaus')                                      
gStyle.SetOptFit(111)
      
tks = TCanvas('tks','Ks plots',800,1200)  
tks.Divide(2,5)                                         
tks.cd(1)      
h_Ks.DrawCopy('box')
tks.cd(2)      
h_type.DrawCopy('text')
tks.cd(3)      
h_Ks_mc.DrawCopy('box')
tks.cd(4)      
h_Ks_rec.DrawCopy('box')
tks.cd(6)      
h_Ks_mc_rec.DrawCopy('box')
tks.cd(7)      
h_Ks_mc_ll.DrawCopy('box')
tks.cd(8)      
h_Ks_rec_ll.DrawCopy('box')
tks.cd(9)      
h_Ks_mc_dd.DrawCopy('box')
tks.cd(10)      
h_Ks_rec_dd.DrawCopy('box')

tksz = TCanvas('tksz','Ks vertex residual',800,800)  
tksz.Divide(2,2)   
tksz.cd(1)      
h_Ks_pullz_ll.Draw()
tksz.cd(2)      
h_Ks_pullz_vv.Draw()
tksz.cd(3)      
h_Ks_pullz_dd.Draw()
tksz.cd(4)   
h_Ks_perpanglerec.Draw()
#h_Ks_perpangle.Draw('same')
tksz.Print('ksvx')

tbs = TCanvas('tbs','B mass',600,400)  
tbs.Divide(2,1)   
tbs.cd(1)
h_Bmass.Fit('gaus')
tbs.cd(2)
h_Bmass_ll.Fit('gaus')
tbs.Print('Bmass.gif')

gStyle.SetOptStat(1111111)
tc2 = TCanvas('tc2','track chi2',800,800)  
tc2.Divide(2,2)   
tc2.cd(1)
h_Ks_chi2_ll.Draw()
tc2.cd(2)      
h_Ks_chi2_vv.Draw()
tc2.cd(3)      
h_Ks_chi2_dd.Draw()

#efficiencies
h_Ks_mc_ll_py = h_Ks_mc_ll.ProjectionY()
h_Ks_rec_ll_py = h_Ks_rec_ll.ProjectionY()
h_Ks_mc_dd_py = h_Ks_mc_dd.ProjectionY()
h_Ks_rec_dd_py = h_Ks_rec_dd.ProjectionY()
h_Ks_rec_dd_eff = h_Ks_rec_dd_py.Clone('dd efficiency')
h_Ks_rec_ll_eff = h_Ks_rec_ll_py.Clone('ll efficiency')

h_Ks_rec_dd_eff.Divide(h_Ks_mc_dd_py)
h_Ks_rec_ll_eff.Divide(h_Ks_mc_ll_py)
tce = TCanvas('tce','efficiencies',800,800)  
tce.Divide(2,2)   
tce.cd(1)
h_Ks_rec_dd_eff.Draw()
tce.cd(2)
h_Ks_rec_ll_eff.Draw()
tce.cd(3)
h_Ks_mc_dd_py.Draw()
tce.cd(4)
h_Ks_mc_ll_py.Draw()





