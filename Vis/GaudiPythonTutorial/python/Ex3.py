from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2010"
lhcbApp.Simulation = True

appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'Ex3')
appConf.TopAlg      += ["UnpackMCParticle","UnpackMCVertex"]

import GaudiPython

appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/MC2010_BsJpsiPhi__00008919_00000068_1.dst']) 
evt = appMgr.evtsvc()

appMgr.run(1)
evt.dump()



