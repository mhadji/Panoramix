# setenv PYTHONPATH /afs/cern.ch/sw/lcg/external/processing/0.51/${CMTCONFIG}/lib/python2.5/site-packages:$PYTHONPATH
from ROOT import TH1F,TFile,gROOT 
from Gaudi.Configuration import *
from GaudiPython.Parallel import Task, WorkManager
from GaudiPython import AppMgr
#from LHCbConfig import *
dataType = 'MC09'
max_Events = 500
debug = False

from Configurables import (Brunel,LHCbApp,TrackSys,ApplicationMgr,AuditorSvc,SequencerTimerTool)
Brunel().DataType         = dataType
Brunel().InputType        = "DST"
Brunel().WithMC           = False
Brunel().Simulation       = True
Brunel().OutputType       = 'None'
Brunel().PrintFreq        = 100
#Brunel().SkipEvents       = 100
if debug :     Brunel().PrintFreq        = 50
LHCbApp().DDDBtag = 'default'
LHCbApp().CondDBtag = 'default'
TrackSys().ExpertTracking += ["simplifiedGeometry"]
appConf = ApplicationMgr( OutputLevel = 3, AppName = 'Ex3_parallel')


from Configurables import (EventNodeKiller, TESCheck,
                           GaudiSequencer)
def doMyChanges():
 evtCheck        = TESCheck("EvtCheck")
 evtCheck.Stop   = False
 mcKiller = EventNodeKiller('MCKiller')
 mcKiller.Nodes  = ["MC"]
 mcKill      = GaudiSequencer("MCKill")
 mcKill.Members = [evtCheck,mcKiller]
 appConf = ApplicationMgr()
 #allTopAlgs = appConf.TopAlg
 appConf.TopAlg = [mcKill,GaudiSequencer('BrunelSequencer')]
appendPostConfigAction(doMyChanges)


class MyTask(Task):
  
  def initializeLocal(self):
    self.output = {'h_chi2' : TH1F('h_chi2','fit chi2',100,0.,10000.) }

  def initializeRemote(self):
  # Use TimingAuditor for timing
   appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
   appConf.AuditAlgorithms = True
   AuditorSvc().Auditors += [ 'TimingAuditor' ] 
   SequencerTimerTool().OutputLevel = 1

  def process(self, file) :
   H = self.output
   appMgr = AppMgr()
   sel = appMgr.evtsel()
   sel.open(file)
   evt = appMgr.evtsvc()
   nevents = 0
   while nevents < max_Events:
    appMgr.run(1)
    nevents+=1
# check if there are still valid events
    if not evt['Rec/Header'] : break
    cont = evt['Rec/Track/Best']
    if cont :
     for t in cont : 
      success = H['h_chi2'].Fill(t.chi2())
   print 'Finishing.... ', file,' ',H['h_chi2'].GetMean() ,H['h_chi2'].GetEntries() 
   appMgr.stop()

#--- In the master process only....

if __name__ == '__main__' :
 if dataType == "DC06" :
  files = [
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000001_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000002_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000003_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000004_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000005_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000007_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000008_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000009_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000010_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000012_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000013_5.dst'
 ]
 else :
  files  = ['PFN:/data/00004879_00000010_1.dst',
            'PFN:/data/00004879_00000011_1.dst',
            'PFN:/data/00004879_00000012_1.dst',
            'PFN:/data/00004879_00000013_1.dst']

 if not debug : 
  task = MyTask()
  wmgr = WorkManager(ncpus = 4 )
  wmgr.process(task,files[:4]) 
 else :
  task=MyTask()
  task.initializeLocal()
  task.initializeRemote()
  task.process(files[0])         
 H = task.output
 H['h_chi2'].Draw()
 print H['h_chi2'].GetEntries()
















