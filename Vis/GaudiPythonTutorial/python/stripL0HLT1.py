# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "MC09"
lhcbApp.Simulation = True

# options to rerun L0DU only and replace RAWBank 
from Configurables import L0DUAlg,L0DUFromRawAlg,bankKiller
lhcbApp.DataType = "2009"
# L0 configured by hand
l0seq = GaudiSequencer("seqL0")
appConf.TopAlg += [ l0seq ]
removeL0DUBank = bankKiller('RemoveL0DUBank')
removeL0DUBank.BankTypes =["L0DU"]
l0decode = L0DUFromRawAlg()
l0decode.ProcessorDataOnTES = True
l0decode.L0DUReportOnTES    = False
l0DU = L0DUAlg('L0DU')
l0DU.DataLocations   = ["Trig/L0/L0DUData"]
l0DU.ReportLocation = "Trig/L0/L0DUReport"
l0DU.StoreInBuffer = True
l0DU.WriteOnTES  = True
l0DU.TCK = '0xFF68'
l0seq.Members+=[ l0decode,removeL0DUBank, l0DU ]
importOptions('L0Thresholds.py')

from Configurables import HltConf
ApplicationMgr().ExtSvc += [ "LoKiSvc" ]
HltConf().HltType = 'Hlt1'
HltConf().ThresholdSettings = 'Charm_320Vis_300L0_10Hlt1_Aug09'

hlt = GaudiSequencer('Hlt')
appConf.TopAlg += [hlt]
hlt.MeasureTime = True  

#rawwriter  = OutputStream('RawWriter', Preload = False,
#  ItemList = ["/Event#1","/Event/DAQ#1","/Event/DAQ/RawEvent#1","/Event/DAQ/ODIN#1"],
#  Output   = "DATAFILE='PFN:L0Hlt1yes.raw' TYP='POOL_ROOTTREE' OPT='REC' ")

rawwriter          = InputCopyStream('RawWriter')
rawwriter.Output   = "DATAFILE='PFN:myEvents.dst' TYP='POOL_ROOTTREE' OPT='REC' "

appConf    = ApplicationMgr( OutputLevel = INFO, AppName = 'stripL0HLT1') 
appConf.OutStream = [rawwriter]
    
import GaudiPython
from GaudiPython import gbl

files = []
file  = '/castor/cern.ch/grid/lhcb/MC/MC09/DST/00004879/0000/00004879_00000XXX_1.dst'
import os
for n in range(1,5) : 
  ff = file.replace('XXX','%(X)03d'%{'X':n})
  x  = os.system('nsls '+ff)
  if x == 0 :  files.append("DATA='"+ff+"'  TYP='POOL_ROOTTREE' OPT='READ'")
EventSelector(Input = files)

appMgr = GaudiPython.AppMgr()
appMgr.initialize()
appMgr.algorithm('Hlt').Enable       = False
appMgr.algorithm('RawWriter').Enable = False

evt  = appMgr.evtsvc()
for n in range(1000) : 
 appMgr.run(1)
 if not evt['Rec/Header']  : break  # probably end of input
 # check L0
 L0dir = evt['Trig/L0/L0DUReport']
 if L0dir.decision() > 0 : 
   rc = appMgr.algorithm('Hlt').execute() # run Hlt on L0 triggered events
   hlt1 = evt['Hlt/DecReports'].decReport('Hlt1Global')
   if hlt1.decision() > 0 :  
     rc = appMgr.algorithm('RawWriter').execute() # output event             

