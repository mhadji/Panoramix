f_2008   = False

from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
if not f_2008:
    lhcbApp.DataType    = "DC06"
    analysis.DataType   = "DC06"
else:
    lhcbApp.DataType    = "2008"
    analysis.DataType   = "2008"
    lhcbApp.Simulation  = True     
    analysis.Simulation = True
from StrippingSelections.StrippingBs2JpsiPhi import sequence as seqBs2JpsiPhi
appConf.TopAlg+=[seqBs2JpsiPhi.sequence()]

appConf = ApplicationMgr(OutputLevel = INFO, AppName  = 'MuonEff')
EventSelector().PrintFreq = 100

import GaudiPython
from LinkerInstances.eventassoc import *
ParticleID  = GaudiPython.gbl.LHCb.ParticleID
Track       = GaudiPython.gbl.LHCb.Track
MCParticle  = GaudiPython.gbl.LHCb.MCParticle

appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
part = appMgr.ppSvc()

if not f_2008:
 sel.open(['PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v4-lumi2/00002146/DST/0000/00002146_00000001_5.dst'])
 #sel.open(['PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001979/DST/0000/00001979_00000002_5.dst'])
else:
 sel.open(['PFN:castor:/castor/cern.ch/user/t/truf/MC2008/00003402_121314_s.dst'])

evt = appMgr.evtsvc()

h_muonpt_rec  = TH1F('h_muonpt_rec', 'true muon pt of reconstructed muons',100,0.,10000.)
h_muonpt_true = TH1F('h_muonpt_true','true muon pt',100,0.,10000.)

h_muonz_mc = TH1F('h_muonz_mc','z of prod vertex for true muons ',100,-500.,1000.)
h_muonz_rl = TH1F('h_muonz_rl','z of prod vertex for reconstructible muons ',100,-500.,1000.)
h_muonz_re = TH1F('h_muonz_re','z of prod vertex for reconstructed muons ',100,-500.,1000.)
h_muonz_id = TH1F('h_muonz_id','z of prod vertex for reconstructed and pid muons ',100,-500.,1000.)

def muoncheck(t):
  pt = None
  track2mc = linkedTo(MCParticle,Track,'Rec/Track/Best')  
  for mcp in track2mc.range(t) :
   if mcp.particleID().abspid() == 13 :
     ovx =  mcp.originVertex()   
     if abs(ovx.position().z()) < 200. :      
      pt = mcp.pt()
  return pt
  
def myIsmuon(key):
  flag = False
  for m in evt['Rec/Track/Muon']:  
   if key == m.key() : 
    flag = True
    break
  return flag


while 0 < 1:
 appMgr.run(1)
# check if there are still valid events 
 if not evt['Rec/Header'] : break
 cont = evt['Rec/Track/Best']
 for t in cont: 
  # check if linked to muon
  pt = muoncheck(t)
  if not pt : continue
  rc = h_muonpt_true.Fill(pt)
  if myIsmuon(t.key()) : h_muonpt_rec.Fill(pt)
     
h_muonpt_true.Draw()
h_muonpt_rec.Draw('same')



