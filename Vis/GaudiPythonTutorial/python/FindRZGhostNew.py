# run in Panoramix
#/castor/cern.ch/grid/lhcb/production/DC06/L0-v1-lumi2/00001959/DST/0000/00001959_00000001_1.dst' 
#/castor/cern.ch/grid/lhcb/production/DC06/L0-v1-lumi2/00001959/DST/0000/00001959_00000002_1.dst' 

from panoramixmodule import *
# prepare page:
if not ui().findWidget('Viewer_2d') :     
 sys_import('truf_Velo_RZView_setup')

from ROOT import TMath,TH1F,TH2F
from LinkerInstances.eventassoc import linkedFrom, linkedTo  
MCParticle   = GaudiPython.gbl.LHCb.MCParticle 
VeloCluster  = GaudiPython.gbl.LHCb.VeloCluster
XYZPoint     = GaudiPython.gbl.ROOT.Math.XYZPoint
import gaudigadgets
enums = gaudigadgets.getEnumNames('LHCb::Track')
ppSvc = appMgr.ppSvc()

## trackcontainer = 'Rec/Track/Velo'
trackcontainer = 'Hlt1/Track/Forward'

h_rz     = TH1F('h_rz','max fraction of rz clusters',100,-0.1,1.1)
h_rz1     = TH1F('h_rz1','max fraction of rz clusters, 1 ghost',100,-0.1,1.1)
h_rznog  = TH1F('h_rznog','max fraction of rz clusters, no electrons at all',100,-0.1,1.1)
h_rznoe  = TH1F('h_rznoe','max fraction of rz clusters, no electrons',100,-0.1,1.1)
h_rznoe1 = TH1F('h_rznoe1','max fraction of rz clusters, no electrons, 1 ghost',100,-0.1,1.1)
h_3d     = TH1F('h_3d','max fraction of 3d clusters',100,-0.1,1.1)

h_3dxy   = TH2F('h_3dxy','tx / ty of velo ghosts',100,-0.4,0.4,100,-0.4,0.4)
h_xy   = TH2F('h_xy','tx / ty all tracks',100,-0.4,0.4,100,-0.4,0.4)


def findGhost():
 found = False
 mc = evt['MC/Particles']
 vc = evt['Raw/Velo/Clusters']
 tr = evt[trackcontainer]
 if not tr : return False 
 clu2mc  = linkedTo(MCParticle,VeloCluster,'Raw/Velo/Clusters')   
 session().setParameter('modeling.userTrackRange','true') 
 session().setParameter('modeling.trackStartz','-500.')
 session().setParameter('modeling.trackEndz','1000.')
 session().setParameter('modeling.projection','-ZR')
 session().setParameter('modeling.lineWidth','2')  
 nghost = 0
 for t in tr : 
  if t.checkFlag(t.Backward) : continue
  ss = 0
  ssrz = 0
  match = {}
  matchrz = {}
  for l in t.lhcbIDs():
   if not l.isVelo() : continue
   ss +=1
   lv = l.veloID()
   cl = vc.containedObject(lv.channelID())
   for m in clu2mc.range(cl) :
     if match.has_key(m.key()) :   match[m.key()]+=1
     else                      :   match[m.key()] =1  
     if cl.isRType()   :  
      ssrz+=1
      if matchrz.has_key(m.key()) :   matchrz[m.key()]+=1
      else                      :   matchrz[m.key()] =1     
  mlist = match.values()
  mlist.sort(reverse=True)
  maxl = 0
  if len(mlist)>0 : maxl = mlist[0]
  mlistrz = matchrz.values()
  mlistrz.sort(reverse=True)
  maxrz = 0
  if len(mlistrz)>0 : maxrz = mlistrz[0]
  fst = t.firstState()
  h_3d.Fill(float(maxl)/float(ss))
  test1 = 0
  test2 = 0
  if maxl > 3 : h_xy.Fill(fst.tx(),fst.ty())   
  if float(maxl)/ss < 0.7 : 
   h_rz.Fill(float(maxrz)/float(ssrz))
   gconf = False
   for m in mlist:
      if mc[m].particleID().abspid() == 11 : 
       gconf = True
       break
   if not gconf :  sc = h_rznog.Fill(float(maxrz)/float(ssrz))   
   if len(mlistrz)>1 :
     test1 = mc[mlistrz[0]].particleID().abspid() 
     test2 = mc[mlistrz[1]].particleID().abspid() 
     if test1 != 11 and test2 != 11 : h_rznoe.Fill(float(maxrz)/float(ssrz))
   else :
     h_rznoe.Fill(float(maxrz)/ssrz)
   h_3dxy.Fill(fst.tx(),fst.ty())   
   print '========================================================================'
   print 'ghost found', t
   print 'matching', match,   float(maxl)/ss
   print 'rz      ', matchrz, float(maxrz)/ssrz
   nghost+=1
   Style().setColor('green')
   for m in match.keys() :                            
     mother = mc[m].mother()
     mkey = None
     if mother : 
      mkey = mother.key()
      motherid = ppSvc.find(mother.particleID()).name()  
     else : motherid = 'None'           
     print m,ppSvc.find(mc[m].particleID()).name(),mkey,motherid
     sc = Object_visualize(mc[m])   
   Style().setColor('cyan')
   sc = Object_visualize(t)
   Style().setColor('orange')
   for l in t.lhcbIDs():
    if not l.isVelo() : continue
    lv = l.veloID()
    cl = vc.containedObject(lv.channelID())
    sc = Object_visualize(cl)
   # try chi2 matching
   matched,tmatch,chi2min = chi2Match(t)   
   if matched : 
     print 'chi2 matching:',matched,tmatch,ppSvc.find(mc[tmatch].particleID()).name(),chi2min
     Style().setColor('magenta')
     sc = Object_visualize(mc[tmatch])
   found = True 
 if nghost==1 and test1 != 11 and test2 != 11 : h_rznoe1.Fill(float(maxrz)/ssrz)   
 if nghost==1  : h_rz1.Fill(float(maxrz)/ssrz)   
 return found 

def dumpMC(t):
  vc = evt['Raw/Velo/Clusters']  
  clu2mc  = linkedTo(MCParticle,VeloCluster,'Raw/Velo/Clusters')   
  match = {}
  for l in t.lhcbIDs():
   lv = l.veloID()
   cl = vc.containedObject(lv.channelID())
   for m in clu2mc.range(cl) :
     if match.has_key(m.key()) :   match[m.key()]+=1
     else                      :   match[m.key()] =1     
  mlist = match.values()
  Style().setColor('green')
  for m in match.keys() :                            
     sc = Object_visualize(mc[m])   

def run(): 
 for n in range(500):
  EraseEventAllRegions()
  appMgr.run(1)
  if not evt['Rec/Header'] : break
  if findGhost() :
   break 

def plotClusters():
   Style().setColor('violet')
   data_collect(da(),'VeloCluster','isR==false')
   data_visualize(da())  
   Style().setColor('white')
   data_collect(da(),'VeloCluster','isR==true')
   data_visualize(da())

def chi2Match(t,flag=False):  
 fstate = t.firstState()
# ignore correlations 
 tx   = fstate.tx()
 ctx  = fstate.errTx2()    
 ty   = fstate.ty()
 cty  = fstate.errTy2()    
 chi2min = 999999.
 tmatch  = None
 for mcp in evt['MC/Particles']:
  pmom = mcp.momentum()
  sx = pmom.x()/(pmom.z()+0.00001)
  sy = pmom.y()/(pmom.z()+0.00001)    
  p  = pmom.r()
# ignore correlations 
  chi2x = (tx-sx)*(tx-sx)/ctx 
  chi2y = (ty-sy)*(ty-sy)/cty 
  chi2 = chi2x+chi2y 
  if t.type() == t.Long and flag :
     print 'should not be called',flag
     delp = abs((t.p()-p)/(p+0.00001))
     chi2+=(delp*p)/(TMath.Sqrt(fstate.errQOverP2())*t.p()*t.p())
  if chi2 < chi2min : 
       chi2min = chi2
       tmatch = mcp.key()              
 matched = False 
 if chi2min < 50 : matched = True    
 return matched,tmatch,chi2min

from ROOT import TFile
def saveHistos(): 
 f=TFile('VeloGhosts.root','recreate')
 sc = h_rz.Write()
 sc = h_rznoe.Write()
 sc = h_rznoe1.Write()
 sc = h_3d.Write()
 f.Close()

