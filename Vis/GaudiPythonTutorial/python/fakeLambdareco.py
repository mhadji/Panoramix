from ROOT import TH1F, TH2F, TBrowser, TCanvas, MakeNullPointer,gStyle,gROOT
import math
gStyle.SetPalette(1)     
# get the basic configuration from here
from LHCbConfig import *

#dataset = 'BsJpsiPhi'
dataset = 'L0strippedDC06'
appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'fakeLambdareco')

if dataset == 'L0strippedDC06' : 
 f_2008   = False 
else: 
 f_2008   = True
debug    = False

if not f_2008:
    lhcbApp.DataType = "DC06"
else:
    lhcbApp.DataType = "2008"
    lhcbApp.Simulation = True     
EventSelector().PrintFreq = 100
 
import GaudiPython
import gaudigadgets
import GaudiKernel.SystemOfUnits as units

import GaudiPython
import gaudigadgets
import GaudiKernel.SystemOfUnits as units
gbl = GaudiPython.gbl
ParticleID = gbl.LHCb.ParticleID
part  = MakeNullPointer(gbl.LHCb.MCParticle)
mysqrt = gbl.Math.sqrt
MCParticle     = gbl.LHCb.MCParticle
Track          = gbl.LHCb.Track
LorentzVector  = gbl.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')

stableParticles = [211,2212]
 
# name = 'mass_'+partsvc.find(ParticleID(3122)).evtGenName()
name = 'lambda_0'
htemp = {}
htemp[name+'_mass']  = TH1F(name+'_mass','mass of '+name,400,1.0,1.2)
htemp[name+'_eff']   = TH1F(name+'_eff','efficiency of '+name,400,1.0,1.2)
htemp['h_gen']       = TH1F('h_gen','nr of generated Lambda',1,0.,1.)
htemp['h_etapt']     = TH2F('h_etapt','pt vs. eta for reco Lambda',100,1.,6.,100,0.0,10.0)
htemp['h_etapt_gen'] = TH2F('h_etapt_gen','pt vs. eta for generated Lambda',100,1.,6.,100,0.0,10.0)

def processFile(file) :
 print '+++++ Start with file ',file
 appMgr = GaudiPython.AppMgr()
 sel  = appMgr.evtsel()
 sel.open(file)
 evt  = appMgr.evtsvc()
 from LinkerInstances.eventassoc import linkedFrom, linkedTo  
 partsvc = appMgr.ppSvc()

 MCDecayFinder  = appMgr.toolsvc().create('MCDecayFinder', interface='IMCDecayFinder')
 MCDecayFinder.setDecay('[Lambda0 -> p+ pi-]cc')
 MCDebugTool             = appMgr.toolsvc().create('PrintMCDecayTreeTool', interface='IPrintMCDecayTreeTool')
 histos = {}
 for h in htemp.keys() : 
    histos[h] = htemp[h].Clone()

 nevents = 0
 while 1>0: 
  appMgr.run(1)
  if not evt['Rec/Header'] : break
  nevents+=1
  if debug and nevents > 300: break 
# look at MC truth
  mc = evt['MC/Particles']
  if not MCDecayFinder.hasDecay(mc) : continue
  lfrommc = linkedFrom(Track,MCParticle,'Rec/Track/Best')
  decaylist = []
  while MCDecayFinder.findDecay(mc,part)>0 :
   pclone = part.clone()
   GaudiPython.setOwnership(pclone,True)
   decaylist.append(pclone)
  for decay in decaylist :
# only take lambdas from prim interaction
   oVx = decay.originVertex().position()
   if oVx.rho()    > 5. : continue
   if abs(oVx.z()) > 200. : continue
   pid = decay.particleID().pid()
   histos['h_gen'].Fill(1) 
   mc_eta = decay.momentum().eta()
   mc_pt  = decay.pt()
   histos['h_etapt_gen'].Fill(mc_eta,mc_pt/units.GeV)
   daughters = GaudiPython.gbl.std.vector('const LHCb::MCParticle*')()
   MCDecayFinder.descendants(decay,daughters)
   recoparticle = []
   if debug : MCDebugTool.printTree(decay,5)
   for dpart in daughters : 
# check if e, muon, pion, kaon, proton 
    stable = False
    abspid = dpart.particleID().abspid()
    for s in stableParticles : 
      if abspid == s : stable = True
    if debug : 
     oVx = dpart.originVertex().position()
     print 'debug 0.5 ',stable,abspid,oVx.rho(),oVx.z(),dpart.endVertices().size()

    if not stable : continue   
# check if origin is close to beam line and inside Velo
    oVx = dpart.originVertex().position()
    if oVx.rho()    > 50. : continue
    if abs(oVx.z()) > 1000. : continue
# another check don't accept MCParticles with endVertices before T
    if dpart.endVertices().size() > 0 :
     eVx = dpart.endVertices()[0].position()
     if debug : print 'debug 0.75 ',eVx.z() 
     # if eVx.z() < 7000. : continue
# check if reconstructed
    tcand = []   
    if debug : print 'debug 1 ',lfrommc.range(dpart).size()
    for tr in lfrommc.range(dpart): 
# check if long track
     if tr.type() == tr.Long : 
# additional problem, decays in flight: also MC decay products are matched with reco track 
# avoid adding twice the same track   
      exist = False
      trkey = tr.key()
      for t in tcand : 
        if t.key() == trkey : 
          exist = True
          break
      if not exist : 
       tcand.append(tr)     
    if debug : print 'debug 1.5 ',len(tcand)
    if len(tcand) == 0 : continue
# if more than one candidate, choose closest to MC truth     
    if len(tcand) > 1 : 
       pmom = dpart.momentum().r()
       diff = [999999.,-1]
       for tr in tcand: 
        delta = abs(pmom - tr.p())
        if delta < diff[0] : 
          diff[0] = delta 
          diff[1] = tr   
       if diff[1] == -1 : print 'this is impossible, BIG ERROR!'
       tcand[0] = diff[1]
# fake PID:
    pid = dpart.particleID().abspid()
    m   = partsvc.find(ParticleID(pid)).mass()
    p   = tcand[0].momentum() 
    rsq = m*m + p.Mag2()
    E   = mysqrt(rsq)
    lv = LorentzVector(p.x(),p.y(),p.z(),E)
    fourmomenta = [lv,dpart.momentum()]
    recoparticle.append(fourmomenta)  
   if debug : print 'debug 2 ',len(recoparticle)
   if len(recoparticle) < 2 : continue
# ignore B field in Velo close to IP, add up 4 momenta 
   bmom   = recoparticle[0][0] 
   bmommc = recoparticle[0][1] 
   for t in range(1,len(recoparticle)): 
    bmom  += recoparticle[t][0]
    bmommc+= recoparticle[t][1]
# fill histogram with B mass
   histos[name+'_mass'].Fill(bmom.mass()/units.GeV) 
   delmass = abs(bmommc.mass() - decay.momentum().mass())
   if delmass < 50.*units.MeV :  
     if debug : print 'mass ',bmom.mass()
     histos[name+'_eff'].Fill(bmom.mass()/units.GeV) 
     histos['h_etapt'].Fill(mc_eta,mc_pt/units.GeV)
 appMgr.stop()
 return histos

if __name__ == '__main__' :
 histos = {}
 if dataset == 'BsJpsiPhi' : 
  if f_2008: 
   files = []
   file  = '/castor/cern.ch/grid/lhcb/MC/2008/DST/00003783/0000/00003783_00000XXX_5.dst'
   for n in range(1,300) :
    ff = file.replace('XXX','%(X)03d'%{'X':n})
    x  = os.system('nsls '+ff)
    if x == 0 :  files.append(ff)
 if dataset == 'L0strippedDC06' : 
   files = []
   file  = '/castor/cern.ch/grid/lhcb/production/DC06/L0-v1-lumi2/00001959/DST/0000/00001959_0000XXXX_1.dst'
   nmx = 4000
   if debug : nmx = 10
   for n in range(2,nmx) :
    ff = file.replace('XXXX','%(X)04d'%{'X':n})
    x  = os.system('nsls '+ff)
    if x == 0 :  files.append(ff)  #------Use up to 8 processes 
 if not debug:
  from processing import Pool
  n = 8
  pool = Pool(n)
  result = pool.map_async(processFile, files)
  for hlist in result.get(timeout=10000) : 
    for h in hlist :
     if histos.has_key(h) : histos[h].Add(hlist[h])
     else                     : histos[h] = hlist[h]
 else: 
  histos = processFile(files[0])
  
from ROOT import TText   
tc = TCanvas('mass','masses',700,400)
histos[name+'_mass'].Draw()
  
tceff = TCanvas('eff','masses with missing m < 50 MeV',700,400)
h = name+'_eff'
histos[h].Draw()
nentries = histos[h].GetEntries()
if nentries > 25 : histos[h].Fit('gaus')
# generated Lambda 
gen = histos['h_gen'].GetEntries()
eff = 100.*float(nentries)/float(gen) 
txt = 'Reconstructed '+name+' = %3.2E %s'%(eff,'%')
y = 0.5 * histos[h].GetMaximum()
x = 2.6
tx  = TText(x,y,txt)
tx.DrawText(x,y,txt)
