import os,sys

if len(sys.argv) > 1 : 
 fn = os.path.expandvars(sys.argv[1])
 if not fn.find('eoslhcb')<0 and fn.find('root')<0:
    fn = 'root:'+fn
else:
 print 'missing file name, stop'
 sys.exit()    

from Configurables import LHCbApp
appConf=LHCbApp()
import GaudiPython
appMgr = GaudiPython.AppMgr()
sel=appMgr.evtSel()
sel.open(fn)

evt=appMgr.evtsvc()
appMgr.run(1)
hdr =  evt['Rec/Header']
if not hdr: hdr =  evt['MC/Header']
if not hdr: print "No header found with database tag information"
if hdr:
 for d in hdr.condDBTags():
   print '$$ %10s : %s'%(d.first, d.second)

