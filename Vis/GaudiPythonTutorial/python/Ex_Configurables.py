# print all known configurables
from Gaudi.Configuration import *
from GaudiKernel.Proxy.ConfigurableDb import CfgDb

print 'configurable          lib             module            package'

for x in CfgDb() : print '%30s %30s %30s %30s \n'  %(x, CfgDb()[x]['lib'],CfgDb()[x]['module'],CfgDb()[x]['package'])

