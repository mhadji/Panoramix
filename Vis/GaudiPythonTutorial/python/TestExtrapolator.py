# count number of events on tape
from ROOT import *
import os,sys,subprocess
if len(sys.argv) == 1 : 
 print 'give input file(s)'
 sys.exit()
fn =  sys.argv[1].split(',')
from LHCbConfig import *
# configure Gaudi with database tags from first event under Rec/Header
addDBTags(fn[0])

from Configurables import TrackMasterExtrapolator,StateSimpleBetheBlochEnergyCorrectionTool
tconf = TrackMasterExtrapolator('TrackMasterExtrapolator')
tconf.ApplyEnergyLossCorr = False

appConf = ApplicationMgr(OutputLevel = INFO) 
EventSelector().PrintFreq = 10000

import GaudiPython
# load some additional gadgets
import gaudigadgets

h={}
zs = {'T1':8100.,'T2':8750.,'T3':9450.,'M1':12110., 'M2':15270.0, 'M3':16470., 'M4':17670.0, 'M5':18870.}
for pos in zs:
 s = str(zs[pos]) 
 h[s+'xy']    = TH2F(s+'xy','xy at T '+s,1000,-5.,5,1000,-5.,5.)
 h[s+'xyP']   = TH2F(s+'xyP','xy at T parity '+s,1000,-5.,5,1000,-5.,5.)
 h[s+'delx']  = TH1F(s+'delx','diff extrap with true state'+s,100,-50.,50.)

appMgr = GaudiPython.AppMgr()
det    = appMgr.detsvc()
stupid = det['/dd/Structure/LHCb']
sel    = appMgr.evtsel()
evt    = appMgr.evtsvc()
extrap = appMgr.toolsvc().create('TrackMasterExtrapolator', interface='ITrackExtrapolator')

sel.open(fn) 

while 1>0:
 appMgr.run(1)
 if not evt['Rec/Header']: break
 for atrack in evt['Rec/Track/Best']:
   if atrack.type() != atrack.Long : continue
   if atrack.pt() < 500.  : continue
   if atrack.p()  < 3000. : continue
   aproto = evt['Rec/ProtoP/Charged'].containedObject(atrack.key())
   ismuon = False
   if aproto:   ismuon = aproto.muonPID().IsMuon() 
   fs  = atrack.firstState()
   ns  = fs.clone()
   nsP = fs.clone()
   nsP.setQOverP(-fs.qOverP())
   for x in zs:
    if not ismuon and not x.find('M')<0: continue
    z = zs[x]
    result = extrap.propagate(nsP,z)  
    result = extrap.propagate(ns,z)  
    s = str(z) 
    h[s+'xy'].Fill(ns.x()/units.m,ns.y()/units.m)
    h[s+'xyP'].Fill(nsP.x()/units.m,nsP.y()/units.m)
    for st in atrack.states():
      if st.z()>z-1. and st.z()<z+1: h[s+'delx'].Fill( (ns.x()-st.x()) /units.mm)

fout = TFile('TandMStationLimits.root','recreate')
for ahist in h:
   rc = h[ahist].Write()

# results:
TLimits = {8100.:{'xmin':100.,'ymin':100.,'xmax':2400.,'ymax':2100.},
               8750.:{'xmin':100.,'ymin':100.,'xmax':2700.,'ymax':2300.},
               9450.:{'xmin':100.,'ymin':100.,'xmax':3100.,'ymax':2400.}} 



