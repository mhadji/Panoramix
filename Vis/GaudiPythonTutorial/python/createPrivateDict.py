from os import system, environ
from os.path import exists, splitext, getmtime

def create_file(name='file.txt', contents='') :
  if not exists(name) or open(name).read() != contents :
    file = open(name,'w')
    file.write(contents)
    file.close()

def execute_cmd(cmd) :
  print cmd
  system(cmd)

def build_dictionary(file, selection='', options='') :
  sdict = splitext(file)[0]+'_dict.cxx'
  bdict = splitext(file)[0]+'.so'
  if not exists(sdict) or getmtime(file) > getmtime(sdict) or selection and getmtime(selection) > getmtime(sdict) :
     rootsys = environ['ROOTSYS']
     if selection : execute_cmd('genreflex %s -o %s -s %s %s' %(file, sdict, selection, options))
     else         : execute_cmd('genreflex %s -o %s  %s' %(file, sdict, options))
  if not exists(bdict) or getmtime(sdict) > getmtime(bdict) :
     execute_cmd('c++ %s -fPIC -shared -o %s -I%s/include -L%s/lib -lReflex %s' % (sdict, bdict, rootsys, rootsys, options))

def load_dictionary(file, selection='', options='') :
  build_dictionary(file, selection, options)
  import PyCintex
  PyCintex.loadDictionary(splitext(file)[0])

import os
_proc_status = '/proc/%d/status' % os.getpid()
_scale = {'kB': 1024.0, 'mB': 1024.0*1024.0,
          'KB': 1024.0, 'MB': 1024.0*1024.0}
def _VmB(VmKey):
  '''Private.
    '''
  global _proc_status, _scale
  # get pseudo file  /proc/<pid>/status
  try:
    t = open(_proc_status)
    v = t.read()
    t.close()
  except:
    return 0.0  # non-Linux?
  # get VmKey line e.g. 'VmRSS:  9999  kB\n ...'
  i = v.index(VmKey)
  v = v[i:].split(None, 3)  # whitespace
  if len(v) < 3:
    return 0.0  # invalid format?
  # convert Vm value to bytes
  return float(v[1]) * _scale[v[2]]

def memory(since=0.0):
  '''Return memory usage in bytes.
  '''
  return _VmB('VmSize:') - since

def resident(since=0.0):
  '''Return resident memory usage in bytes.
  '''
  return _VmB('VmRSS:') - since

def stacksize(since=0.0):
  '''Return stack size in bytes.
  '''
  return _VmB('VmStk:') - since


#------------This part is to generate the dictionary-----------------
#            it should be placed in a proper dictionary--------------

create_file('enclose.h',  """
#include <vector>
#include <string>
#include "GaudiKernel/DataObject.h"

using namespace std;
class MyClass {
  public:
    int i;
    double d;
    string s;
};

template <class T> class Enclose: public DataObject, public T  {
  public:
    Enclose() :  DataObject(), T() {}
    Enclose(const T& p) :  DataObject(), T(p) {}
    virtual ~Enclose() {}
  /// Retrieve reference to class definition structure
  virtual const CLID& clID() const  { return Enclose<T>::classID();  }
  /// Retrieve reference to class definition structure (static access)
  static const CLID& classID();
};
template class Enclose<vector<double> >;
template class Enclose<vector<std::pair<std::string,std::string> > >;
template class Enclose<MyClass>;
template class Enclose<std::vector<MyClass> >;
template class Enclose<std::vector<MyClass*> >;

template <> const CLID& Enclose<std::vector<double> >::classID() {
	static CLID cl=0x3eb;
      return cl;
}
template <> const CLID& Enclose<std::vector<std::pair<std::string,std::string> > >::classID() {
	static CLID cl=0x3ed;
      return cl;
}
template <> const CLID& Enclose<MyClass>::classID()  {
	static CLID cl=0x3ec;
      return cl;
}
template <> const CLID& Enclose<std::vector<MyClass> >::classID()  {
	static CLID cl=0x3ef;
      return cl;
}
template <> const CLID& Enclose<std::vector<MyClass*> >::classID()  {
	static CLID cl=0x3e3;
      return cl;
}
""")



create_file('enclose.xml', """
  <lcgdict>
    <class name="MyClass" id="000003EA-0000-0000-0000-000000000000"/>
    <class name="Enclose<std::vector<double> >" id="000003EB-0000-0000-0000-000000000000"/>
    <class name="Enclose<std::vector<MyClass> >" id="000003EF-0000-0000-0000-000000000000"/>
    <class name="Enclose<std::vector<MyClass*> >" id="000003E3-0000-0000-0000-000000000000"/>
    <class name="Enclose<std::vector< std::pair<std::string,std::string> > >" id="000003ED-0000-0000-0000-000000000000"/>
    <class name="Enclose<MyClass>" id="000003EC-0000-0000-0000-000000000000"/>
  </lcgdict>
""")

build_dictionary('enclose.h', selection='enclose.xml',
                              options='-I${GAUDIKERNELROOT} -I/home/truf/LHCb_software/lcg/external/Boost/1.44.0_python2.6/x86_64-slc5-gcc43-opt/include/boost-1_44/')
#-------------------End of Building Dictionary----------------------
