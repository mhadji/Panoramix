from processing import Process, Queue, Pool

from random import *
from ROOT import *

#------The function to run in each 'process'
def g(n) :
  print 'doing the next iteration to calc something [%i]'% n
  i = 0
  while i<10000000:
    r=random()
    s=TMath.ASin(r)
    i=i+1
  print 'woke up again '
  return s

#------Use up to 4 processes
p = Pool(4)

workitems = range(20)
result = p.map_async(g, workitems)

print result.get(timeout=1000)

