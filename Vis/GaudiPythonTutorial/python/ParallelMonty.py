runGauss  = True
runBoole  = False
runBrunel = False

import os,time,webbrowser
from processing import Process, Queue, Pool

#------The function to run in each 'process'
def gauss(cycle) :
 f = open(outputName+'_'+str(cycle)+'.log', 'w')
 x = os.popen4('python myGauss.py ' + str(cycle))
 f.writelines(x[1].readlines())
 f.close()
 return (cycle)

# environment:
# SetupProject Boole 
# SetupProject Brunel --runtime-project Boole --runtime-project Gauss --ignore-missing --nightly lhcb1 Mon

runnr      = 52
outputName =  "gauss"+'_'+str(runnr)

# variables for the subprocesses
os.environ['myProdRunnr'] = str(runnr)
os.environ['myProdFile']  = outputName
os.environ['myProdSize']  = str(50)

ncycles = 4

start = time.time()

f=open(outputName+'.log', 'w')

if runGauss: 
 print '####  Start with Gauss Step '
 # gauss
 #------Use up to 8 processes
 p = Pool(ncyles)

 workitems = range(ncycles)
 result = p.map_async(gauss, workitems)
 for c in result.get(timeout=10000) : 
  print 'cycle finished', c 

if runBoole:
 print '####  Start with Boole Step '
 # boole
 x = os.popen4('python myBoole.py '+str(ncycles))
 f.writelines(x[1].readlines())

if runBrunel:
 print '####  Start with Brunel Step '
 # brunel
 x = os.popen4('python myBrunel.py')
 f.writelines(x[1].readlines())

f.close()

end = time.time()
print 'job lasted for ',end-start
webbrowser.open(os.getcwd()+'/'+outputName+'.log')
