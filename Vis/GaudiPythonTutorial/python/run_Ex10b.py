import os
from ROOT import TFile
slots = ['DAQ/RawEvent'] 
for i in range(1,7): 
  slots.append('Next'+str(i)+'/DAQ/RawEvent')
  slots.append('Prev'+str(i)+'/DAQ/RawEvent')

r = open('Ex10b_results.txt','w')
r.close()
  
for s in slots :
 print 'executing ',s
 ex  = open('Ex10b.py')
 tmp = open('tmp.py','w')
 lines = ex.readlines()
 for line in lines :
  if line.find('DAQ/RawEvent') > -1:
   line = line.replace('DAQ/RawEvent',s)
  tmp.write(line)
 tmp.close()
 ex.close()
# 
 os.system('python tmp.py') 
 os.remove('tmp.py')
 
from ROOT import gROOT 
for s in slots :
 f = TFile(s.replace('/','')+'.root')
 h_ot    = gROOT.FindObject('h_ot')
 h_track = gROOT.FindObject('h_track')
 print s, h_ot.GetTitle(),h_ot.GetMean()
 print s, h_track.GetTitle(),h_track.GetEntries()-h_track.GetBinContent(1)
 f.Close()
 

