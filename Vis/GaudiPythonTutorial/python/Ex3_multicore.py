# setenv PYTHONPATH /afs/cern.ch/sw/lcg/external/processing/0.51/${CMTCONFIG}/lib/python2.5/site-packages:$PYTHONPATH
import time
start = time.time()
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "DC06"

importOptions('$PANORAMIXROOT/options/PanoramixCleanUp.py')
importOptions('$PANORAMIXROOT/options/PanoramixFullRec.py')

appConf = ApplicationMgr( OutputLevel = 3, AppName = 'Ex3_multicore')

EventSelector().PrintFreq  = 100

#--- Inport needed modules to be reused in all processes---------------
import GaudiPython
from processing import Pool
from ROOT import TH1F,TFile,gROOT 

def processFile(file) :
  h_chi2 = TH1F('h_chi2_'+str(file.__hash__()),'fit chi2',100,0.,10000.)
  appMgr = GaudiPython.AppMgr()
  sel = appMgr.evtsel()
  sel.open(file)
  evt = appMgr.evtsvc()
  while 0 < 1:
   appMgr.run(1)
# check if there are still valid events
   if not evt['Rec/Header'] : break
   cont = evt['Rec/Track/Best']
   if cont  :
     for t in cont : 
      success = h_chi2.Fill(t.chi2())
  print 'Finishing.... ', file,' ',h_chi2.GetMean() ,h_chi2.GetEntries() 
  return h_chi2

#--- In the master process only....

if __name__ == '__main__' :
  files = [
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000001_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000002_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000003_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000004_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000005_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000007_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000008_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000009_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000010_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000012_5.dst',
'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000013_5.dst'
 ]
      
  #------Use up to 8 processes
  n = 8
  pool = Pool(n)

  result = pool.map_async(processFile, files[:n])
  
  rootfile = 'final.root'
  h_chi2 = None
  for h in result.get(timeout=10000) : 
   if not h_chi2 : h_chi2 = h
   else              : h_chi2.Add(h)

  h_chi2.Draw()
  end = time.time()
  print 'total time', end - start
  print h_chi2.GetEntries()

















