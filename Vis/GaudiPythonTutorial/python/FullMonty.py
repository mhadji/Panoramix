import os,time,webbrowser
# for backward compatibility with units in opts files
import os.path, GaudiKernel.ProcessJobOptions
GaudiKernel.ProcessJobOptions._parser._parse_units(os.path.expandvars("$STDOPTS/units.opts"))

# environment:
# SetupProject Boole 
# SetupProject Brunel --runtime-project Boole --runtime-project Gauss --ignore-missing --nightly lhcb1 Mon

import logging
from GaudiKernel.ProcessJobOptions import InstallRootLoggingHandler
#level = logging.INFO
level = logging.DEBUG
InstallRootLoggingHandler("# ", level = level)
root_logger = logging.getLogger()

runnr      = 2011
outputName =  "gauss"+'_'+str(runnr)

# variables for the subprocesses
os.environ['myProdRunnr'] = str(runnr)
os.environ['myProdFile']  = outputName
os.environ['myProdSize']  = str(50)

start = time.time()

f=open(outputName+'.log', 'w')

print '####  Start with Gauss Step '
# gauss
x = os.popen4('python myGauss.py')
f.writelines(x[1].readlines())

print '####  Start with Boole Step '
# boole
x = os.popen4('python myBoole.py')
f.writelines(x[1].readlines())

print '####  Start with Brunel Step '
# brunel
x = os.popen4('python myBrunel.py')
f.writelines(x[1].readlines())
f.close()

end = time.time()
print 'job lasted for ',end-start
webbrowser.open(os.getcwd()+'/'+outputName+'.log')
