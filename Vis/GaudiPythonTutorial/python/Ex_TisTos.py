#example from https://twiki.cern.ch/twiki/bin/view/LHCb/TriggerTisTosInPython

# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType   = "2010"

appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'ExTisTos')

# container name
candidates = 'Dimuon/Phys/BetaSBu2JpsiKPrescaledLine/Particles'

import GaudiPython
appMgr = GaudiPython.AppMgr()
appMgr.initialize() 
sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/Bu2JpsiK_00011917_00000048_1.dst'])
evt = appMgr.evtsvc()

#get TISTOS tool
tsvc = appMgr.toolsvc()
# get TisTos tool
tistostool= tsvc.create('TriggerTisTos',interface='ITriggerTisTos')
l0tistostool = tsvc.create('L0TriggerTisTos',interface='ITriggerTisTos')
#    used for printout
daughterTool=tsvc.create('ParticleDescendants',interface='IParticleDescendants')

def dvs(v): # dump vector content
    for iv in v:
        print iv,
    print

def dvsl0(v): # special version for Hlt1L0 trigger names to strip Hlt1 off
    for iv in v:
        print iv[iv.find('L0'):],
    print


# process 1 event
appMgr.run(1)

# methods which don't require signal definition -------------------------
# reset the tool
tistostool.setOfflineInput()

# explore trigger names
print ' Hlt1L0 Trigger Names '
l0triggers =  tistostool.triggerSelectionNames('Hlt1L0.*Decision') 
dvs( l0triggers )
print ' Hlt1 Trigger Names '
hlt1triggers =  tistostool.triggerSelectionNames('Hlt1.*Decision')
dvs( hlt1triggers )
print ' Hlt2 Trigger Names '
hlt2triggers = tistostool.triggerSelectionNames('Hlt2.*Decision')
dvs(hlt2triggers)

# validate specific trigger name
def validate(triggerName):
    if tistostool.triggerSelectionNames(triggerName).size()>0:
        print ' Trigger name ',triggerName,' is valid '
    else:
        print ' Trigger name ',triggerName,' is invalid '

validate('Hlt1L0ElectronDecision')
validate('Hlt2.*')
validate('Hlt.*')

def tistossignal(signal):
    # thrustful source of L0 decisions
    l0du = evt['Trig/L0/L0DUReport']
    print ' L0DUReport decision=',l0du.decision(),
    l0chan = l0du.configuration().channels()
    for l0c in l0chan:
        print ' ',l0c.first,' ',l0du.channelDecision(l0c.second.id()),
    print ' '    
    l0tistostool.setOfflineInput(signal)
    # tistos L0 trigger  
    l0 = l0tistostool.tisTosTobTrigger() 
    print ' L0 decision=',1 if l0tistostool.hltObjectSummaries().size() else 0,' tis=',l0.tis(),' tos=',l0.tos(),' L0DUdecision=',l0du.decision()
    print ' L0 TOS triggers'
    dvsl0(  l0tistostool.triggerSelectionNames(tistostool.kAnything,tistostool.kAnything,tistostool.kTrueRequired) )
    print ' L0 TIS triggers'
    dvsl0(  l0tistostool.triggerSelectionNames(tistostool.kAnything,tistostool.kTrueRequired,tistostool.kAnything) )

    # tistos Hlt1L0 (these are real Hlt1 triggers which pass through L0 accept events - heavily postscaled, don't expect them to have decision=true often)
    hlt1l0 = tistostool.triggerTisTos('Hlt1L0.*Decision')
    print ' Hlt1L0 decision=',hlt1l0.decision(),' tis=',hlt1l0.tis(),' tos=',hlt1l0.tos()
    print ' Hlt1L0 TOS triggers'
    dvs( tistostool.triggerSelectionNames(tistostool.kTrueRequired,tistostool.kAnything,tistostool.kTrueRequired) )
    print ' Hlt1L0 TIS triggers'
    dvs( tistostool.triggerSelectionNames(tistostool.kTrueRequired,tistostool.kTrueRequired,tistostool.kAnything) )
    
    # tistos Hlt1
    hlt1g = tistostool.triggerTisTos('Hlt1Global')
    print ' Hlt1Global decision=',hlt1g.decision(),' tis=',hlt1g.tis(),' tos=',hlt1g.tos()
    hlt1 = tistostool.triggerTisTos('Hlt1.*Decision')
    print ' Hlt1 decision=',hlt1.decision(),' tis=',hlt1.tis(),' tos=',hlt1.tos()
    print ' Hlt1 TOS triggers'
    dvs( tistostool.triggerSelectionNames(tistostool.kTrueRequired,tistostool.kAnything,tistostool.kTrueRequired) )
    print ' Hlt1 TIS triggers'
    dvs( tistostool.triggerSelectionNames(tistostool.kTrueRequired,tistostool.kTrueRequired,tistostool.kAnything) )
    # tistos Hlt2
    hlt2g = tistostool.triggerTisTos('Hlt2Global')
    print ' Hlt2Global decision=',hlt2g.decision(),' tis=',hlt2g.tis(),' tos=',hlt2g.tos()
    hlt2 = tistostool.triggerTisTos('Hlt2.*Decision')
    print ' Hlt2 decision=',hlt2.decision(),' tis=',hlt2.tis(),' tos=',hlt2.tos()
    print ' Hlt2 TOS triggers'
    dvs( tistostool.triggerSelectionNames(tistostool.kTrueRequired,tistostool.kAnything,tistostool.kTrueRequired) )
    print ' Hlt2 TIS triggers'
    dvs( tistostool.triggerSelectionNames(tistostool.kTrueRequired,tistostool.kTrueRequired,tistostool.kAnything) )
    #
    # tistos first Hlt1 trigger    
    #
    specificHlt1Trigger = 'Hlt1TrackAllL0Decision'
    results = tistostool.triggerTisTos(specificHlt1Trigger)
    print ' Trigger name ',specificHlt1Trigger,' decision=',results.decision(),' tis=',results.tis(),' tos=',results.tos()
    # all saved selections
    selReports = evt['/Event/Hlt/SelReports']    
    if selReports:
        sids=selReports.selectionNames()
        for selname in sids:
            results = tistostool.selectionTisTos(selname)
            print ' decision=' , results.decision() , ' sel-decision=', 1 if tistostool.hltSelectionObjectSummaries(selname).size() else 0,
            print ' TOS=' , results.tos() , ' TIS=' , results.tis(),
            print ' #-of-TOS obj ' , tistostool.hltSelectionObjectSummaries(selname,tistostool.kFalseRequired,tistostool.kTrueRequired).size(),
            print ' TOB ' , tistostool.hltSelectionObjectSummaries(selname,tistostool.kFalseRequired,tistostool.kFalseRequired).size(),
            print ' TIS ' , tistostool.hltSelectionObjectSummaries(selname,tistostool.kTrueRequired,tistostool.kFalseRequired).size(),
           # print ' #-of-matched TOS tracks ' , tistostool.matchedTOSTracks(selname).size(),
           # print ' vertices ' , tistostool.matchedTOSVertices(selname).size(),
           # print ' particles ' , tistostool.matchedTOSParticles(selname).size(),                  
            print ' Trigger Selection=' , selname        


# loop over 10 events
for i in range(10):
    # process a few events
     appMgr.run(1)
     signalcontainer = evt[candidates]
     if signalcontainer != None : 
      if signalcontainer.size() > 0 :
       print signalcontainer.size(), ' --------------- New Event ------------------------------------------------------------------ '
       for signal in signalcontainer :
        print ' Candidate found in <><><><><><><><><><><><><><><><><><><><><><><><><><><> '
        finals = daughterTool.finalStates(signal)
        print '    Signal Particle ' , signal.particleID().pid() , ' Pt ' , signal.pt() \
              , ' #-of-daughters ' , signal.daughtersVector().size() \
              , ' #-of-finals ' , finals.size()
        for pf in finals:
            print ' Final state particle ',pf.particleID().pid(),' Pt ',pf.pt()
            tistossignal(signal)
            print ' TisTos Results for first final-state daughter set as signal ++++++++++++++++++++++++++++++++++ '
            tistossignal(finals[0])

