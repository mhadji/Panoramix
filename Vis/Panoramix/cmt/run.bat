@echo off

rem # 
rem #  In order that this script run properly
rem # the python and cmt commands must be available
rem # from the DOS prompt.
rem # 
rem # Usage :
rem #   DOS> cd <path>\Panoramix\<version>\cmt
rem #   DOS> call run.bat
rem # 

python ..\scripts\Python\pmx_run.py Panoramix_main.exe ..\options\Panoramix.opts

@echo on
