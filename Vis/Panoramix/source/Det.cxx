//
//  All functions here should be OnX callbacks, that is to say
// functions with signature :
//   extern "C" {
//     void callback_without_arguments(IUI&);
//     void callback_with_arguments(IUI&,const std::vector<std::string>&);
//   }
//


// Panoramix :
#include "Helpers.h"

// Lib :
#include <Lib/smanip.h>

// OnX :
#include <OnX/Interfaces/IUI.h>
#include <OnX/Helpers/Inventor.h>

// Inventor :
#include <Inventor/nodes/SoOrthographicCamera.h>

// HEPVis :
#include <HEPVis/nodekits/SoPage.h>
#include <HEPVis/nodekits/SoRegion.h>
#include <HEPVis/nodekits/SoDisplayRegion.h>

// Panoramix :
#include "Style.h"

// OnX Callbacks :

extern "C" {

//////////////////////////////////////////////////////////////////////////////
void gb_DetView(
 IUI& aUI
) 
/////////////////////////////////////////////////////////////////////;/////////
// Compiled version of the python examples/Python/gb_DetView_o.py
// Used to test python wrapping if needed.
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  ISession& session = aUI.session();
  IUserInterfaceSvc* uiSvc = find_uiSvc(aUI.session());
  if(!uiSvc) return;

  // Page and region layout :
  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  soPage->deleteRegions();
  soPage->titleVisible.setValue(TRUE);
  soPage->title.setValue("My LHCb");
  soPage->createRegions("SoDisplayRegion",1,1,0);

  SoRegion* soRegion = soPage->currentRegion();
  if(!soRegion) return;
  soRegion->color.setValue(SbColor(1,1,1));

  Panoramix::Style style(session);
  style.setSolid();

  // Camera positionning :
  // Side +z at left :
  SoCamera* soCamera = soRegion->getCamera();
  if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
    SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
    camera->height.setValue(16000);
    camera->position.setValue(1660,7250,9570);
    camera->orientation.setValue
      (SbRotation(SbVec3f(-0.513F,0.688F,0.513F),1.936F));
    camera->nearDistance.setValue(1);
    camera->farDistance.setValue(100000);
  }

  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Velo");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Velo2Rich1");

  style.setOpened();
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PDPanel0");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PDPanel1");
  style.setClosed();

  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q0A:1");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q0B:0");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q1A:5");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q1B:4");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q2A:6");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q2B:7");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q3A:2");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q3B:3");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q00:0");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q01:1");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q02:2");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q03:3");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q110:10");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q111:11");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q18:8");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q19:9");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q212:12");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q213:13");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q214:14");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q215:15");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q34:4");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q35:5");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q36:6");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q37:7");

  uiSvc->visualize("/dd/Structure/LHCb/MagnetRegion/Magnet");
  //  visualize_magneticField(aUI)

  style.setWireFrame();
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/TT");
  style.setSolid();
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTa");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTb");

  uiSvc->visualize("/dd/Structure/LHCb/AfterMagnetRegion/T/IT");

  std::string s;

  int i,j;
  for(i=1;i<4;i++) {
    for(j=0;j<2;j++) {
      style.setColor("lightgrey");
      Lib::smanip::printf(s,256,
        "/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T%d/Ulayer/Quarter%d",i,j);
      uiSvc->visualize(s);
  
      style.setColor("grey");
      Lib::smanip::printf(s,256,
        "/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T%d/Vlayer/Quarter%d",i,j);
      uiSvc->visualize(s);
  
      style.setColor("dimgrey");
      Lib::smanip::printf(s,256,
        "/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T%d/X1layer/Quarter%d",i,j);
      uiSvc->visualize(s);
      Lib::smanip::printf(s,256,
        "/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T%d/X2layer/Quarter%d",i,j);
      uiSvc->visualize(s);
    }
  }  
  style.setColor("indianred");
  uiSvc->visualize("/dd/Structure/LHCb/AfterMagnetRegion/Rich2/PDPanel0");
  uiSvc->visualize("/dd/Structure/LHCb/AfterMagnetRegion/Rich2/PDPanel1");

  style.setClosed();

  for(i=0;i<39;i++) {
    Lib::smanip::printf(s,256,
      "/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2SecMirror:%d",i);
    uiSvc->visualize(s);
  }
  for(i=0;i<55;i++) {
    Lib::smanip::printf(s,256,
      "/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2SphMirror:%d",i);
    uiSvc->visualize(s);
  }
  style.setColor("cyan");
  style.dontUseVisSvc();

  style.setTransparency(0.1);

  style.setColor("lightblue");
  uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Prs");
  style.setColor("orange");
  uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Spd");
  style.setColor("red");
  uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Ecal");
  style.setColor("violet");
  uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Hcal");

  style.setSolid();
  style.setColor("forestgreen");
  for(i=1;i<6;i++) {
    for(j=1;j<5;j++) {
      Lib::smanip::printf(s,256,
        "/dd/Structure/LHCb/DownstreamRegion/Muon/M%d/R%d",i,j);
      uiSvc->visualize(s);
    }
  }
  style.useVisSvc();
  style.setSolid();
  style.setTransparency(0);

}

}
