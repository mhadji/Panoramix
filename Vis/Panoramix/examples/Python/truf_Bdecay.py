from panoramixmodule import *
def createPage(title): 
 Page().titleVisible(True)
 Page().setTitle(title)
 Page().createDisplayRegions(1,1,0)
 
def execute(check4page=True):
 title = 'B Decay MC particles'
 curtitle = ui().currentPage().title.getValues()[0]
 if not (title == curtitle) and check4page :     
  createPage(title)
  onlineViews[title] = 'truf_Bdecay.execute()'

 Style().showText()
 Style().setTextSize(10)
 Page().setTitle('Selected MC Particles')
 Page().setCurrentRegion(0)
 Region().setBackgroundColor('black')

 Camera().setPosition(0.,0.,0.)
 Camera().setHeight(2.5)
 Camera().setOrientation(0.,1.,0.,3.14)
 Camera().setNearFar(-50000.,23000.)
 
 # pions and kaons from B :
 Style().setColor('red')
 data_collect(da(),'MCParticle','(particle==\'pi+\'||particle==\'pi-\'||particle==\'K-\'||particle==\'K+\')&&(bcflag>9)')
 session().setParameter('modeling.posText','4')
 data_visualize(da())
  # all heavy particles
 Style().setColor('grey')
 Style().setLineWidth(3) 
 
 data_collect(da(),'MCParticle','(mass>5000)')
 session().setParameter('modeling.posText','medium')
 data_visualize(da())
 
 # B :
 Style().setColor('white')
 data_collect(da(),'MCParticle','particle==\'B_s~0\'||particle==\'B_s0\'||particle==\'B0\'||particle==\'B+\'||particle==\'B-\'||particle==\'B~0\'')
 session().setParameter('modeling.posText','medium')
 data_visualize(da())
 
 Style().setLineWidth(2) 
   # all medium heavy particles
 Style().setColor('cyan')
 data_collect(da(),'MCParticle','(mass>1800)&&(mass<2100)')
 session().setParameter('modeling.posText','medium')
 data_visualize(da())
 
 Style().setLineWidth(0) 
 
 # psi :
 Style().setColor('magenta')
 data_collect(da(),'MCParticle','particle==\'J/psi(1S)\'')
 data_visualize(da())
 
 # KS0 from B :
 Style().setColor('yellow')
 data_collect(da(),'MCParticle','particle==\'KS0\'&&(parent==\'B0\'||parent==\'B~0\'||parent==\'B_s0\'||parent==\'B_s~0\')')
 data_visualize(da())
 
 # D_s+, D_s-, D0 from B :
 Style().setColor('yellow')
 data_collect(da(),'MCParticle','(particle==\'D_s+\'||particle==\'D_s-\'||particle==\'D0\'||particle==\'D~0\')&&(bcflag>9)')
 session().setParameter('modeling.posText','medium')
 data_visualize(da())
 
 # D0 from D* from B:
 Style().setColor('skyblue')
 data_collect(da(),'MCParticle','(particle==\'D0\'||particle==\'D~0\')&&(bcflag>9)')
 session().setParameter('modeling.posText','medium')
 data_visualize(da())
 
 # K+, K-, pi+,pi- from D*, D0 :
 Style().setColor('lightblue')
 data_collect(da(),'MCParticle','(particle==\'K+\'||particle==\'K-\'||particle==\'pi+\'||particle==\'pi-\')&&(bcflag!=0&&bcflag!=10&&bcflag!=20)')
 session().setParameter('modeling.posText','3')
 data_visualize(da())
 
 # e from psi :
 Style().setColor('blue')
 data_collect(da(),'MCParticle','(particle==\'e+\'||particle==\'e-\')&&(parent==\'J/psi(1S)\')&&(bcflag>9)')
 session().setParameter('modeling.posText','3')
 data_visualize(da())
 
 # mu from psi :
 Style().setColor('blue')
 data_collect(da(),'MCParticle','(particle==\'mu+\'||particle==\'mu-\')&&(parent==\'J/psi(1S)\')&&(bcflag>9)')
 session().setParameter('modeling.posText','3')
 data_visualize(da())
 
 # pi+, pi- from KS0 :
 Style().setColor('violet')
 data_collect(da(),'MCParticle','(particle==\'pi+\'||particle==\'pi-\')&&(parent==\'KS0\') && (bcflag>9)')
 session().setParameter('modeling.posText','3')
 data_visualize(da())
 
   # all K and pi with large pt  :
 Style().setColor('plum')
 data_collect(da(),'MCParticle','(particle==\'K+\'||particle==\'K-\'||particle==\'pi-\'||particle==\'pi+\')&&(pt>2000)')
 session().setParameter('modeling.posText','3')
 data_visualize(da())
 
 Style().hideText()

