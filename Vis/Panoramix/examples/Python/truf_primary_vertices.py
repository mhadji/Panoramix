from panoramixmodule import *

# primary vertex : 
if not ui().findWidget('Viewer_PV') :     
 ui().createComponent('Viewer_PV','PageViewer','ViewerTabStack')
 ui().setCallback('Viewer_PV','collect','DLD','OnX viewer_collect @this@')
 ui().setCallback('Viewer_PV','popup','DLD','Panoramix Panoramix_viewer_popup')
 ui().setParameter('Viewer_PV.popupItems','Current region\nNo highlighted\nTrack_MCParticle\nMCParticle_Track\nParticle_MCParticle\nParticle_Daughters\nMCParticle_Clusters\nCluster_MCParticles')

 Viewer().setFrame()
 Viewer().removeAutoClipping()
   
 ui().setCurrentWidget(ui().findWidget('Viewer_PV'))
 Page().setTitle('Primary Vertices')
 Page().titleVisible(True)
 
ui().setCurrentWidget(ui().findWidget('Viewer_PV'))
page   = OnX.session().ui().currentPage()
if not page.getNumberOfRegions() == 5 :      
 page.deleteRegions()
 page.createRegions(1,2,0)

 # Setup region (a page can have multiple drawing region) :
 Page().setCurrentRegion(0)
 Viewer().setFrame()
 Style().setColor('black')
 session().setParameter('modeling.modeling','wireFrame')
 Camera().setPosition(0., 0., 0.)
 Camera().setHeight(1.0)
 Camera().setOrientation(0., 0., 1., 0.)
 Camera().setNearFar(-150.,50.)
 ui().executeScript('DLD','OnX region_addFrame 0.1 grey')

 Page().setCurrentRegion(1)
 Viewer().setFrame()
 Style().setColor('black')
 Camera().setPosition(0., 50., -10.)
 Camera().setHeight(163)
 Camera().setOrientation(-0.57735026, -0.57735026, -0.57735026,  2.0943)
 Camera().setNearFar(0.,100.)
 session().setParameter('modeling.modeling','wireFrame')
 Region().setTransformScale(10.,10.,1.)
 ui().executeScript('DLD','OnX region_addFrame 10 grey')
 
Page().setCurrentRegion(0)
Style().setColor('steelblue')
uiSvc().visualize('/Event/Rec/Vertex/Primary')  
#
Style().setColor('green')
uiSvc().visualize('/Event/Hlt/VertexReports/PV3D')

if evt['MC/Particles']:
 Style().setColor('yellow')
 data_collect(da(),'MCParticle','parent==\'nil\'&&charge==0')
 data_visualize(da())

Page().setCurrentRegion(1)
Style().setColor('steelblue')
uiSvc().visualize('/Event/Rec/Vertex/Primary')  
#
Style().setColor('green')
uiSvc().visualize('/Event/Hlt/VertexReports/PV3D')

if evt['MC/Particles']:
 Style().setColor('yellow')
 data_collect(da(),'MCParticle','parent==\'nil\'&&charge==0')
 data_visualize(da())
 
Style().setColor('cyan')
session().setParameter('modeling.what','no')
session().setParameter('modeling.lineWidth','1') 
 
 
del page
