
from Panoramix import *

##################################################################
# Draw a nice 3D picture of the IT and OT including beam-pipe    #
##################################################################

# Clear the page
page = ui.currentPage()
region = page.currentRegion()
region.clear()

# Set the camera and background
region_setCamera('height 6960','position -1915 2295 4572','orientation -0.08 -0.96 -0.25 2.5')
region.setParameter('color','white')

# Use different colors than the standard colors from color.xml
session.setParameter('modeling.useVisSvc','false')

# Draw TT
session.setParameter('modeling.color','1 0 1')
uiSvc.visualize('/dd/Structure/LHCb/IT/ITS1/LayerX')
session.setParameter('modeling.color','0.25 0 0.25')
uiSvc.visualize('/dd/Structure/LHCb/IT/ITS1/LayerU')
session.setParameter('modeling.color','0.5 0 0.5')
uiSvc.visualize('/dd/Structure/LHCb/IT/ITS2/LayerV')
session.setParameter('modeling.color','0.25 0 0.25')
uiSvc.visualize('/dd/Structure/LHCb/IT/ITS2/LayerX')

# Draw IT1 to IT3
for i in range(3,6) :
 s = '/dd/Structure/LHCb/IT/ITS%01d' % i
 session.setParameter('modeling.color','0.5 0 0.5')
 uiSvc.visualize(s+'/BoxT/X1')
 uiSvc.visualize(s+'/BoxT/U')
 uiSvc.visualize(s+'/BoxT/V')
 uiSvc.visualize(s+'/BoxT/X2')
 session.setParameter('modeling.color','0.5 0 0.5')
 uiSvc.visualize(s+'/BoxB/X1')
 uiSvc.visualize(s+'/BoxB/U')
 uiSvc.visualize(s+'/BoxB/V')
 uiSvc.visualize(s+'/BoxB/X2')
 session.setParameter('modeling.color','1 0 1')
 uiSvc.visualize(s+'/BoxL/X1')
 uiSvc.visualize(s+'/BoxL/U')
 uiSvc.visualize(s+'/BoxL/V')
 uiSvc.visualize(s+'/BoxL/X2')
 session.setParameter('modeling.color','1 0 1')
 uiSvc.visualize(s+'/BoxR/X1')
 uiSvc.visualize(s+'/BoxR/U')
 uiSvc.visualize(s+'/BoxR/V')
 uiSvc.visualize(s+'/BoxR/X2')

# Draw OT1 to OT3
session.setParameter('modeling.color','cyan')
for i in range(1,4) :
 t = '/dd/Structure/LHCb/OT/T%01d' % i
 for j in range(0,4) :
  s1 = t + '/X1layer/Quarter%01d' % j
  s2 = t + '/Ulayer/Quarter%01d' % j
  s3 = t + '/Vlayer/Quarter%01d' % j
  s4 = t + '/X2layer/Quarter%01d' % j
  for k in range(1,10) :
   t1 = s1 + '/Module%01d' % k
   t2 = s2 + '/Module%01d' % k
   t3 = s3 + '/Module%01d' % k
   t4 = s4 + '/Module%01d' % k
   uiSvc.visualize(t1)
   uiSvc.visualize(t2)
   uiSvc.visualize(t3)
   uiSvc.visualize(t4)

# Draw the beam pipe
session.setParameter('modeling.color','yellow')
uiSvc.visualize('/dd/Structure/LHCb/Pipe')

