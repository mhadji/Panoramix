#
#  This script assumes to have at hand the Vis/BenderVis package.
#  (in requirements : use BenderVis v* Vis) 
#
# X11 : The X11 ROOT graphic behing launched in thread mode
# (XInitThread) then the OnX/Xt driver should have been also
# started in threded mode. This is done by having in job options :
#   OnXSvc.Threaded = true;
#
#/////////////////////////////////////////////////////////////////////////////
# Bender :
#/////////////////////////////////////////////////////////////////////////////
import gaudimodule
gaudimodule.loaddict('BenderVisDict')

_aida2root   = gaudimodule.gbl.Bender.Aida2ROOT.aida2root
_data2root1D = gaudimodule.gbl.Bender.Aida2ROOT.data2root1D
_data2root2D = gaudimodule.gbl.Bender.Aida2ROOT.data2root1D
_data2root3D = gaudimodule.gbl.Bender.Aida2ROOT.data2root1D

def aida2root ( object ) :
  if not object : return None
  try:
    obj = _aida2root( object )
    if obj : return obj
    else   : return None 
  except :   return None
  
def data2root ( object ) :
  if not object : return None
  try :
    h1 = _data2root1D ( object )
    if h1 : return h1
  except : pass
  try :
    h2 = _data2root2D ( object )
    if h2 : return h2
  except : pass
  try :
    h3 = _data2root3D ( object )
    if h3 : return h3
  except : pass
  return None 

#/////////////////////////////////////////////////////////////////////////////
# Create an histo with Gaudi :
#/////////////////////////////////////////////////////////////////////////////
import pmx
hsvc = pmx.histSvc()
if hsvc == None:
  print 'gb_test_bender : HistogramSvc not found.'
else:
  print 'gb_test_bender : HistogramSvc found.'
  histo = hsvc.book("gb_test_plot_histo","Gaudi histo from Python",100,-5,5)
  if histo == None:
    print "gb_test_bender : can't create histogram."
  else:
    print 'gb_test_bender : histogram created.'
    print 'gb_test_bender : fill histogram...'
    import random
    r = random.Random()
    for I in range(0,10000):
      histo.fill(r.gauss(0,1),1)
    del r
    # Plot with ROOT :
    print 'gb_test_bender : try to plot with the pain...'
    import ROOT
    ROOT.gROOT.Reset()
    canvas = ROOT.TCanvas()    
    aida2root(histo).Draw()
    canvas.Modified()
    canvas.Update()
    
