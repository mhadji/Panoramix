#
#  Example script to manipulate a Gaudi::XYZPoint from Python.
#
#  Execute this script with :
#    OS> <setup Panoramix>
#    OS> python gb_test_xyzpoint.py
#

import PyCintex

import sys
if sys.platform == 'win32':
  PyCintex.gbl.gSystem.Load("MathRflx.dll")
else:
  PyCintex.gbl.gSystem.Load("libMathRflx.so")

import gaudimodule

#XYZPoint = PyCintex.gbl.ROOT.Math.XYZPoint

XYZPoint = gaudimodule.gbl.ROOT.Math.XYZPoint

point = XYZPoint(0.,0.,0.)
point.SetX(3.)

print point.X()      

#///////////////////////////////////////////////////////
#// Now a std::vector of XYXPoint :
#///////////////////////////////////////////////////////

points = gaudimodule.gbl.std.vector(XYZPoint)()

point = XYZPoint(0.,0.,0.)
point.SetX(2.)
points.push_back(point)
point.SetX(3)
points.push_back(point)
print points[1].x()
