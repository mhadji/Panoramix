
from Panoramix import *

#####################################################################
# Draw a zoom of the IT wafers and OT modules in T1 to see overlap  #
#####################################################################

page = ui.currentPage()
region = page.currentRegion()
region.clear()

region_setCamera('height 1200','position 0 0 -20000','orientation 0 1 0 3.14')

# Use different colors than the standard colors from color.xml
session.setParameter('modeling.useVisSvc','false')

session.setParameter('modeling.color','blue')
session.setParameter('modeling.modeling','wireFrame')

# Draw IT1, x and u layer
for i in range(3,4) :
 s = '/dd/Structure/LHCb/IT/ITS%01d' % i
 session.setParameter('modeling.color','0.5 0 0.5')
 uiSvc.visualize(s+'/BoxT/X1') 
 uiSvc.visualize(s+'/BoxT/U')
 session.setParameter('modeling.color','0.5 0 0.5')
 uiSvc.visualize(s+'/BoxB/X1')
 uiSvc.visualize(s+'/BoxB/U')
 session.setParameter('modeling.color','1 0 1')
 uiSvc.visualize(s+'/BoxL/X1')
 uiSvc.visualize(s+'/BoxL/U')
 session.setParameter('modeling.color','1 0 1')   
 uiSvc.visualize(s+'/BoxR/X1')
 uiSvc.visualize(s+'/BoxR/U')

session.setParameter('modeling.color','blue')

# Draw OT1, x and u layer
for i in range(0,4) :
 s1 = '/dd/Structure/LHCb/OT/T1/X1layer/Quarter%01d' % i
 s2 = '/dd/Structure/LHCb/OT/T1/Ulayer/Quarter%01d' % i
 for j in range(1,10) :
  t1 = s1 + '/Module%01d' % j
  t2 = s2 + '/Module%01d' % j
  uiSvc.visualize(t1)
  uiSvc.visualize(t2)

session.setParameter('modeling.modeling','solid')

