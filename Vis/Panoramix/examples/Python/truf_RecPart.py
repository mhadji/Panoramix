from panoramixmodule import *

def execute():
 filter1 = '(unique==true)'
 
 session().setParameter('modeling.showCurve','true')
 session().setParameter('modeling.useExtrapolator','true')
 Style().setLineWidth(3) 
 
 session().setParameter('Track.location','Rec/Track/Best') 

 Style().setColor('blue')
 data_collect(da(),'Track',filter1)
 data_visualize(da())
 session().setParameter('modeling.lineWidth','0') 
 
 Style().setColor('green')
 session().setParameter('modeling.what','clusters')
 data_collect(da(),'Track',filter1)
 data_visualize(da())
 session().setParameter('modeling.what','OTMeasurements')
 Style().setColor('cyan')
 data_collect(da(),'Track',filter1)
 data_visualize(da())
 session().setParameter('modeling.what','STMeasurements')
 Style().setColor('violet')
 data_collect(da(),'Track',filter1)
 data_visualize(da())
 
 session().setParameter('modeling.what','no')
 Style().setColor('white')
 data_collect(da(),'VeloCluster','isR==true')
 data_visualize(da())
 Style().setColor('magenta')
 data_collect(da(),'VeloCluster','isR==false')
 data_visualize(da())
 Style().dontUseVisSvc()
 Style().setColor('orange')
 uiSvc().visualize('/Event/Raw/TT/Clusters')
 Style().setColor('red')
 uiSvc().visualize('/Event/Raw/IT/Clusters')
 Style().setColor('green')
 uiSvc().visualize('/Event/Raw/OT/Times')
 Style().useVisSvc()
 
# and now the muon tracks
 session().setParameter('Track.location','Rec/Track/Muon') 
 Style().setColor('green')
 Style().setLineWidth(1) 
 data_collect(da(),'Track',filter1)
 data_visualize(da())
 session().setParameter('modeling.lineWidth','0') 
 session().setParameter('modeling.what','clusters')
 data_collect(da(),'Track',filter1)
 data_visualize(da()) 
 
 ui().echo(' Visualize Track (blue) Muon(green) ')
 ui().echo(' '+filter1+' (red)')
 ui().echo(' with their measurements')
 ui().echo(' Velo R: white, Velo Phi purple')
 ui().echo(' TT: orange, IT: red, OT: green, Muon: green')
 del filter1


