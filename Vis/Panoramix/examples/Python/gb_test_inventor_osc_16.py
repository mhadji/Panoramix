#
# Example script to manipulate Inventor from Python.
#
# It assumes that the GUI with one SoPage viewer is here.
#

import CoinPython as iv
import HEPVis
import OnX

def addObjects(aRegion):
  sep = aRegion.cast_SoDisplayRegion().getStaticScene()
  if sep == None: return
  soCube = iv.SoCube()
  soCube.width = 3
  soCube.height = 1
  sep.addChild(soCube)
  soCylinder = iv.SoCylinder()
  sep.addChild(soCylinder)

def setCamera(aCamera):
  aCamera.height.setValue(10)
  aCamera.position.setValue(iv.SbVec3f(0,0,10))
  aCamera.orientation.setValue(iv.SbRotation(iv.SbVec3f(0,1,0),0))

soRegion = OnX.session().ui().currentPage().currentRegion()
addObjects(soRegion)

#setCamera(soRegion.getCamera())

import pmx
camera = pmx.Camera() # To work on the camera of the current region.
camera.setHeight(10)
camera.setPosition(0,0,10)
camera.setOrientation(0,1,0,0)
