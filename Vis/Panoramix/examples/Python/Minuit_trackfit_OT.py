from ROOT import TMinuit,Long,Double,TH2F,TH1F,TCanvas,TText,gStyle,gROOT,TF1
from array import array   
from PRplot import *
import GaudiPython, Calo_Viewer

newcont = GaudiPython.makeClass('KeyedContainer<LHCb::Track,Containers::KeyedObjectManager<Containers::hashmap> >')
Track   = GaudiPython.gbl.LHCb.Track
State   = GaudiPython.gbl.LHCb.State
location = '/Event/Rec/Cosmic/Track'
poca  = appMgr.toolsvc().create('TrajPoca', interface='ITrajPoca')
XYZPoint  = GaudiPython.gbl.ROOT.Math.XYZPoint
XYZVector = GaudiPython.gbl.ROOT.Math.XYZVector              

# Dialog inputs : 
ecolor        = session().parameterValue('Panoramix_Calo_input_ecolor')
if ecolor == '' : Calo_Viewer.defaults()

ecalenergy    = session().parameterValue('Panoramix_Calo_input_ecalenergy')
hcalenergy    = session().parameterValue('Panoramix_Calo_input_hcalenergy')
ecolor        = session().parameterValue('Panoramix_Calo_input_ecolor')
hcolor        = session().parameterValue('Panoramix_Calo_input_hcolor')

session().setParameter('modeling.lineWidth','2.') 

def store_track(track):
  t = Track()
  GaudiPython.setOwnership(t,False)
  s1 = State()
  GaudiPython.setOwnership(s1,False)
  s2 = State()
  GaudiPython.setOwnership(s2,False)
  z1 = 7000.
  x1,y1 = trackxy(track,z1)
  z2 = 20000.
  x2,y2 = trackxy(track,z2)
  s1.setX(x1)
  s1.setY(y1)
  s1.setZ(z1)
  s1.setTx( (x2-x1)/(z2-z1) )
  s1.setTy( (y2-y1)/(z2-z1) )
  s1.setQOverP(0.00001)
  t.addToStates(s1)
  s2.setX(x2)
  s2.setY(y2)
  s2.setZ(z2)
  s2.setTx( (x2-x1)/(z2-z1) )
  s2.setTy( (y2-y1)/(z2-z1) )
  s2.setQOverP(0.00001)
  t.addToStates(s2)
  cosmic_cont = newcont()
  GaudiPython.setOwnership(cosmic_cont,False)
  cosmic_cont.add(t)             
  evt.registerObject(location,cosmic_cont)

def fcn(npar, gin, f, par, iflag):
#calculate chisquare
   chisq = 0   
   global points,epoints 
# (x,y) = (tx,ty)*z + (px,py)  
   for p in range(len(points)) :
     point  = points[p]
     epoint = epoints[p] 
     deltax  = point[0] - (point[2]*par[2]+par[0])
     deltay  = point[1] - (point[2]*par[3]+par[1])
     edeltax  = (epoint[0]*epoint[0])
     edeltax += (epoint[2]*par[2])*(epoint[2]*par[2])
     edeltay  = (epoint[1]*epoint[1])
     edeltay += (epoint[2]*par[3])*(epoint[2]*par[3])
     if iflag == 3 : print 'chi2',p,(deltax*deltax/edeltax + deltay*deltay/edeltay)  
     chisq += (deltax*deltax/edeltax + deltay*deltay/edeltay)
   f[0] = chisq  
   return   

def outlier_removal(par):
   global points,epoints 
   outlier = []
   eoutlier = []   
   for p in range(len(points)) :
     point  = points[p]
     epoint = epoints[p] 
     deltax  = point[0] - (point[2]*par[2]+par[0])
     deltay  = point[1] - (point[2]*par[3]+par[1])
     edeltax  = (epoint[0]*epoint[0])
     edeltax += (epoint[2]*par[2])*(epoint[2]*par[2])
     edeltay  = (epoint[1]*epoint[1])
     edeltay += (epoint[2]*par[3])*(epoint[2]*par[3])
     chi2 = (deltax*deltax/edeltax + deltay*deltay/edeltay)  
     print 'chi2',p,chi2
     if chi2 > 49 :
      outlier.append(point)
      eoutlier.append(epoint)
   print "remove ",len(outlier)," points"   
   for o in outlier :  points.remove(o)
   for o in eoutlier :  epoints.remove(o)
   return  
    
def outliermax_removal(par):
   global points,epoints 
   chi2max = 49
   outlier = []
   eoutlier = []   
   sc = -1   
   for p in range(len(points)) :
     point  = points[p]
     epoint = epoints[p] 
     deltax  = point[0] - (point[2]*par[2]+par[0])
     deltay  = point[1] - (point[2]*par[3]+par[1])
     edeltax  = (epoint[0]*epoint[0])
     edeltax += (epoint[2]*par[2])*(epoint[2]*par[2])
     edeltay  = (epoint[1]*epoint[1])
     edeltay += (epoint[2]*par[3])*(epoint[2]*par[3])
     chi2 = (deltax*deltax/edeltax + deltay*deltay/edeltay)  
     print 'chi2',p,chi2
     if chi2 > chi2max :
      chi2max = chi2
      eoutlier = epoint
      outlier = point
   if chi2max > 49 :
    points.remove(outlier)
    epoints.remove(eoutlier)
    sc = 0
   return sc  



def fillpoints():
 points = []
 epoints = [] 
 ot = det['/dd/Structure/LHCb/AfterMagnetRegion/T/OT']
 for othitx in evt['Raw/OT/Times'] :  
 #x layer loop
     channelx = othitx.channel()
     modulex  = ot.findModule(channelx)
     name = modulex.name()  
     if name.find('X1layer')==-1 and name.find('X2layer')==-1 : continue
     lhcbidx  = GaudiPython.gbl.LHCb.LHCbID(channelx)
     trajx    = ot.trajectory(lhcbidx,0.)                        
     for othity in evt['Raw/OT/Times'] :  
 #matching stereo layer
      channely = othity.channel()
      moduley  = ot.findModule(channely)
      name = moduley.name()  
      if name.find('X1layer')>-1 or name.find('X2layer')>-1 : continue
      if channelx.station() != channely.station() : continue
      if channelx.quarter() != channely.quarter() : continue
      # found stereo hit         
      lhcbidy  = GaudiPython.gbl.LHCb.LHCbID(channely)
      trajy    = ot.trajectory(lhcbidy,0.)      
      dis  = XYZVector()
      a    = Double(0.001)
      mu1  = Double(0.1)
      mu2  = Double(0.1)
      success  = poca.minimize(trajx.get(),mu1,trajy.get(),mu2,dis,a) 
      cp = trajx.position(mu1)
      x = cp.x()
      y = cp.y()
      z = cp.z()
      points.append([x,y,z])
      epoints.append([2.,20.,2.])
 return points,epoints

def trackxy(track,z) : 
 x = track[0]+track[2]*z
 y = track[1]+track[3]*z
 return x,y

def drawline(track):
 LINES = 0
 XYZPoint = GaudiPython.gbl.Math.XYZPoint
 ps = GaudiPython.gbl.std.vector(XYZPoint)()
 x,y = trackxy(track,5000.)
 po = XYZPoint(x,y,5000.)
 ps.push_back(po)
 x,y = trackxy(track,20000.)
 po = XYZPoint(x,y,20000.)
 ps.push_back(po)
 Style().setColor('magenta')
 uiSvc().visualize(ps,LINES)  

def runMinuit(points,epoints) : 
 gMinuit = TMinuit(4);  # initialize TMinuit with a maximum of 4 params
 gMinuit.SetFCN(fcn)
 arglist = array('d',[500,1.])
 vstart  = array('d',[0. ,0. ,0. ,0.])
 step    = array('d',[1.,1.,1.,1.])
 ierflg = Long(0)      
# initial guess
 meanx,meany = 0.,0.
 for p in points:
   meanx+=p[0]
   meany+=p[1]
 vstart[0]=meanx/float(len(points)+1)   
 vstart[1]=meany/float(len(points)+1)    
 gMinuit.mnparm(0, "px", vstart[0], step[0], 0,0,ierflg)
 gMinuit.mnparm(1, "py", vstart[1], step[1], 0,0,ierflg)
 gMinuit.mnparm(2, "tx", vstart[2], step[2], 0,0,ierflg)
 gMinuit.mnparm(3, "ty", vstart[3], step[3], 0,0,ierflg)
 gMinuit.mnexcm("SIMPLEX",arglist,4,ierflg)
 gMinuit.mnexcm("MIGRAD",arglist,4,ierflg)
#gMinuit.mnexcm("MINOS",arglist,4,ierflg)
 track = [Double(0),Double(0),Double(0),Double(0)]
 etrack = [Double(0),Double(0),Double(0),Double(0)]
 for i in range(4) :
  gMinuit.GetParameter(i,track[i],etrack[i])
 return track,etrack


def select() : 
 odin = 1                                                 
 while odin :                                          
   appMgr.run(1)                                                
   s = [0,0,0,0]   
# protect against too busy events   
   if evt['Raw/OT/Times'].size()>40: continue                                              
   for othit in evt['Raw/OT/Times'] :                        
     station = othit.channel().station()                   
     s[station]+=1                                              
   if s[3]>0 and s[2]>0 and s[1]>0 : break           
 odin = evt['DAQ/ODIN']   
 if odin :                                       
  print odin.runNumber(),odin.eventNumber(),s[1],s[2],s[3]
  Page().currentRegion().clear("dynamicScene")     
 return odin

def xzview():
# Camera positioning :
 Camera().setPosition( -9042.79, 0, 13507.5)
 Camera().setHeight(15000.)
 Camera().setOrientation( 0, -1, 0, 1.57)
 Camera().setNearFar(8000.,11000.)
def xyview():
# Camera positioning :
 Camera().setPosition(0, 0, 20000)
 Camera().setHeight(13000.)
 Camera().setOrientation( 0, 0, 1, 0)
 Camera().setNearFar(0.,13000.)
 
def setup():
 Magnet_view()
 session().setParameter('modeling.modeling','wireFrame') 
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')
 #uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT')
 session().setParameter('modeling.modeling','solid')

def view3d(location):
 Style().setRGB(0.61,1.,0.63)    
 uiSvc().visualize('/Event/Raw/OT/Times') 
 Style().setRGB(0.,1.,0.2) 
 uiSvc().visualize(location) 
 Style().setRGB(0.85,0.73,0.0)    
 uiSvc().visualize('/Event/Calo/Track/Backward') 
 uiSvc().visualize('/Event/Calo/Track/Forward') 
 Style().setColor(hcolor)
 data_collect(da(),'HcalDigits','(e>'+hcalenergy+')')
 data_visualize(da())
 Style().setColor(ecolor)
 data_collect(da(),'EcalDigits','(e>'+ecalenergy+')')
 data_visualize(da())
 
 
def evtloop():
 global points, epoints 
 plots = True      
 w3d = False
 h_slopes.Reset()
 odin = 0
# event loop
 while odin : 
  odin = select() 
  if not odin : 
   print 'odin = None'
   break
  points, epoints = fillpoints()
  sc = 0 
  while sc == 0 : 
   track,etrack = runMinuit(points,epoints)                               
   sc = outliermax_removal(track)                                   
  if len(points) > 3 :
   track,etrack = runMinuit(points,epoints)
   h_slopes.Fill(track[2],track[3])
   print 'track slopes',track[2],track[3]
#   drawline(track)
   store_track(track)
   Object_visualize(evt[location][0])
   xyview()
   ui().synchronize()
   if plots : 
    widget = ui().currentWidget()
    fname = 'cosmics_ot_xy_'+str(odin.eventNumber())
    widget.write(fname+'.jpg','JPEG100') 
    xzview()
    ui().synchronize()
    widget = ui().currentWidget()
    fname = 'cosmics_ot_xz_'+str(odin.eventNumber())
    widget.write(fname+'.jpg','JPEG100') 
    if w3d : 
     Region().write_wrl()
     f=open('out.wrl')
     a = f.readlines()
     f.close()
     x=len(a)-1
     while a[x].find('diffuseColor') < 0:
      x-=1
     new = a[x].replace('diffuseColor','emissiveColor')    
     a.insert(x,new)
     f = open(fname+'.wrl','w')
     f.writelines(a)
     f.close()

def correctwrl():
 import os
 c = os.listdir('')
 for fn in c : 
  if fn.find('.wrl')!=-1 : 
   f = open(fn)
   a = f.readlines()
   f.close()
   x=len(a)-1
   while a[x].find('diffuseColor') < 0:
    x-=1
   new = a[x].replace('diffuseColor','emissiveColor')    
   a.insert(x,new)
   f = open(fn,'w')
   f.writelines(a)
   f.close()                                                               
    
def test():   
 global points, epoints    
 for h in gROOT.GetList() : 
    h.Reset()
 odin = 0
 while odin : 
  odin = select()     
  points, epoints = fillpoints()                           
  sc = 0 
  while sc == 0 : 
   track,etrack = runMinuit(points,epoints)                               
   sc = outliermax_removal(track)                                   
  if len(points) > 3 :                                     
   track,etrack = runMinuit(points,epoints)                              
   h_slopes.Fill(track[2],track[3])                        
   print 'track slopes',track[2],track[3]                  
   store_track(track)
   Object_visualize(evt[location][0])
   cal_residual(track)                                                                                   
   ui().synchronize()                                      
def searchOrPlot(sel):   
  global points, epoints
  while 1>0 :
   odin = 1
   if sel : odin = select()
   if not odin : break     
   points, epoints = fillpoints()  
   print "number of points found",len(points) 
   if len(points) > 3 :                         
    sc = 0 
    while sc == 0 : 
     track,etrack = runMinuit(points,epoints)                               
     sc = outliermax_removal(track)                                   
    if len(points) > 3 :                                     
     track,etrack = runMinuit(points,epoints)                              
     store_track(track)
     print 'track slopes',track[2],track[3],' nr of points:', len(points)                 
     break
   if not sel : break                                
 

