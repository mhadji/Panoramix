from panoramixmodule import *
from GaudiPython import gbl

VeloChannelID   = gaudimodule.gbl.LHCb.VeloChannelID
VeloLiteCluster = gaudimodule.gbl.LHCb.VeloLiteCluster
VeloCluster     = gaudimodule.gbl.LHCb.VeloCluster

# ROOT5 Code
#pair      = gaudimodule.makeClass('pair<int,unsigned int>')
#vector    = gaudimodule.makeClass('vector<pair<int,unsigned int> >') 
#newcont   = gaudimodule.makeClass('KeyedContainer<LHCb::VeloCluster,Containers::KeyedObjectManager<Containers::hashmap> >')

# ROOT6 code
pair = gbl.std.pair('int', 'unsigned int')
#vector    = cppyy.makeClass('vector<pair<int,unsigned int> >')
vector = gbl.std.vector(pair)
#newcont   = cppyy.makeClass('KeyedContainer<LHCb::VeloCluster,Containers::KeyedObjectManager<Containers::hashmap> >')
newcont = getattr(gbl, "KeyedContainer<LHCb::VeloCluster,Containers::KeyedObjectManager<Containers::hashmap> >")

def bits(w,n,m) :
  temp = w >> n
  mask = (1<<(m-n+1))-1
  result = temp & mask
  return result
def tobin(x, count=32):
         """
         Integer to binary
         Count is number of bits
         """
         return "".join(map(lambda y:str((x>>y)&1), range(count-1, -1, -1)))  

def decode_l0du():
 Enums = getEnumNames('LHCb::RawBank')
 BankType = Enums['BankType']
 rb = evt['DAQ/RawEvent']
 if not rb : 
   print "DecodePUS:  No RawEvent buffer found."
   return 
 for k in range(len(BankType)) :
  if BankType[k] == 'L0DU' :
    word3 = rb.banks(k)[0].data()[2]
    word4 = rb.banks(k)[0].data()[3]
    word5 = rb.banks(k)[0].data()[4]
    peakpos_1 = bits(word3,8,15)
    peakpos_2 = bits(word4,8,15)
    peakcon_1 = bits(word3,0,7)
    peakcon_2 = bits(word4,0,7)
    nrhits    = word5  
    if appMgr.properties()['OutputLevel'].value() < 4 : 
      print 'Pus info:  peakpos_1 peakpos_2 peakcon_1 peakcon_2 nrhits '
      print '      ','%7i'%(peakpos_1),'%9i'%(peakpos_2),'%9i'%(peakcon_1), '%9i'%(peakcon_2), '%9i'%(nrhits)
    break

def decode_l0pus():
 rb = evt['DAQ/RawEvent']
 Enums = getEnumNames('LHCb::RawBank')
 BankType = Enums['BankType']
 PuHits = [[],[],[],[]]
 for k in range(len(BankType)) :
  if BankType[k] == 'L0PU' :   
    if rb.banks(k).size() == 0 : break
    size =  int((rb.banks(k)[0].size() )/4+0.5)
    for l in range(size) :
      word = rb.banks(k)[0].data()[l]
      w = bits(word,0,15)
      if w != 0 :
       sensor = w >> 14
       strip = bits(w,0,13)
       PuHits[sensor].append(strip)
      w = bits(word,16,31)
      if w != 0 :
       sensor = w >> 14
       strip = bits(w,0,13)
       PuHits[sensor].append(strip)
    break  
 return PuHits

def decode_l0analog() :
 PuHits = [[],[],[],[]]
 ab = evt['Raw/Velo/Clusters'].containedObjects()
 for cl in ab :
  if cl.isPileUp() : 
   strip = cl.channelID().strip()
   sensor = cl.channelID().sensor() - 128
# convert to quater precision
   strip = int(strip/4) * 4  + 2 
   PuHits[sensor].append(strip)
 return PuHits

def create_L0PusClusters() :  
 decode_l0du()
 xb = decode_l0pus()
#create veloclusters from strip nr and sensor:
 L0Puscont = newcont()
 gaudimodule.setOwnership(L0Puscont, False) 
 for sensor in range(len(xb)) : 
  for strip in xb[sensor] :  
####chid = VeloChannelID(sensor+128,strip,pustype)
   chid      = VeloChannelID(sensor+128,strip)
   vlcl      = VeloLiteCluster(chid,1.,1,False)
   npair = pair(strip,99)
   adcValues = vector()
   adcValues.push_back(npair)
   vcl       = VeloCluster(vlcl,adcValues)
   vcl.key().setChannelID(chid.channelID())  
   gaudimodule.setOwnership(vcl,False)
   L0Puscont.add(vcl)
 evt.registerObject('/Event/Raw/Pus/BinaryHits',L0Puscont)
 return
