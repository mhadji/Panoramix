from panoramixmodule import *

session().setParameter('modeling.projection','-ZR')
tc = evt['Rec/Track/Best']
vc = evt['Raw/Velo/Clusters']

t = tc[205] 

Style().setColor('red')
for l in t.lhcbIDs() : 
 if l.isVelo() :
  v = vc[l.veloID().channelID()]
  Object_visualize(v)
 

