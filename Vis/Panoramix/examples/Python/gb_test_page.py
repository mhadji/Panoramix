#
# Test script for common SoPage manipulations.
#

import pmx

page = pmx.Page()
page.setTitle('gb_test_page')

page.createDisplayRegions(2,3,0)
page.setCurrentRegion(1)

page.createPlotterRegion(0.2,0.1,0.4,0.4)

region = page.currentRegion()
page.connectCurrentRegion(3)

# Region :
region = pmx.Region()
region.setBackgroundColor('blue')

page.setCurrentRegion(2)
region = pmx.Region()
region.setBackgroundRGB(1,1,0)
region.eraseEvent()
region.clear()
