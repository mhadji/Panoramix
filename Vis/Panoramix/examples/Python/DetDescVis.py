from panoramixmodule import *
std = gaudimodule.gbl.std

def eraseDetectorAndCaches():
    Page().setCurrentRegion(0)
    session().setParameter('staticScene','clear')

def detectorTree() :
    print 'this does not work anymore, what should it do ?'
    # ui().setParameter('mainTree.items', uiSvc.writeToString('/dd')) # requires two input parameters

def treeSelect() :
    print 'this does not work anymore, what should it do ?'
    selection = ''
    ui().parameterValue('mainTree.selection', selection)
    print 'Juan: DetDescVis treeSelect : ', selection

def layout_default() :
    print 'DetDescVis.layout_default()'
    Page().titleVisible(True)
    Page().setTitle("Juan")
    Page().createDisplayRegions(1,1,0)
    Page().setCurrentRegion(0)
    Region().setBackgroundColor('black')
    Camera().setPosition(0., 0., -20000.,)
    Camera().setHeight(30000.)
    Camera().setOrientation(0., 1., 0., 3.14)
    Style().setColor('cyan')
    session().setParameter("modeling.highlightColor","white");
    #Style().setWireFrame()
    session().setParameter('viewer.viewing','3D')
    session().setParameter('viewer.feedbackVisibility','true')

def layout_velo() :
    print 'DetDescVis.layout_velo()'
    Page().titleVisible(True)
    Page().setTitle("Juan")
    Page().createDisplayRegions(1,1,0)
    Page().setCurrentRegion(0)
    Region().setBackgroundColor('black')
    Camera().setPosition(0., 0., 800.,)
    Camera().setHeight(100.)
    Camera().setOrientation(0., 1., 0., 0.)
    Camera().setNearFar(0.,1000.)
    Style().setColor('cyan')
    Viewer().removeAutoClipping()
    ui().echo("Camera is at z 800 looking in -z direction.");
    ui().echo("A z slice had been set in the interval [-200,800]");
    session().setParameter("modeling.highlightColor","white");
    Style().setWireFrame()

def layout_velo_rz() :
    Page().titleVisible(True)
    Page().createDisplayRegions(1,1,0)
    Region().setBackgroundColor('black')    
    session().setParameter('modeling.projection','-ZR')
    ui().setParameter("viewer.viewing","3D")
    Region().setTransformScale(1.,16.6,1.)  
    Camera().setPosition(300., 400., 100.,)
    Camera().setHeight(1000.)
    Camera().setOrientation(0., 1., 0., 0.)

def window() :
    layout_default()
    toui()

def plotElement(element) :
    uiSvc().visualize(element)
    
def plotVelo(openX) :   
    veloR = det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight']
    veloL = det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft']
    transL = std.vector('double')(0)
    transR = std.vector('double')(0)
    rot   = std.vector('double')(3)    
    transL.push_back(openX)
    transL.push_back(0.)
    transL.push_back(0.)
    transR.push_back(-1*openX)
    transR.push_back(0.)
    transR.push_back(0.)
    veloR.geometry().localDeltaParams(transR, rot)
    veloL.geometry().localDeltaParams(transL, rot)
    uiSvc().visualize(veloR)
    uiSvc().visualize(veloL)
    
def elementFromPath(elementPath) :
    return det[elementPath]

def elementDaughters(element) :
    daughters = element.childIDetectorElements()  
    if (daughters.size()==0) :
        plotElement(element)
        print 'Drawing ', element
    for iDE in range ( daughters.size() ) :
        elementDaughters(daughters[iDE])

def plotElementLeaves(elementName) :
    element = elementFromPath(elementName)
    elementDaughters(element)

def loadElementTree(elementPath) :
    element = elementFromPath(elementPath)
    daughters = element.childIDetectorElements()    
    for iDE in range ( daughters.size() ) :
        print 'loading ', daughters[iDE].name()
        loadElementTree( daughters[iDE].name() )

