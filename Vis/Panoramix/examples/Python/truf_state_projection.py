#
# change projection to look along the track trajectory
#
from panoramixmodule import *

key_of_track = 4

tracklocation = 'Rec/Track/Best'
tc = evt[tracklocation]

session().setParameter('Track.location',tracklocation)
session().setParameter('modeling.useExtrapolator','true')

page   = OnX.session().ui().currentPage()

t = tc[key_of_track]
print 'momentum ',t.p(),t.pt()

one_state = t.states()[0]

Viewer().removeAutoClipping()
tx = one_state.tx()
ty = one_state.ty()
Camera().lookAt(tx,ty,-1.)

Object_visualize(t)

