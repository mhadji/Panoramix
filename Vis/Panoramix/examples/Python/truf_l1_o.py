from panoramixmodule import *

def l1_spacetracks() :
  session.setParameter('modeling.showCurve','true')
  session.setParameter('modeling.useExtrapolator','true')

# TrgTrackLocation::L1SpaceVelo = Rec/L1/VeloSpaceTracks
# TrgTrackLocation::Long = Rec/Trg/LongTracks
# TrgTrackLocation::SpaceVelo = Rec/Trg/VeloSpaceTracks

  session.setParameter('TrgTrack.location','Rec/L1/VeloSpaceTracks')

  session.setParameter('modeling.lineWidth','4') 
  session_setColor('blue')
  data_collect('TrgTrack','trgbits>0')
  data_visualize()
  session.setParameter('modeling.lineWidth','2') 
  session_setColor('green')
  data_collect('TrgTrack','trgbits<1')
  data_visualize()

  session.setParameter('modeling.what','no')
  data_collect('MCParticle','(charge!=0)&&(mass>0.1)&&(mass<1000)&&(timeOfFlight>0.1)&&(bcflag>0)')
  data_visualize()

  session_setColor('gold')
  data_collect('SceneGraph','highlight==false')
  data_filter('name','TrgTrack*')
  session.setParameter('modeling.what','MCParticle')
  data_visualize()

  session_setColor('olivegreen')
  uiSvc.visualize('/Event/Rec/Trg/Vertex2D')  
    
  session_setColor('white')
  data_collect('VeloCluster','isR==true')
  data_visualize()
  session_setColor('violet')
  data_collect('VeloCluster','isR==false')
  data_visualize()
  
  session_setColor('red')
  data_collect('SceneGraph','highlight==false')
  data_filter('name','MCParticle*')
  session.setParameter('modeling.what','Clusters')
  data_visualize()
     
  session.setParameter('modeling.what','no')
  session.setParameter('modeling.lineWidth','1') 

page = ui.currentPage()
region = page.currentRegion()

l1_spacetracks()

del region;del page
