from panoramixmodule import *

def trg_spacetracks() :

  session.setParameter('modeling.showCurve','true')
  session.setParameter('modeling.useExtrapolator','true')

# TrgTrackLocation::L1SpaceVelo = Rec/L1/VeloSpaceTracks
# TrgTrackLocation::Long = Rec/Trg/LongTracks
# TrgTrackLocation::SpaceVelo = Rec/Trg/VeloSpaceTracks

  session.setParameter('TrgTrack.location','Rec/Trg/VeloSpaceTracks')
  session.setParameter('modeling.lineWidth','2') 
  session_setColor('green')
  data_collect('TrgTrack','')
  data_visualize()

  session_setColor('blue')
  session.setParameter('TrgTrack.location','Rec/Trg/LongTracks')
  data_collect('TrgTrack','')
  data_visualize()
  
  session_setColor('cyan')
# extrapolator very slow
#  uiSvc.visualize('/Event/Phys/Trg/Particles')  


  session.setParameter('modeling.what','no')
  session_setColor('red')
  data_collect('MCParticle','(charge!=0)&&(mass>0.1)&&(mass<1000)&&(timeOfFlight>0.1)&&(bcflag>0)')
  data_visualize()

  session_setColor('gold')
  data_collect('SceneGraph','highlight==false')
  data_filter('name','TrgTrack*')
  session.setParameter('modeling.what','MCParticle')
  data_visualize()

  session_setColor('olivegreen')
  uiSvc.visualize('/Event/Phys/Trg/Vertices')  
    
  session_setColor('white')
  data_collect('VeloCluster','isR==true')
  data_visualize()
  session_setColor('violet')
  data_collect('VeloCluster','isR==false')
  data_visualize()
  
  session_setColor('red')
  data_collect('SceneGraph','highlight==false')
  data_filter('name','MCParticle*')
  session.setParameter('modeling.what','Clusters')
  data_visualize()
     
  session.setParameter('modeling.what','no')
  session.setParameter('modeling.lineWidth','1') 


page = ui.currentPage()
region = page.currentRegion()

trg_spacetracks()

del region;del page
