from panoramixmodule import *

def display(view = '-ZR',option='noMC'):

 if not ui().findWidget('Viewer_2d')  :
  sys_import('truf_Velo_RZView_setup')  
 ui().setCurrentWidget(ui().findWidget('Viewer_2d'))        
 Page().setCurrentRegion(0)
 session().setParameter('modeling.projection',view)
 session().setParameter('modeling.what','no')  
 
 if option != 'clustersOnly':
  session().setParameter('modeling.showCurve','true')
  # session().setParameter('modeling.useExtrapolator','true')
  tracklocation = 'Rec/Track/Best'
  session().setParameter('Track.location',tracklocation)
  Style().setColor('green')
  data_collect(da(),'Track','velo==true&&unique==true')
  data_visualize(da())
  Style().setColor('greenyellow')
  data_collect(da(),'Track','upstream==true&&unique==true')
  data_visualize(da())
  Style().setColor('blue')
  data_collect(da(),'Track','long==true&&unique==true')
  data_visualize(da())
  Style().setColor('skyblue')
  data_collect(da(),'Track','downstream==true&&unique==true')
  data_visualize(da())

 Style().setColor('violet')
 data_collect(da(),'VeloCluster','isR==false')
 data_visualize(da())

 Style().setColor('white')
 data_collect(da(),'VeloCluster','isR==true')
 data_visualize(da())

 if option != 'clustersOnly':
  session().setParameter('modeling.what','clusters')
  Style().setColor('red')
  data_collect(da(),'Track','')
  data_visualize(da())

  if option == 'withMC' :
   session().setParameter('modeling.what','no')
   session().setParameter('modeling.color','red')
   data_collect(da(),'MCParticle','(charge!=0)&&(mass>0.1)&&(mass<1000)&&(timeOfFlight>0.1)&&(bcflag>0)')
   data_visualize(da())
   session().setParameter('modeling.color','gold')
   session().setParameter('modeling.what','MCParticle')
   data_collect(da(),'Track','')
   data_visualize(da())

  Style().setColor('red')
  uiSvc().visualize('/Event/Rec/Vertex/Primary')  

  Style().setColor('yellow')
  session().setParameter('modeling.what','no')
  Style().setLineWidth(1.) 

  Style().setColor('blue')
  session().setParameter('modeling.projection','')

