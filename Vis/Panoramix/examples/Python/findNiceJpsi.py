# find nice events
from panoramixmodule import *
import PRplot,PR_Viewer,LHCbMath,math
import GaudiKernel.SystemOfUnits as units
from GaudiPython import gbl
XYZPoint =  gbl.ROOT.Math.XYZPoint
XYZVector = gbl.ROOT.Math.XYZVector
container = 'Phys/JpsiMonitor/Particles'
ParticleID = gbl.LHCb.ParticleID
sqrt       = gbl.Math.sqrt
MCParticle = gbl.LHCb.MCParticle
Track      = gbl.LHCb.Track
LorentzVector  = gbl.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')
XYZVector      = gbl.Math.XYZVector
boost          = GaudiPython.gbl.Math.Boost

def print_jpg():
  tevt = "%05d" % (evt['DAQ/ODIN'].eventNumber())
  ttext = str(evt['DAQ/ODIN'].runNumber())+'_'+tevt+'_'
  PRplot.wprint('Jpsi_'+ttext+'.jpg',format='JPEG',quality='100',info=False)    


def myEventLoop(flag=False):
 found = False 
 while 1>0:
  appMgr.run(1)
  if not evt['DAQ/ODIN']: break
  if not evt[container]: continue
  for aJpsi in evt[container]: 
    mass = aJpsi.momentum().mass()/units.GeV
    if mass > 3.086 and mass < 3.106:    
     print mass, aJpsi.key() 
     found = True
  if found : break
 if found: 
   if flag :
    createPage()
    drawV0(0)
   else: 
    PR_Viewer.execute(version='with Velo')
    PV    = evt['Rec/Vertex/Primary'][0]
    pvpos = PV.position()
    overlay_v0properties(aJpsi,pvpos,flag)
    tevt = "%05d" % (evt['DAQ/ODIN'].eventNumber())
    ttext = str(evt['DAQ/ODIN'].runNumber())+'_'+tevt+'_'
    PRplot.wprint('Jpsi_'+ttext+'.jpg',format='JPEG',quality='100',info=False)    

 else:
   print 'probably end of file reached'

def overlay_v0properties(aV0,pvpos,flag):
  soPage = ui().currentPage()
  if (soPage.getNumberOfRegions() < 5 and flag or soPage.getNumberOfRegions() < 7 and not flag) : 
    if flag:  soRegion = soPage.createRegion('SoTextRegion',0.01,0.7,0.3,0.1)
    else:     soRegion = soPage.createRegion('SoTextRegion',0.51,0.2,0.3,0.1)
    soTextRegion = soRegion.cast_SoTextRegion()
    soTextRegion.verticalMargin.setValue(0)  
  if flag: reg = soPage.getRegion(4)
  else:    reg = soPage.getRegion(6)
  soTextRegion = reg.cast_SoTextRegion()
  soTextRegion.text.set1Value(0,'mu-mu mass=(%4.1f+/-%2.1f)MeV/c2'%(aV0.measuredMass(),aV0.measuredMassErr()) )
  soTextRegion.text.set1Value(1,'momentum: p=%4.2f GeV/c pt=%2.2f GeV/c'%(aV0.p()/(units.GeV),aV0.pt()/(units.GeV)) )
  v0pos = aV0.endVertex().position()
  delx = v0pos.x()-pvpos.x()
  dely = v0pos.y()-pvpos.y()
  delz = v0pos.z()-pvpos.z()
  direction = XYZVector(dely,dely,delz)
  mom = aV0.momentum()
  momdir = XYZVector(mom.x(),mom.y(),mom.z())
  cosangle =  momdir.Dot(direction)/(momdir.r()*direction.r())
  dist = math.sqrt( delx*delx+dely*dely+delz*delz )
  #soTextRegion.text.set1Value(2,'decaylength=%4.2fmm  cos(alpha)=%6.5f'%(dist,cosangle) )


def createPage(): 
 page = ui().currentPage()
 page.deleteRegions()
 Page().setTitle('V0')
 Page().titleVisible(True)
 Page().createDisplayRegions(1,2,0)
 Page().setCurrentRegion(1) 
 #uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft')
 #uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight')

 # ui().executeScript('DLD','Panoramix layout_rulers')

def drawV0(key,new=False):
 if new : createPage()
# take first for the moment
 PV    = evt['Rec/Vertex/Primary'][0]
 pvpos = PV.position()
 aV0 = evt[container][key]
 print aV0
 v0pos = aV0.endVertex().position()
 points = gbl.std.vector(XYZPoint)()
 points.push_back(v0pos)
 points.push_back(pvpos)
 LINES = 0
 for n in range(2):
  Page().setCurrentRegion(n) 
  page   = OnX.session().ui().currentPage()
  height = math.sqrt( (v0pos.x()-pvpos.x())*(v0pos.x()-pvpos.x()) + (v0pos.y()-pvpos.y())*(v0pos.y()-pvpos.y())) * 2.
  region = page.getRegion(n)
  Style().setColor('skyblue')
  data_collect(da(),'Track','long==true&&unique==true&&pt>500')
  data_visualize(da())

  # PRplot.disp_tracks(0)  
  if n == 0 : 
   Camera().setPosition( (v0pos.x()+pvpos.x())/2.,(v0pos.y()+pvpos.y())/2.,5000.)
   Camera().setHeight(height)
   Camera().setOrientation(0,0,1,0)
   Camera().setNearFar(0.,25000.)
   tsf = region.cast_SoDisplayRegion().getTransform()
   tsf.scaleFactor.setValue(iv.SbVec3f(1.,1.,1.))  
  if n == 1 : 
   height = abs(v0pos.z()-pvpos.z()) * 2.
   Camera().setPosition( 0.,20000.,(v0pos.z()+pvpos.z())/2.)
   Camera().setHeight(height)
   Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
   Camera().setNearFar(0.,25000.)
   tsf = region.cast_SoDisplayRegion().getTransform()
   tsf.scaleFactor.setValue(iv.SbVec3f(10.,10.,1.))  
  Style().setColor('yellow')
  Object_visualize(aV0) 
  Object_visualize(aV0.endVertex()) 
  session().setParameter('modeling.lineWidth','0') 
  tmp = session().parameterValue('modeling.lineWidth')
  session().setParameter('modeling.lineWidth','3.')  
  Style().setColor('yellow')
  Object_visualize(aV0) 
  Object_visualize(aV0.endVertex()) 
  uiSvc().visualize(points,LINES) 
  Style().setColor('cyan')
  Object_visualize(PV) 
  for sdaughter in aV0.daughters():
   daughter = sdaughter.target()
   Style().setColor('red')
   Object_visualize(daughter)
   proto = daughter.proto()
  session().setParameter('modeling.lineWidth',tmp) 
  overlay_runeventnr(t1=0.75,t2=0.77,t3=0.2,t4=0.1,i1=0.01,i2=0.01,i3=0.0,i4=0.0,logoonly=False)
  overlay_v0properties(aV0,pvpos,True)

LorentzVector  = GaudiPython.gbl.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')
ParticleID = GaudiPython.gbl.LHCb.ParticleID
import math
def create_LorentzVector(pid,p):
       m   = partsvc.find(ParticleID(pid)).mass()
       rsq = m*m + p.Mag2()
       E   = math.sqrt(rsq)
       lv = LorentzVector(p.x(),p.y(),p.z(),E)
       return lv
def addTracks():
  ajpsi = evt['Phys/JpsiMonitor/Particles'][0].momentum()
  amom = evt['Rec/Track/Best'][17].momentum()
  newmom = create_LorentzVector(321,amom) 
  newPart = newmom + ajpsi
  return newPart

def pointing(aV0,pvpos,mom):
  v0pos = aV0.endVertex().position()
  delx = v0pos.x()-pvpos.x()
  dely = v0pos.y()-pvpos.y()
  delz = v0pos.z()-pvpos.z()
  direction = XYZVector(delx,dely,delz)
  momdir = XYZVector(mom.x(),mom.y(),mom.z())
  cosangle =  momdir.Dot(direction)/(momdir.r()*direction.r())
  dist = math.sqrt( delx*delx+dely*dely+delz*delz )
  print 'cosangle = ',cosangle,' dist = ',dist



#************************************************
#*    runNumber * eventNumb * Jpsi_tau * nPV
#************************************************
#*   69627 *  87192657 * 4.5672788 *  2
#*   69860 *   3754057 * 3.4184768 *  1
#*   69618 *     12484 * 0.5975813 *  1
#*   69625 *  45507449 * 0.2373959 * 1
#************************************************




