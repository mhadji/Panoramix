import OnX
import ExploreDraw as D

ui = OnX.session().ui()

def isset(toggle_instance):
	param = ui.parameterValue(toggle_instance+'.set')	
	if param == 'true':
		return 1
	else:
		return 0
	
#wire=isset('tmi_wire')
#if wire: D.s_modeling('w')
#else: D.s_modeling('s')
#
#transp=isset('tmi_transp')
#if transp: 
#	D.s_transparency(0.5)
#	D.s_modeling('s')
#else: D.s_transparency(0)
#
#vissvc=isset('tmi_vissvc')
#if vissvc: D.s_useVisSvc(1)
#else: D.s_useVisSvc(0)

color = ui.parameterValue('styleDialog_input_color.value');
D.s_color(color);			
transp = ui.parameterValue('styleDialog_input_transparency.value');
D.s_transparency(float(transp));			
modeling = ui.parameterValue('styleDialog_input_modeling.value');
D.s_modeling(modeling);
linewidth = ui.parameterValue('styleDialog_input_linewidth.value');
D.s_lw(float(linewidth));
markersize = ui.parameterValue('styleDialog_input_markersize.value');
D.s_markerSize(float(markersize));		
modopen = ui.parameterValue('styleDialog_input_modopen.value');
if modopen=='yes': D.s_opened(1)
else: D.s_opened(0)
vissvc = ui.parameterValue('styleDialog_input_vissvc.value');
if vissvc=='yes': D.s_useVisSvc(1)
else: D.s_useVisSvc(0)
