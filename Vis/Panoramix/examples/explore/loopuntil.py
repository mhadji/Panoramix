import OnX
import ExploreDraw as D
import ExploreLib as L

ui = OnX.session().ui()

counter = 0
MAXLOOPS = 100

havetrig = 1
havesel = 1

while 1:

	D.appMgr.run(1)
	counter += 1

	trigstr = ui.parameterValue('id_loopuntil_input_trigger.value')
	selstr = ui.parameterValue('id_loopuntil_input_selection.value')
	
	if not trigstr == '':
		trigsels = D.HLTSUM.selections()
		havetrig = len( L.findStrOnList(trigstr, trigsels)	)
	
	if not selstr == '':
		selections = D.evt['Phys/Selections']
		selstrs = L.SelectionsList(selections)
		havesel = len(L.findStrOnList(selstr, selstrs) )
	
	if havetrig and havesel:
		print 'picking event after', counter, 'tries'
		break
	
	if counter == MAXLOOPS:
		print 'leaving loop after', MAXLOOPS ,'tries, try again'
		break
