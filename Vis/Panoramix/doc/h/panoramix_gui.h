/**

@page panoramix_gui The main panel

  The GUI (Graphical User Interface) is organized around a main panel

@image html panoramix_1.jpg
@image latex panoramix_1.ps

 This panel has a menubar. Below it and on the left, there is
 a "tree widget" to visualize hierarchical data like the detector, 
 event trees. At its right there is an Open Inventor viewer with 
 its "decorations". Below the tree widget
 and the Inventor viewer, there is a message dump area with a scrollbar
 at its right. Below the dump message area, there is a command typing area.
 At the left of the typing area there is an option menu to choose 
 for which interpreter the typed text is for. By default it is Python,
 but someone may want to switch to "system" to give commands to 
 the system (try a ls on UNIX or a dir on Windows).

  The GUI must be though as a "document" editor. The document being
 what is inside the Inventor viewer. All the rest (menus, commands,
 dialog boxes) are here to feed the document with various representations
 of the data (detector, event data, histograms, etc...).

  The below schema shows a logical view of the GUI organization :
@image html panoramix_6.jpg
@image latex panoramix_6.ps

@section panoramix_gui_menubar The menu bar

  The main panel has one menu bar with dedicated pulldown menus.
 The pulldown menus may have submenus coming from subpackages. The XML
 description of the GUI permits to easily "inherit" some menu from some 
 packages. For example the Panoramix "Scene" pulldown menu 
 (Panoramix/scrtips/OnX/Scene.onx file), have a submenu coming
 from the OnX package (OnX/scripts/OnX/Scene.onx) for very general
 scene manipulations. The Panoramix Camera contains LHCb dedicated
 camera positionning and a submenu, coming from OnX, with the "dump" item
 that permits to dump the current camera positionning. The Panoramix "Event"
 pulldown menu ineherits the "Rec" menu coming from the SoEvent package, etc...

@section panoramix_gui_dialogs The dialog boxes (or Editors)

  Some menu items may map "dialog boxes" or "Editors". The editors
 are the real "magic potions" of the system. They permit to avoid
 tedious typing to customize things. One major challenge will be to have
 these editors the most user friendly and intuitive as possible in order
 to do good part of things with the mouse. The today layout
 and "semantic" of these panels may evolve in the future according to
 user feedbacks.

  For the moment, the today layout is that an input dialog has various
 "input area" to enter things, a last combo box to select an action, and
 the classical "Ok, Apply, Cancel" button group. When the user click 
 on the ok or apply button, the various input area are read and are dispatched
 to various action scripts (see, for example, the 
 SoEvent/scripts/OnX/InputBest.onx for an example on how to write an 
 input dialog with OnX/XML).

  To play with, click the "Session/Session/Session parameter editor" menu item
 to map a dialog box that permits to change the current modeling color.
 You must see something like :

 @image html panoramix_2.jpg
 @image latex panoramix_2.ps
 <p>
  Choose a color in the first "combo box". Here you can give an "rgb" input
 like (0.4 0.3 0.2) if the color you want is not in the list. Click on "Apply"
 to set the current modeling color. Visualize for example the Ecal with
 "Detector/Ecal". If all is ok, the new "scene graph objects" should 
 have the desired color.

  You can change the current modeling "shape" style (solid/wire frame) in 
 the same way. Choose solid or wire frame in the second combo, select
 "modeling.modeling" in the "Action" combo, clik in "Apply" to change the
 current modeling shape type. Erase the scene with "Scene/Scene/Erase Detector"
 and revisualize the Ecal with "Detector/Ecal".

@section panoramix_gui_customization Customization of the GUI

  The GUI is constructed from a main OnX XML file (.onx suffix). By default, 
 the file associated to the ONXFILE environment variable is used.
 To customize the GUI, have a modified local copy of the XML GUI file(s) 
 and change the ONXFILE environment variable. If starting from the onx 
 program, you can specify directly the main XML GUI file with 
 the "file" option, for example :
@verbatim
   csh> onx -file <my file>
@endverbatim

  If starting from a Gaudi program, you can specify directly the main 
 XML GUI file with the OnXSvc.File property in the job option file, for 
 example :
@verbatim
   OnXSvc.File = "<my file>"
@endverbatim

  For example, you can have your own Event pulldown menu. Have a copy
 of Panoramix.onx and Event.onx. In Panoramix.onx, change the line :

@verbatim
  <reference file="$PANORAMIXROOT/scripts/OnX/Event.onx"/>
@endverbatim
 to access your own Event.onx file. In your own Event.file, add for 
 exemple the follwing XML lines after other menu items :

@verbatim
    <widget class="MenuItem">
      <label>Hello me</label>
      <activate exec="Python">print 'Hello me'</activate>
    </widget>
@endverbatim
 Restart the program. If all is ok, a "Hello me" menu item should be here
 at the end of the Event pulldown menu. Click on it, a message "Hello me"
 should appear in the message area. 

  Note that here a Python script had been given, but scripts for other 
 interpreters may be used, for example :
@verbatim
    <widget class="MenuItem">
      <label>ls</label>
      <activate exec="system">ls</activate>
    </widget>
@endverbatim
 
@section panoramix_gui_menus More on default pulldown menus

  The Panoramix/scripts/OnX/Panoramix.onx file contains a set of 
 default menus (oftenly put in standalone .onx files). We detail them here.

@subsection panoramix_gui_menu_file The File menu

 "import..." maps a file chooser to pick a Python file and execute it.

 "output -> OnX" redirects all output (cout, cerr, stdout, stderr) in 
   the GUI dump area.

 "output -> terminal" redirects all output to the terminal.

 "Exit" exits the program.

@subsection panoramix_gui_menu_detector The Detector menu

  The Detector menu permits to visualize various parts of the detector.
 For the moment, the existing items had been put by the developers
 to test things. This menu may evolve according to user feedbacks.

 "Erase All (and Caches)" erases detector and event scene graphs. Erase
 also the XML detector cache. This permits to edit the XML detector 
 description and revisualize the detector without relaunching 
 the program.

@subsection panoramix_gui_menu_event The Event menu

 "Next event" brings a new event in memory (at startup no event is loaded).

 "Infos" dumps run and event number.

 "MC" leads to submenu dedicated to visualize MC data (Vis/SoEvent package).

 "Rec" leads to submenu to visualize reconstructed data (Vis/SoEvent package).

 "Calo" leads to submenu to visualize Calorimeter data (Vis/SoCalo package).

@subsection panoramix_gui_menu_scene The Scene menu

 "Scene/Erase detector" erases detector scene graph.

 "Scene/Erase event" erases event scene graph.

 "Scene/Session parameter editor" maps the parameter editor panel.

 "Scene/Graphical object editor" maps the editor to change graphical objects.
 A graphical object could be highlighted by passing the viewer in 
 "picking mode" (top arrow icon on the right of the viewer) and clicking
 on it with the left button of the mouse. Remember to pass the viewer
 in "viewing mode" (the "hand" icon) to navigate by moving the cursor
 within the viewer.

 "Scene/OnX data accessor" maps a dialog to select data declared to the 
 OnX "type manager". In particuler when selecting the "SceneGraph" data type,
 you can ask to erase selectivly some objects in the scene graph. For example
 if you have drawn some ICluster in the scene and want now to erase them ; 
 type "name=='*Cluster*'" in the "Data filter" input area of the dialog, 
 choose the "erase" Action in the below combobox, and click on "Apply".

 "Scene/Current color chooser" maps a color chooser to set current 
 modeling color.

 "Frame m" creates a frame at meter scale.

 "Frame mm" creates a frame adequate for vertex scanning.

 "Floor" loads an inventor file that represent the floor.

@subsection panoramix_gui_menu_camera The Camera menu 

 "Front, back, side, 3D, Vertex" set camera position according LHCb layout.

 "Dump" dumps camera position (usefull to create new camera positionning buttons).

@subsection panoramix_gui_menu_page The Page menu 

  The "Page" is the viewer content. The page is organized
 in viewing regions (regions in short). The one with a red border is 
 the "current" one. By default, only one region is here. Click on 
 the "Page/2rx1c" item to have to viewing regions.

 "next, prev" set the current active viewport region (the one with a 
 red border) to the neightbourg one (left to rigth, top to bottom).
 Note that when the viewer is in "picking mode", you can use the 
 left and right arrows to change the current active region.

 "Page layout editor" maps a dialog that permits to
 change the number of regions, the backgroung of the current regions, etc...
 (see the Dialog section for more).

 "Region layout editor" maps a dialog that permits to
 change with sliders the position and size of the current region.

 "1x1, 2rx1c" set a grid of viewport regions in the current page.

 "Region page" maps the current region to the whole page.

 "2D, 3D" prepares the region for a 2D (for exa histo plotting) 
 or 3D scene viewing.

 "black, white" set background color of the current region.

 "view all" maps the viewport camera viewing to match the bounding box scene.
 Same action than the viewer "eye" icon.

 "Print" maps the print dialog.

 "Page title on/off" set/remove global page title.

 "Create region" creates a new viewport region. The region could
 be resized and moved by using the "Region layout editor".

 "Delete region" deletes the current viewport region.

@subsection panoramix_gui_menu_tree The Tree menu 

  On top of the tree widget, there is a pulldown menu to do action
 on the tree widget and on the selected item in the tree widget. 
 (On Windows, this menu is still in the main menubar). 

 "Tree/Detector" shows the detector hierarchy in the tree widget.

 "Tree/Event" shows the event hierarchy in the tree widget.

 "Tree/stat" shows the Gaudi stat hierarchy in the tree widget.

 "Tree/Vis" visualizes  the selected item in the tree widget.
 Note that for histograms, the viewer should be first set 
 in "plotting mode" with the "Page/Plotting" button. 
 The plotting mode is a special scene layout ready to visualize histos.

@subsection panoramix_gui_menu_help The Help menu 

  The Help menu permits to dump some help text about various items.

@subsection panoramix_gui_menu_analysis The Analysis menu 

  If the Lab package is installed, the Analysis menu permits to 
 plot histograms.

 "Test Gaudi histo" creates, from python, some Gaudi histos. The
  "Tree/stat" button permits then to visualize the "stat" hierarchy
  in the tree widget. Click in "Page/Plotting" to set the plotting 
 layout in the viewer. Select one histo and click in the "Tree/Vis" 
 button to visualize it.
 
 "MCParticle e<20" triggers a script to demo the Lab histo system.

@subsection panoramix_gui_menu_plotter The Plotter menu 

  The Plotter menu permits to customize the current histo plot.

*/
