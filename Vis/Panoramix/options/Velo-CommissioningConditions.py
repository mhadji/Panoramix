from Gaudi.Configuration import *
from Configurables import UpdateManagerSvc

#2008, open position
#UpdateManagerSvc().ConditionsOverride +=  ["Conditions/Online/Velo/MotionSystem := double ResolPosRC =-29. ; double ResolPosLA = 29.;"]
#2008, open position

# force closed position
#UpdateManagerSvc().ConditionsOverride +=  ["Conditions/Online/Velo/MotionSystem := double ResolPosRC =0. ; double ResolPosLA = 0.;"]
#UpdateManagerSvc().ConditionsOverride += ["Conditions/Alignment/Velo/VeloRight := double_v dPosXYZ = 0 0 0;",  "Conditions/Alignment/Velo/VeloLeft  :=  double_v dPosXYZ = 0 0 0;"]

#2009, closest position
UpdateManagerSvc().ConditionsOverride +=  ["Conditions/Online/Velo/MotionSystem := double ResolPosRC =-20. ; double ResolPosLA = 20.;"]
#2009, closest position

