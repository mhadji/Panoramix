from Gaudi.Configuration import appendPostConfigAction
from Configurables import ApplicationMgr, CondDB, COOLConfSvc

CORAL_XML_DIR = "/group/online/condb_viewer"
ApplicationMgr().Environment["CORAL_AUTH_PATH"] = CORAL_XML_DIR
ApplicationMgr().Environment["CORAL_DBLOOKUP_PATH"] = CORAL_XML_DIR

CondDB(UseOracle = True)
CondDB().UseLatestTags = ["2011"]

def disableLFC():
    COOLConfSvc(UseLFCReplicaSvc = False)
    CondDB().IgnoreHeartBeat = True
appendPostConfigAction(disableLFC)
