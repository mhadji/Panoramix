from Gaudi.Configuration import *
import GaudiKernel.SystemOfUnits as units

bunch = ["Prev2/","Prev1/","","Next1/","Next2/"]
CosmicReco = False

from Configurables import TrackSys,MagneticFieldSvc

## Set of standard fitting options
#importOptions( "$TRACKSYSROOT/options/Fitting.py" )

MagneticFieldSvc().ScaleFactor = 0
TrackSys().setSpecialDataOption("fieldOff",True)
TrackSys().ExpertTracking = ["noDrifttimes"] 
TrackSys().SpecialData = ['fieldOff']

#PatSeeding for collisions events
from Configurables import (OTRawBankDecoder,Tf__OTHitCreator,Tf__STHitCreator_Tf__IT_,PatTStationHitManager,TrackEventFitter,PatSeeding,PatSeedingTool)
pat_conf = {}
for x in bunch : 
 b = x.replace('/','')
 #importOptions('$PATALGORITHMSROOT/options/PatSeedingTool-Cosmics.opts')
 pat_conf['PatSeeding'+b] = PatSeeding("PatSeeding"+b)
 pat_conf['PatSeeding'+b].OutputTracksName = x+'Rec/Track/Seed'

 pat_conf['PatSeeding'+b].addTool(PatSeedingTool, name='PatSeedingTool')
 # options to tune PatSeeding for tracking with B field off
 pat_conf['PatSeeding'+b].PatSeedingTool.xMagTol = 4e2;
 pat_conf['PatSeeding'+b].PatSeedingTool.zMagnet = 0.;
 pat_conf['PatSeeding'+b].PatSeedingTool.FieldOff = True;
 pat_conf['PatSeeding'+b].PatSeedingTool.MinMomentum = 5e4;

 # cosmic reconstruction
 if CosmicReco : 
  pat_conf['PatSeeding'+b].PatSeedingTool.Cosmics = True;
 # relax requirements on number of hits/planes
 # (this may need tweaking if some C frames are in "open" position) 
  pat_conf['PatSeeding'+b].PatSeedingTool.MinXPlanes = 4;
  pat_conf['PatSeeding'+b].PatSeedingTool.MinTotalPlanes = 8;
  pat_conf['PatSeeding'+b].PatSeedingTool.OTNHitsLowThresh = 9;
  pat_conf['PatSeeding'+b].PatSeedingTool.MaxMisses = 2;
 # relax pointing requirement (no PV for cosmics!)
  pat_conf['PatSeeding'+b].PatSeedingTool.MaxYAtOrigin = 4e5;
  pat_conf['PatSeeding'+b].PatSeedingTool.MaxYAtOriginLowQual = 8e5;
  pat_conf['PatSeeding'+b].PatSeedingTool.xMagTol = 4e5;
  pat_conf['PatSeeding'+b].PatSeedingTool.MinMomentum = 1e-4; # pointing requirement in disguise!
 else : 
  pat_conf['PatSeeding'+b].PatSeedingTool.Cosmics = False;

 # detector not yet aligned - chi^2 should not contribute to track quality
 pat_conf['PatSeeding'+b].PatSeedingTool.QualityWeights        = [ 1.0, 0.0 ]
 pat_conf['PatSeeding'+b].PatSeedingTool.addTool(PatTStationHitManager, name='PatTStationHitManager')
 pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.addTool(Tf__OTHitCreator('OTHitCreator'))
 pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.OTHitCreator.addTool(OTRawBankDecoder, name='OTRawBankDecoder')
 pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.OTHitCreator.OTRawBankDecoder.RawEventLocation = x+'DAQ/RawEvent'
 pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.OTHitCreator.OTRawBankDecoder.OutputLevel = 4
 pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.OTHitCreator.OutputLevel = 4
 pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.OutputLevel = 4
 pat_conf['PatSeeding'+b].PatSeedingTool.OutputLevel = 4
 pat_conf['PatSeeding'+b].OutputLevel = 4

 if CosmicReco:
  pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.OTHitCreator.NoDriftTimes = True

 pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.addTool(Tf__STHitCreator_Tf__IT_('ITHitCreator'))
 pat_conf['PatSeeding'+b].PatSeedingTool.PatTStationHitManager.ITHitCreator.ClusterLocation=x+'Raw/IT/LiteClusters'
 pat_conf['PatSeedingFit'+b] = TrackEventFitter('PatSeedingFit'+b)
 pat_conf['PatSeedingFit'+b].TracksInContainer = x+'Rec/Track/Seed'
 pat_conf['PatSeedingFit'+b].TracksOutContainer = x+'Rec/Track/Best'

# Get the fitters
 from TrackFitter.ConfiguredFitters import ConfiguredFit, ConfiguredFitSeed
 from Configurables import TrackStateInitAlg
 GaudiSequencer("TrackSeedFitSeq"+b).Members += [TrackStateInitAlg("InitSeedFit"+b)]
 TrackStateInitAlg("InitSeedFit"+b).TrackLocation = x+"Rec/Track/Seed"
 GaudiSequencer("TrackSeedFitSeq"+b).Members += [ConfiguredFit("FitSeed"+b, x+"Rec/Track/Seed")]


 ApplicationMgr().TopAlg += [pat_conf['PatSeeding'+b],pat_conf['PatSeedingFit'+b],GaudiSequencer("TrackSeedFitSeq"+b)]



