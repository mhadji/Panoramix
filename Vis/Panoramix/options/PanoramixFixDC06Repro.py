from Gaudi.Configuration import *

# fix wrong linker tables
from Configurables import GaudiSequencer,TESCheck,EventNodeKiller,TrackAssociator
daVinciFix             = GaudiSequencer("DaVinciFix")
evtCheck               = TESCheck("EvtCheck")
eventNodeKiller        = EventNodeKiller("DaVinciEvtNodeKiller")
daVinciFix.Members    += [evtCheck,eventNodeKiller,TrackAssociator()]
evtCheck.Inputs        = ["Link/Rec/Track/Best"]
evtCheck.Stop          = False
eventNodeKiller.Nodes  = ["Link/Rec"]
ApplicationMgr().TopAlg += [daVinciFix]

