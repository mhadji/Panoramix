from Gaudi.Configuration import *
#importOptions("$PATPVROOT/options/PVLoose.py")
from PatPV import PVConf
PVConf.LoosePV().configureAlg()

from Configurables import PatPVOffline
from Configurables import TESCheck, EventNodeKiller
PVCheck = TESCheck('PVCheck')
PVCheck.Inputs = ['Rec/Vertex']
eventNodeKiller = EventNodeKiller('PVkiller')
eventNodeKiller.Nodes = ['pRec/Vertex','Rec/Vertex']
removePV = GaudiSequencer( "RemovePV",
                           Members = [PVCheck, eventNodeKiller] )
redoPV = GaudiSequencer( "RedoPV",
                         IgnoreFilterPassed=True,
                         Members = [removePV, PatPVOffline("PatPVOffline")] )

DataOnDemandSvc().AlgMap["Rec/Vertex/Primary"] = redoPV

