from panoramixmodule import *
def print_aproperties():
 print(10*'-')
 print('Application Manager Properties ')
 print(20*'-')
 print('')
 properties = gaudi.properties()
 modules = []
 for key in properties :
            if key=='Dlls' :
               modules = properties[key].value()
            if type(properties[key].value()) != type('') :
                try: 
                  first = True
                  for x in properties[key].value() : 
                      if first: 
                        first = False
                        print('%-35s : %s ' % ( key, x ) )
                      else :   print('%-35s , %s ' % (' ', x ) )
                except:   
                  print('%-35s : %s ' % ( key, properties[key].value() ) )
            else :
                  print('%-35s : %s ' % ( key, properties[key].value() ) )
gaudi = gaudimodule.AppMgr()
print_aproperties()

