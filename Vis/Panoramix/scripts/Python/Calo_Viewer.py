from panoramixmodule import *
import PR_Viewer

def defaults():
 dialog = 'Panoramix_PRViewer_dialog'
 session().setParameter('Panoramix_Calo_input_ecalenergy','0')
 session().setParameter('Panoramix_Calo_input_hcalenergy','0')
 session().setParameter('Panoramix_Calo_input_spdcalenergy','0')
 session().setParameter('Panoramix_Calo_input_prscalenergy','0')
 session().setParameter('Panoramix_Calo_input_scale','linear')
 session().setParameter('Panoramix_Calo_input_eoret','no')
 session().setParameter('Panoramix_Calo_input_ecolor','red')
 session().setParameter('Panoramix_Calo_input_hcolor','blue')
 session().setParameter('Panoramix_Calo_input_scolor','yellow')
 session().setParameter('Panoramix_Calo_input_pcolor','green')
 session().setParameter('Panoramix_Calo_input_projection',' ')
 session().setParameter('Panoramix_Calo_input_action','visualize')
 if not ui().findWidget(dialog) :  ui().showDialog( '$PANORAMIXROOT/examples/Python/PR_Viewer_setup.onx' ,dialog )
 if ui().findWidget(dialog) : 
    ui().findWidget(dialog).hide()
    ui().synchronize()
def createPage(title,nregions): 
 page = ui().currentPage()
 page.deleteRegions()
 Page().setTitle(title)
 Page().titleVisible(True)
 if nregions == 4 : Page().createDisplayRegions(2,2,0)
 if nregions == 1 : Page().createDisplayRegions(1,1,0)
 ui().currentWidget().cast_ISoViewer().setHeadlight(1)
 Style().setWireFrame()
 if nregions == 4 : 
    Page().setCurrentRegion(0)
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Prs')
    Page().setCurrentRegion(1)
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Spd')
    Page().setCurrentRegion(2)
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
    Page().setCurrentRegion(3)
    uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')
 if nregions == 1 :
     uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal') 
     Page().currentRegion().color.setValue(0.7,0.7,0.7)
 Style().setSolid()  
                                     
def execute(what,ecalenergy,hcalenergy,spdcalenergy,prscalenergy,scale,eoret,ecolor,hcolor,scolor,pcolor,action) :
 curtitle = ui().currentPage().title.getValues()[0]
 if what == 'xy' :
# setup xy projections for all 4 calorimeters on one page
  title = 'CALO XY view SPD PRS ECAL HCAL'
  if curtitle != title or action == 'recreate':  
   createPage(title,4)
   onlineViews[title] = 'Calo_XYView'
  for i in range(Page().fPage.getNumberOfRegions()) :
   Page().setCurrentRegion(i)  
# Camera positioning :
   Camera().setPosition(0., 0., 20000.)
   Camera().setHeight(7500.)
   Camera().setOrientation(0.,0.,1.,0.)
   Camera().setNearFar(-5000.,20000.)

 if what == 'lego' :
# setup lego projections for all 4 calorimeters on one page
  title = 'CALO Lego view SPD PRS ECAL HCAL'
  if curtitle != title or action == 'recreate':      
   createPage(title,4)
   onlineViews[title] = 'Calo_LEGOView'
  for i in range(Page().fPage.getNumberOfRegions()) :
   Page().setCurrentRegion(i)  
# Camera positioning :
   Camera().setPosition(-2000., -2500., 16000.)
   Camera().setHeight(9000.)
   Camera().setOrientation(0.778, -0.316, -0.542, 1.072)
   Camera().setNearFar(-1000.,17000.)
 
 if what == '3d' :
# setup side view for all 4 calorimeters in one region
  title = 'CALO 3d SPD PRS ECAL HCAL'
  if curtitle != title or action == 'recreate':      
   createPage(title,1)
   onlineViews[title] = 'Calo_3DView'
  for i in range(Page().fPage.getNumberOfRegions()) :
   Page().setCurrentRegion(i)  
# Camera positioning :
   Camera().setPosition(2000., 2000., 11000.)
   Camera().setHeight(9000.)
   Camera().setOrientation(0.50, -0.50, 0.72, 0.82)
   Camera().setNearFar(-5000.,20000.)

 if action ==  'visualize' and evt['DAQ/ODIN'] : 


  ui().echo(' Visualize Calo Digits')
 # clear regions before showing new event
  for i in range(Page().fPage.getNumberOfRegions()) : 
   if what != ' ':
    Page().setCurrentRegion(i)      
    Page().currentRegion().clear("dynamicScene")
 
   Style().dontUseVisSvc()
   if hcalenergy != 'n' :
    if what == 'xy' or what == 'lego' : Page().setCurrentRegion(3)
    Style().setColor(hcolor)
    data_collect(da(),'HcalDigits','(e>'+hcalenergy+')')
    data_visualize(da())
   if ecalenergy != 'n' :
    if what == 'xy' or what == 'lego' : Page().setCurrentRegion(2)
    Style().setColor(ecolor)
    data_collect(da(),'EcalDigits','(e>'+ecalenergy+')')
    data_visualize(da())
   if prscalenergy != 'n' :
    if what == 'xy' or what == 'lego' : Page().setCurrentRegion(1)
    Style().setColor(pcolor)
    data_collect(da(),'PrsDigits','(e>'+prscalenergy+')')
    data_visualize(da())
   if spdcalenergy != 'n' :
    if what == 'xy' or what == 'lego' : Page().setCurrentRegion(0)
    Style().setColor(scolor)
    data_collect(da(),'SpdDigits','(e>'+spdcalenergy+')')
    data_visualize(da())
   Style().useVisSvc()
   if what == ' ': break
   if ui().findWidget('Panoramix_PRViewer') :
     if ui().parameterValue('Panoramix_PRViewer_input_addinfo.value') == 'True' : overlay_runeventnr()
   else: overlay_runeventnr()

def xx(taction):
# Dialog inputs : 
 ecalenergy    = ui().parameterValue('Panoramix_Calo_input_ecalenergy.value')
 hcalenergy    = ui().parameterValue('Panoramix_Calo_input_hcalenergy.value')
 spdcalenergy  = ui().parameterValue('Panoramix_Calo_input_spdcalenergy.value')
 prscalenergy  = ui().parameterValue('Panoramix_Calo_input_prscalenergy.value')
 scale         = ui().parameterValue('Panoramix_Calo_input_scale.value')
 eoret         = ui().parameterValue('Panoramix_Calo_input_eoret.value')
 ecolor        = ui().parameterValue('Panoramix_Calo_input_ecolor.value')
 hcolor        = ui().parameterValue('Panoramix_Calo_input_hcolor.value')
 scolor        = ui().parameterValue('Panoramix_Calo_input_scolor.value')
 pcolor        = ui().parameterValue('Panoramix_Calo_input_pcolor.value')
 projection    = ui().parameterValue('Panoramix_Calo_input_projection.value')
 action        = ui().parameterValue('Panoramix_Calo_input_action.value')
 session().setParameter('Panoramix_Calo_input_ecalenergy',ecalenergy)
 session().setParameter('Panoramix_Calo_input_hcalenergy',hcalenergy)
 session().setParameter('Panoramix_Calo_input_spdcalenergy',spdcalenergy)
 session().setParameter('Panoramix_Calo_input_prscalenergy',prscalenergy)
 session().setParameter('Panoramix_Calo_input_scale',scale)
 session().setParameter('Panoramix_Calo_input_eoret',eoret)
 session().setParameter('Panoramix_Calo_input_ecolor',ecolor)
 session().setParameter('Panoramix_Calo_input_hcolor',hcolor)
 session().setParameter('Panoramix_Calo_input_scolor',scolor)
 session().setParameter('Panoramix_Calo_input_pcolor',pcolor)
 session().setParameter('Panoramix_Calo_input_projection',projection)
 session().setParameter('Panoramix_Calo_input_action',action)

 if taction != '' : 
   projection = taction
   action = 'visualize' 
 if eoret == 'yes' : 
  session().setParameter("CaloEtVis","TRUE")
 if eoret == 'no' : 
  session().setParameter("CaloEtVis","FALSE")
 if scale == 'linear' : 
  session().setParameter("CaloLogVis","FALSE")
 if scale == 'log' : 
  session().setParameter("CaloLogVis","TRUE")

 if action == 'NextEvent' :
  appMgr.run(1)
  action = 'visualize' 

 if action != 'setting':
  execute(projection,ecalenergy,hcalenergy,spdcalenergy,prscalenergy,scale,eoret,ecolor,hcolor,scolor,pcolor,action) 
 else : 
  action = 'visualize' 
