from panoramixmodule import *
from PanoramixSys.Configuration import *
import OT_Viewer

def visualizeCosmicOTTrack(flag):
 if PanoramixSys().getProp("Cosmics"):
  location = '/Event/Rec/Track/Seed'
  odin = 1                                                 
  if flag : 
    while odin :                                          
     appMgr.run(1)   
     odin = evt['DAQ/ODIN']   
     if not odin : break                                                  
     if evt[location].size()>0: break
  if not odin : print 'No Odin bank, probably End of File'
  Page().currentRegion().clear("dynamicScene")     
 else : 
  print "Panoramix not setup for OT cosmic tracks, missing option --Cosmics" 
 if evt[location] : 
    thisPage =  ui().currentPage()
    curtitle = thisPage.title.getValues()[0]
    found = False
    for t in onlineViews :
     if curtitle == t : 
      found = True
      break 
    session().setParameter('modeling.userTrackRange','true')
    session().setParameter('modeling.trackStartz','5000.')
    session().setParameter('modeling.trackEndz','20000.')
    if found : 
     x(onlineViews[t])
     Style().setRGB(0.,1.,0.2) 
     for i in range(Page().fPage.getNumberOfRegions()) : 
      Page().setCurrentRegion(i)      
      uiSvc().visualize(evt[location])
      ui().synchronize()
    else : 
      title = 'CosmicOTTrackView' 
      if curtitle != title : 
       Page().setTitle(title)
       OT_Viewer.setup()
      OT_Viewer.view3d(location)

