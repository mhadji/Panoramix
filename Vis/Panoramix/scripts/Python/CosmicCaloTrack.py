from panoramixmodule import *
newcont                 = GaudiPython.makeClass('KeyedContainer<LHCb::Track,Containers::KeyedObjectManager<Containers::hashmap> >')
CaloCosmicsTrackTool    = tsvc.create('CaloCosmicsTrackTool',  interface='ICaloCosmicsTrackTool')

def visualizeCosmicCaloTrack(search):
 loop = True
 while loop : 
  if search : appMgr.run(1)   
  backward = 'Calo/Track/Forward'
  forward  = 'Calo/Track/Backward' 
  algfound = False
  for alg in appMgr.algorithms(): 
   if alg == 'CaloCosmicsTrackAlg' : 
     algfound = True
     break 
  if not algfound :
   CaloCosmicsTrackTool.processing()
   t = CaloCosmicsTrackTool.track()
   if CaloCosmicsTrackTool.tracked() :
    GaudiPython.setOwnership(t,False)
    cosmic_cont = newcont()
    GaudiPython.setOwnership(cosmic_cont,False)
    cosmic_cont.add(t)
    if t.states()[0].ty() > 0 :
     evt.registerObject(backward,cosmic_cont)
    else :  
     evt.registerObject(forward,cosmic_cont)  
 
  whatTracks = '' 
# check if a track had been found
  if evt[forward]  != None:  
   if evt[forward].size()>0 : 
    print "Forward Calo Cosmic found"
    whatTracks = forward
    loop = False
  if evt[backward] != None:  
   if evt[backward].size()>0 : 
    print "Backward Calo Cosmic found"
    whatTracks = backward 
    loop = False  
  if  whatTracks != '' :
# we have a track, update views when in search mode
   if search : 
    found = False
    curtitle = ui().currentPage().title.getValues()[0]
    for t in onlineViews :
     if curtitle == t : 
      x(onlineViews[t])
      found = True
      break 
    if not found :
     Page().currentRegion().clear("dynamicScene")
   visTrack(whatTracks)
  if not search : loop = False

def visTrack(whatTracks):
    session().setParameter('modeling.color','orange')
    session().setParameter('modeling.userTrackRange','true')
    session().setParameter('modeling.trackStartz','5000.')
    session().setParameter('modeling.trackEndz','20000.')
    for i in range(Page().fPage.getNumberOfRegions()) : 
     Page().setCurrentRegion(i)      
     uiSvc().visualize(evt[whatTracks])
     ui().synchronize()

