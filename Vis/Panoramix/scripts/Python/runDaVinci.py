from Gaudi.Configuration import *
from Configurables import DaVinci

DaVinci().DataType     = "DC06"
DaVinci().InputType    = "DST"
DaVinci().WithMC       = True

